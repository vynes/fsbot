# fsbot  
_**IRC Bot**_  
__________________________  


### Requirements  

Python >=3.10  
Does _not_ work with Windows. 


### Dependencies  

Some Python dependencies:  
 - setuptools - install python packages  
 - Twisted - networking framework (pulled in by install)  
 - Sqlite - schema (pulled in by install)  
 - requests - conectivity  (pulled in by install)  
 - peewee - database ORM  (pulled in by install)  


### Install  

`git clone https://vynes@bitbucket.org/vynes/fsbot.git`  

**Regular install:**  
`cd fsbot`   
`python ./setup.py install` or `pip install .`  

**_or_ Virtual environment install:**  
`virtualenv some/path/venv`  
`. ./some/path/venv/bin/activate`  
`cd fsbot`  
`python ./setup.py install` or `pip install .`  

**Configuration**  
config.ini is looked for in the following locations (if not specified at commandline):  
 - `~/.fsbot/config.ini`  
 - `~/.config/fsbot/config.ini`  
 - `~/.config/fsbot_config.ini`  
 - `~/.local/share/fsbot/config.ini`  
 - current dir  

Decide where you want config.ini to live and copy `example.config.ini` from examples folder 
as `config.ini` to that location.  

Most important sections to edit in your config.ini are the following:  
```ini

[DATABASE]
# AirportSqliteFile is main database used during normal operation. 
AirportSqliteFile = ~/.fsbot/fsbot.sqlite3

# AirportUpdateSqliteFile, if set, will be used by the update 
# command. Useful in case a separate database (ex: for testing) is 
# required. If not set, the update command will use the value of 
# AirportSqliteFile (main db) instead, doing an in place update.
;AirportUpdateSqliteFile = ~/.fsbot/fsbot_test.sqlite3


[APT]
# The apt.dat file is only used during the update process.
# FlightGear apt.dat is ISO-8859-1, X-Plane is UTF-8.
AirportDatFile = /path/to/apt.dat
AirportDatFileEncoding = UTF-8


[OURAIRPORTS]
### OUR AIRPORTS DATA SPECIFIC ###
# Get the files here: http://ourairports.com
AirportsCSVFile = ~/.fsbot/airports.csv
RunwaysCSVFile = ~/.fsbot/runways.csv
FrequenciesCSVFile = ~/.fsbot/airport-frequencies.csv
CountriesCSVFile = ~/.fsbot/countries.csv  
```

`AirportSqliteFile` is the only file here used during normal operation. The rest is for use 
with `fsbot-update`, when populating the database.

Database and data source files may be stored anywhere, but their locations 
must be specified correctly in config.ini (or at commandline).  

apt.dat file can be sourced from X-Plane data. FlightGear's outdated apt.dat also works.  

Download OurAirports csv files from here: http://ourairports.com/data/  


**Populate/Update Database**  
Make sure paths to database and data source files are correct in your config.ini.  

Update database:  
`fsbot-update all`  

If run for the first time, you will be asked whether the database (fsbot.sqlite3) 
should be created.  
Answer "yes".  

For future updates, replace those source files (ap.dat & \*.csv) with new ones and rerun:  
`fsbot-update all`  


### Some usage examples  

Run commands:  
`fsbot [options] <command> [command-options] [command-args]`  
  
**Some examples:**  
Get airport info for airport FQMA:  
`fsbot icao FQMA`  

Get verbose airport info for FQMA using source #2 (OurAirports):  
`fsbot icao -vv -s 2 FQMA`  

Get METAR for FMMI and FQMA:  
`fsbot metar FMMI FQMA`  

Get both TAF and METAR for LRBS:  
`fsbot wxr LRBS`  

Search for airport in Madagascar with ICAO ending in 'MI', with more (verbose) output:  
`fsbot search -i MG -a ?MI -vv`   

Search for airport in Madagascar containing 'Ivato':  
`fsbot search -i MG Ivato`  

IRC equivalent command (command trigger is ':'):  
`:search -i MG Ivato`  

Start the irc bot:  
`fsbot-irc [options]`  

Get contextual help:  
`fsbot <command> --help`

IRC: Get contextual help:  
`:help <command>`

See all available commands:  
`fsbot --help`


### Optional packages
:title irc command only parses HTML (and some json of specific sites) by default.  
To get title from javascript webapps, install:  
 - selenium (python package)  
 - Firefox (system) - for headless server see https://bugzilla.mozilla.org/show_bug.cgi?id=1372998  
 - geckodriver (system)  

