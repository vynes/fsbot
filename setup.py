#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='fsbot',
    python_requires='>=3.10',
    packages =
        find_packages('.', exclude=(
            'examples', 'examples.*',
            'test', 'test.*',
    )),
    version='0.1.71',
    description='IRC Bot',
    author='Curvian Vynes',
    author_email='vynes@protonmail.ch',
    license='MIT',
    url='https://bitbucket.org/vynes/fsbot',
    install_requires=[
        'service_identity',
        'pyopenssl',
        'twisted',
        'requests',
        'peewee',
        'feedparser'
    ],
    entry_points={
        'console_scripts': [
            'fsbot=fsbot.main:main',
            'fsbot-irc=fsbot.main:main',
            'fsbot-update=fsbot.main:main'
        ],
    },
    keywords=['fsbot', 'flight', 'sim', 'FlightGear', 'X-plane', 'irc', 'bot'],
    classifiers=[],
    zip_safe=False
)
