import re, random, math, time
import html

from twisted.internet import defer, threads, protocol, task, reactor, ssl
from twisted.words.protocols import irc

from builtins import chr

from fsbot import log, cfg, is_uri, __version_long__, FileCache
from fsbot.validation import is_empty
from fsbot.commands import command_registry, parse_args, ArgumentParser, CommandError, NoResultsError
from fsbot.feeds import scheduled_feeds
from fsbot.connectivity import http
from fsbot.scrape import title as stitle
from fsbot.rfactor import RfServerError, live_info, source_registry as rf_sources
from fsbot.teamspeak import TsServerError, poll_clients, source_registry as ts_sources


__all__ = [
    'run_irc_bot'
]


# User file cache to persist across sessions.
user_file_cache = FileCache(
    keystr='irc_user_command_cache',
    directory=cfg.get('CACHE', 'CacheDir'),
    ttl=0
)

# Restore from user file cache
user_cache = user_file_cache.read()

# New user file cache.
if user_cache is None:
    user_cache = {}
    user_file_cache.write(user_cache)



class IRCProtocol(irc.IRCClient):

    # For large output, every intv seconds a chunk is sent.
    intv = 2

    # Max chunk size. Can be set by user config.
    fallback_max_output_chars = 512

    # max command size
    max_command_chars = 32

    # IRC only bot commands with argument definitions.
    # args: list of ArgumentParser arguments.
    # hidden: Don't show in help.
    # argsrc: Other command's arguments to use.
    internal_command_registry = {
        'quit': {
            'args': [
                {
                    'names': ['password'],
                    'options': {
                        'metavar': 'PASSWORD',
                        'type': str,
                        'help': 'Admin password.'
                    }
                },
                {
                    'names': ['-m', '--quit-message'],
                    'options': {
                        'metavar': 'QUIT_MESSAGE',
                        'type': str,
                        'help': 'Quit message.'
                    }
                }
            ],
            'hidden': True,
            'argsrc': None
        },
        'reconnect': {'args': [], 'hidden': True, 'argsrc': 'quit'},
        'aichat': {'args': [], 'hidden': True, 'argsrc': None},
        'version': {'args': [], 'hidden': False, 'argsrc': None},
        'say': {
            'args': [
                {
                    'names': ['msg'],
                    'options': {
                        #'nargs': '+',
                        'metavar': 'MESSAGE',
                        'type': str,
                        'help': 'A message.'
                    }
                },
                {
                    'names': ['-s', '--sleep'],
                    'options': {
                        'metavar': 'SECONDS',
                        'type': int,
                        'default': 2,
                        'help': 'Time to wait before sending.'
                    }
                },
                {
                    'names': ['-c', '--channel'],
                    'options': {
                        'metavar': '#CHANNEL',
                        'type': str,
                        'help': 'Channel to send message to.'
                    }
                }
            ],
            'hidden': False,
            'argsrc': None
        },
        'help': {
            'args': [
                {
                    'names': ['command'],
                    'options': {
                        'nargs': '?',
                        'default': 'No',
                        'metavar': 'COMMAND',
                        'help': 'A valid bot command'
                    }
                },
                {
                    'names': ['-m', '--more'],
                    'options': {
                        'action': 'store_true',
                        'help': 'Show complete help'
                    }
                }
            ],
            'hidden': False,
            'argsrc': None
        },
        'title': {
            'args': [
                {
                    'names': ['url'],
                    'options': {
                        #'nargs': '+',
                        'metavar': 'URL',
                        'type': str,
                        'help': 'URL to get title from.'
                    }
                }
            ],
            'hidden': False,
            'argsrc': None
        }
    }

    # Command used for bot mentions at start of the message:
    command_direct = 'aichat'
    # Command used for general bot mentions:
    command_mention = 'aichat'


    def __init__(self):

        # Bot nick
        self.nickname = cfg.get('IRC', 'BotNick', fallback='fsbot')

        if log.getEffectiveLevel() == 10:
            self.nickname = self.nickname + '_debug'

        log.info(f'Setting nick: {self.nickname}')

        # Command trigger
        self.cmdtrigger = cfg.get('IRC', 'CommandTrigger', fallback=':')

        # Admin password
        self.password = cfg.get('IRC', 'AdminPass')

        # User nick
        self.nick = None
        
        # User host
        self.host = None

        # Max chars for output chunks
        self.max_output_chars = cfg.getint('IRC', 'MaxOutputChars', fallback=self.fallback_max_output_chars)

        self.commands = command_registry

        # Commands for which to cache args
        self.arg_cache_commands = list(map(lambda c: c.strip(), cfg.get('IRC', 'ArgCacheCommands', fallback='').split(',')))


    def connectionMade(self):
        irc.IRCClient.connectionMade(self)
        log.info('Connect time: {}'.format(time.asctime(time.localtime(time.time()))))


    def connectionLost(self, reason):
        irc.IRCClient.connectionLost(self, reason)
        log.info('Disconnect time: {}'.format(time.asctime(time.localtime(time.time()))))
        log.info(f'Disconnect reason: {reason}')


    def signedOn(self):
        key = None
        nickserv_passwd = cfg.get('IRC', 'NickServPassword', fallback=None)

        if nickserv_passwd is not None:
            log.info('Authenticating...')
            self.msg('NickServ', f'identify {nickserv_passwd}')

        for channel, passwd in self.factory.channels.items():
            log.info(f'Joining {channel}{" (using key)" if passwd else " (no key)"}...')
            self.join(channel=channel, key=passwd)        

        # Start polling loop.
        log.info('Starting feed loop...')
        loopDeferred = self.factory.feed_loop.start(cfg.get('POLLING', 'MinPollingLoopInterval', fallback=10))
        
        # Start rfactor loop.
        log.info('Starting rfserver loop...')
        rfLoopDeferred = self.factory.rfserver_loop.start(cfg.getint('RFACTOR_POLLING', 'PollingInterval', fallback=600))
        
        # Start Teamspeak loop.
        log.info('Starting tsserver loop...')
        tsLoopDeferred = self.factory.tsserver_loop.start(5)
        
        # Add callbacks for stop and failure.
        loopDeferred.addCallback(self.polling_loop_finished)
        loopDeferred.addErrback(self.polling_loop_error)
        
        rfLoopDeferred.addCallback(self.polling_loop_finished)
        rfLoopDeferred.addErrback(self.polling_loop_error)
        
        tsLoopDeferred.addCallback(self.polling_loop_finished)
        tsLoopDeferred.addErrback(self.polling_loop_error)


    def joined(self, channel):
        """This is called after bot joins the channel."""
        log.info(f'Successfully joined {channel}.')


    def polling_loop_error(self, failure):
        log.error(failure.getBriefTraceback())


    def polling_loop_finished(self, loop):
        log.debug(f'Loop stopped: {loop.__repr__()}')


    def privmsg(self, user, channel, message):
        """Called when a PRIVMSG received."""

        funky_args = {}

        def gather_funky_args(arg_match):
            """
            Callback for re.sub() command delegation.
            
            Appends all funky arguments (containing quoted spaces and weirdness) 
            to the funky_args dict with a placholder string as index to serve as 
            replacement marker for reinserting the complete argument back into 
            its proper slot.

            Returns the placeholder string.
            """
            if isinstance(arg_match, re.Match):
                idx = '##{:.6f}##'.format(len(funky_args))
                try:
                    funky_args[idx] = arg_match.group(1).encode('utf8').decode('unicode_escape')
                except IndexError as e:
                    log.error(e)
                    return
                return idx

        def join_args(args):
            log.debug(f'ARGS (before): {args}')
            rest_with_placeholders = re.sub(r'["\']((?:\\.|[^"\\])*)["\']', gather_funky_args, args)
            args = rest_with_placeholders.split()

            for placeholder, argvalue in funky_args.items():
                try:
                    args[args.index(placeholder)] = argvalue
                except ValueError as e:
                    log.error(e)
                    break

            log.debug(f'ARGS after: {args}')
            return args

        # reply mode
        msgmode = 'channel'

        self.nick, _, self.host = user.partition('!')
        message = message.strip()

        # Add user to cache
        if self.nick not in user_cache:
            user_cache[self.nick] = {'commands': {}}
            user_file_cache.write(user_cache)

        # Turn bot mention into a trigger.
        if message == self.nickname:
            message = self.cmdtrigger + self.command_direct
        elif message.startswith((self.nickname + ':', self.nickname + ',', self.nickname + ' ')):
            rest = message[len(self.nickname)+1:]
            if len(rest) > 0 and rest[0] != ' ': rest = ' ' + rest
            message = self.cmdtrigger + self.command_direct + rest
        elif ' ' + self.nickname.lower() in message.lower() or self.nickname.lower() + ' ' in message.lower():
            message = self.cmdtrigger + self.command_mention

        # Not a trigger command:
        if not message.startswith(self.cmdtrigger):
            return

        log.info(f'privmsg from user: {user}')

        command, sep, rest = message[1:].partition(' ')

        # Check command for funkiness
        for c in command:
            if re.match('[^A-Za-z0-9\-_:{}]'.format(self.cmdtrigger), c):
                log.info(f'Invalid command character: {repr(c)}. Ignoring command...')
                return

        # Check command length
        if len(command) > self.max_command_chars:
            log.info(f'Command too long: {len(command)} characters. Ignoring command...')
            return

        # Set command mode
        if command[0:2] == self.cmdtrigger*2:    # :::command
            command = command[2:]
            msgmode = 'query'
        elif command[0:1] == self.cmdtrigger:    # ::command
            command = command[1:]
            msgmode = 'notify'

        log.debug(f'msgmode is: {msgmode}')

        # Complete user command with his previous arguments used if omitted.
        if command in self.arg_cache_commands:
            if len(rest) == 0:
                if command in user_cache[self.nick]['commands']:
                    rest = user_cache[self.nick]['commands'][command]
            else:
                if command not in user_cache[self.nick]['commands'] or user_cache[self.nick]['commands'][command] != rest:
                    user_cache[self.nick]['commands'][command] = rest
                    user_file_cache.write(user_cache)


        args = join_args(rest)
        log.info(f'Request ({self.nick}): {command} {args}')

        # Call appropriate command handler
        if command in self.internal_command_registry or command.lower() == self.nickname.lower():
            func = getattr(self, 'command_' + command, None)
            
            if func is None: return

            d = defer.maybeDeferred(func, args)
        else:
            # No command
            if command not in self.commands:
                log.warning(f'No such command \'{command}\', skipping.')
                return 
            
            # All global commands are threaded
            d = threads.deferToThread(self.exec_command, command, args, channel)
        
        # Add callbacks to deal with what ever the command results are.
        # If the command throws error, the _show_error callback will log
        # the error and send a terse message to channel first:
        d.addErrback(self._showError)

        if msgmode == 'notify':
            log.debug(f'Return msg will be notification to nick: {self.nick}')
            target = self.nick
            notify = True
            self.msg(channel, 'Sending notice...')  # Should defer?
        elif msgmode == 'query':
            log.debug(f'Return msg will be query to nick: {self.nick}')
            target = self.nick
            notify = False
            self.msg(channel, 'Sending query...')  # Should defer?
        elif msgmode == 'channel':
            log.debug(f'Return msg will be to channel: {channel}')
            target = channel
            notify = False
        elif channel == self.nickname:
            log.debug(f'Return msg will be query to nick: {self.nick}')
            target = self.nick
            notify = False

        # What ever is returned is sent back as a reply:
        d.addCallback(self.send_message, target, self.nick, channel, notify)


    def send_message(self, data, target, nick, channel, notify=False):
        sendFunc = self.notice if notify == True else self.msg

        msg = None

        if isinstance(data, dict):
            if 'msg' in data:
                msg = data['msg']
            if 'target' in data:
                target = data['target']
            if 'nick' in data:
                nick = data['nick']
            if 'channel' in data:
                channel = data['channel']
        elif isinstance(data, str):
            msg = data
        else:
            return

        if msg is not None:
            log.debug(f'Output length is {len(msg)}, limit is {self.max_output_chars}.')

            if len(msg) > self.max_output_chars:
                log.info(f'Message exceeds {self.max_output_chars} character limit.')

                if target == channel:
                    self.msg(channel, 'Large output, sending query...')
                
                target = nick

                chunks = int(math.ceil(len(msg) / float(self.max_output_chars)))

                for chunk in range(0, chunks):
                    msg_chunk = msg[chunk * self.max_output_chars:self.max_output_chars * (chunk + 1)]
                    log.debug('Sending message chunk...')
                    reactor.callLater(self.intv * (chunk + 1), sendFunc, target, msg_chunk)
            else:
                log.info(f'Sending message ({type(msg)}): {msg}')
                sendFunc(target, msg)
        else:
            # TODO: Could do some work here (other than commands) that does not require sending a reply. 
            pass


    def _showError(self, failure):
        log.error(f'Error: {failure}')
        return self.nickname + ' panic'


    def exec_command(self, command, args, channel):

        log.debug(f'command: {command}')
        log.debug(f'args: {args}')

        # add irc flags to args
        args.append('__origin_irc__')
        args.append(f'__origin_irc_nick_{self.nick}__')
        args.append(f'__origin_irc_channel_{channel}__')

        try:
            command_cls = self.commands[command]['cls']
        except KeyError as e:
            log.error(e)
            return None

        try:
            result = str(command_cls(args))
        except CommandError as e:
            if 'human_error' in e.context:
                result = f'{self.nick}: {str(e.context["human_error"])} - see `{self.cmdtrigger}help {command}`'
            else:
                result = 'No result.'
        except NoResultsError as e:
                result = e.context['human_error']

        # Return result only, not deferred, since this command executer
        # function might be threaded.
        return result


    def get_internal_command_args(self, command):
        args = self.internal_command_registry[command]['args']

        if 'argsrc' in self.internal_command_registry[command] and self.internal_command_registry[command]['argsrc'] != None:
            src_command = self.internal_command_registry[command]['argsrc']
            if not src_command in self.internal_command_registry:
                # Argument source command was specified, but the command does not exist.
                raise ValueError(f'Command \'{src_command}\' does not exist.')
            args = self.internal_command_registry[src_command]['args']
        
        return args


    ##### INTERNAL COMMANDS #####
    
    def command_quit(self, args, reconnect=False):

        prog='quit' if not reconnect else 'reconnect'

        quit_parser = ArgumentParser(add_help=False, prog=prog)
        quit_arguments = self.get_internal_command_args(prog)

        try:
            quit_args = parse_args(parser=quit_parser, argument_definitions=(quit_arguments,), arguments=args)
        except CommandError as e:
            return str(e)

        log.debug(f'PARSED QUIT ARGS: {quit_args}')

        if quit_args['quit_message'] is None:
            if reconnect == True:
                quit_args['quit_message'] = 'brb!'
            else:
                quit_args['quit_message'] = 'ktxbye'

        if quit_args['password'] != self.password:
            log.warning('QUIT: Wrong password from {self.nick}. Ignoring.')
            return

        if reconnect == False:
            log.info(f'{self.nickname} was ordered to quit...')
            self.factory.quit_mode = 'die'
        else:
            log.info(f'{self.nickname} was ordered to reconnect...')
            self.factory.quit_mode = 'reconnect'
        
        self.quit(quit_args['quit_message'])


    def command_reconnect(self, args):
        return self.command_quit(args, reconnect=True)


    def command_aichat(self, args):
        """AI chatter/text processing handler.
        TODO: Some deep learning chatter bot shit or what ever.
        """
        d = defer.Deferred()

        replies = [
            '...','!',
            'а ну! чики брики и в дамки!',
            '⛬ want some candy?',
            'xD',
            ':P',
            ''
        ]
        replies.append(chr(random.randint(9840,9983)))

        reactor.callLater(1, d.callback, replies[random.randint(0,len(replies)-1)])
        return d


    def command_version(self, args):
        return __version_long__


    def command_say(self, args):

        d = defer.Deferred()
        
        say_parser = ArgumentParser(add_help=False, prog='say')
        say_arguments = self.get_internal_command_args('say')

        try:
            say_args = parse_args(parser=say_parser, argument_definitions=(say_arguments,), arguments=args)
        except CommandError as e:
            reactor.callLater(1, d.callback, str(e))
            return d

        if say_args['sleep'] == None or say_args['sleep'] < 2:
            say_args['sleep'] = 2

        if say_args['sleep'] > 604800:
            return 'Limit is 604800 seconds.'

        log.debug(f'SAY ARGS: {say_args}')

        overrides = {'msg': say_args['msg']}
        if say_args['channel'] is not None:
            overrides['target'] = say_args['channel']
            overrides['channel'] = say_args['channel']

        # callLater will callback the Deferred with the reply after so many seconds.
        reactor.callLater(say_args['sleep'], d.callback, overrides)
        # Returning the Deferred here means that it'll be returned from
        # maybeDeferred in privmsg.
        return d


    def command_help(self, args=''):
        """Return (contextual) help"""
        log.debug(f'Help context: {repr(args)}')

        d = defer.Deferred()

        # Command parser for help command
        help_parser = ArgumentParser(add_help=False, prog='help')
        help_arguments = self.get_internal_command_args('help')

        try:
            help_parsed_args = parse_args(parser=help_parser, argument_definitions=(help_arguments,), arguments=args)
        except CommandError as e:
            reactor.callLater(1, d.callback, str(e))
            return d

        # Create a parser for global command.
        if help_parsed_args['command'] in self.commands:
            
            context_parser = ArgumentParser(add_help=False, prog=help_parsed_args['command'])
            
            try:
                # Get command's help text by adding argument definitions without parsing
                # (Command parsing takes place in its command class when command is invoked)
                parse_args(
                    context_parser,
                    (self.commands[help_parsed_args['command']]['cls'].base_arguments, self.commands[help_parsed_args['command']]['cls'].arguments),
                    parse_arguments=False
                )
            except CommandError as e:
                reactor.callLater(1, d.callback, str(e))
                return d

            if help_parsed_args['more'] == True:
                helpmsg = context_parser.format_help()
            else:
                helpmsg = context_parser.format_usage()

        # Create parser for IRC only command.
        elif help_parsed_args['command'] in self.internal_command_registry:

            context_parser = ArgumentParser(add_help=False, prog=help_parsed_args['command'])

            try:
                # Use a different argument source if specified.
                arguments = self.get_internal_command_args(help_parsed_args['command'])

                # Get command's help text by adding argument definitions without parsing.
                parse_args(
                    context_parser,
                    (arguments,),
                    parse_arguments=False
                )
            except CommandError as e:
                reactor.callLater(1, d.callback, str(e))
                return d

            if help_parsed_args['more'] == True:
                helpmsg = context_parser.format_help()
            else:
                helpmsg = context_parser.format_usage()
        else:
            helpmsg = 'commands: {cmds}\nsee `{sep}help [--more] command`'.format(
                cmds = ", ".join(
                    c for c in list(self.commands.keys()) + [i for i in self.internal_command_registry if not self.internal_command_registry[i]['hidden']]
                ),
                sep = self.cmdtrigger
            )

        reactor.callLater(1, d.callback, helpmsg)
        return d


    def command_title(self, args):
        
        d = defer.Deferred()
        
        title_parser = ArgumentParser(add_help=False, prog='title')
        title_arguments = self.get_internal_command_args('title')

        try:
            title_args = parse_args(parser=title_parser, argument_definitions=(title_arguments,), arguments=args)
        except CommandError as e:
            reactor.callLater(1, d.callback, str(e))
            return d

        if not is_uri(title_args['url']):
            reactor.callLater(1, d.callback, f'Invalid url: \'{title_args["url"]}\'')
            return d

        if len(title_args['url']) > 2048:
            log.warning(f'URL too long, skipping title: {title_args["url"][:32]}...')
            return

        title = stitle(title_args['url'])

        # If title is still None, give up
        if title is None:
            title = 'No title found.'
        else:
            title = f'{html.unescape(title.strip())} ({title_args["url"]})'
        
        reactor.callLater(1, d.callback, title)
        
        return d


    def feed_polling(self):
        """
        [{'name': 'feed1_name', 'channel': '#chan1', 'items': []},{'name': 'feed2_name', 'channel': '#chan2': 'items': []}]
        """
        def get_published_parsed(item) -> str:
            """feedparser decided to drop published_parsed for updated_parsed
            """
            if hasattr(item, 'published_parsed'):
                timestamp_parsed = time.strftime("%Y-%m-%d",item.published_parsed)
            elif hasattr(item, 'updated_parsed'):
                timestamp_parsed = time.strftime("%Y-%m-%d",item.updated_parsed)
            else:
                timestamp_parsed = "--unknown-timestamp--"
            return timestamp_parsed
        
        for scheduled in scheduled_feeds(self.factory.channels.keys()):
            result = '\n'.join(f'[{get_published_parsed(item)}][{scheduled["name"]}] {item.title} - {item.link}' for item in scheduled['items'])
            log.info(f'Sending scheduled feed \'{scheduled["name"]}\' to {scheduled["channel"]}')
            self.msg(scheduled['channel'], result)


    def rfserver_polling(self):
        result = []

        for idx in rf_sources:
            try:
                sess_info = live_info(idx)
            except RfServerError:
                log.error(f'Error making request to server #{idx}, skipping...')
                continue

            num_humans_online = sum(map(lambda d: 1 if not d.ControlIsAI else 0, sess_info.VehicleInfo))
            #num_ai_online = sum(map(lambda a: 1 if a.ControlIsAI else 0, sess_info.VehicleInfo))

            if num_humans_online > 0:
                result.append((idx, sess_info.TrackName, num_humans_online))
                time.sleep(1)

        if len(result) > 0:
            if 'RFACTOR_POLLING' in cfg.sections():
                for channel in cfg.get('RFACTOR_POLLING', 'Channels', fallback='NONE').split(','):
                    log.info(f'Sending rf server player report to {channel}...')
                    self.msg(channel.strip(), '\n'.join(f'[RF2 S#{r[0]}] {r[2]} player{"s" if r[2] != 1 else ""} racing now at {r[1]}.' for r in result))
            self.msg(scheduled['channel'], result)


    def tsserver_polling(self):
        disable_ts_sources = []

        if len(ts_sources) == 0:
            return
        
        result = []

        for idx in ts_sources:
            if not ts_sources[idx].get('poll_address') or is_empty(ts_sources[idx]['poll_address']):
                continue
            try:
                users = poll_clients(idx)
            except TsServerError:
                log.error(f'Error making telnet request to Teamspeak server #{idx}, skipping...')
                disable_ts_sources.append(idx)
                continue
            
            if users is not None and (len(users['new']) > 0 or len(users['left']) > 0 or len(users['rejoined']) > 0):
                result.append([idx, ts_sources[idx]['name'], users])
                time.sleep(1)

        for src_idx in disable_ts_sources:
            log.warning(f'Disabling Teamspeak polling to server #{idx}...')
            del ts_sources[src_idx]

        if len(result) > 0:
            if 'TEAMSPEAK_SERVERS' in cfg.sections():
                for r in result:
                    server_text = f'TS server {r[0]}: \"{r[1]}\"'
                    num_online = len(r[2]["online"])
                    new_text = f'joined: [{",".join(u["client_nickname"] for u in r[2]["new"])}]' if len(r[2]['new']) > 0 else ''
                    rejoined_text = f'rejoined: [{",".join(u["client_nickname"] for u in r[2]["rejoined"])}]' if len(r[2]['rejoined']) > 0 else ''
                    left_text = f'left: [{",".join(u["client_nickname"] for u in r[2]["left"])}]' if len(r[2]['left']) > 0 else ''

                    for channel in ts_sources[idx]['channels'].split('|'):
                        log.info(f'Sending Teamspeak users status to {channel}...')
                        self.msg(channel.strip(), f'{server_text}, online: {num_online} | {" | ".join([st for st in [left_text, new_text, rejoined_text] if len(st) > 0])}')


### IRC CONNECTION ###

class IRCFactory(protocol.ClientFactory):

    def __init__(self, channels):
        self.channels = {}
        for chaninfo in channels:
            channel, _, passwd = chaninfo.partition(':')
            self.channels[channel] = None if len(passwd) == 0 else passwd
        log.info(f'Channels: {", ".join(list(self.channels.keys()))}')
        
        self.sleepers = {}
        
        self.quit_mode = 'error'

        self.feed_loop = None
        

    def relax_more(self, sleeper_name, initial_wait_time=0, multiplier=2):
        """Sleeps for wait_time in seconds, stretched exponentially every
        subsequent call by multiplier.
        """
        initial_wait_time = initial_wait_time if initial_wait_time > 0 else 1
        multiplier = multiplier if multiplier > 0 else 1

        if sleeper_name in self.sleepers:
            sleepytime = self.sleepers[sleeper_name]
        else:
            sleepytime = self.sleepers[sleeper_name] = initial_wait_time

        self.sleepers[sleeper_name] = self.sleepers[sleeper_name] * multiplier

        time.sleep(sleepytime)


    def buildProtocol(self, addr):
        p = IRCProtocol()
        p.factory = self
        self.feed_loop = task.LoopingCall(p.feed_polling)
        self.rfserver_loop = task.LoopingCall(p.rfserver_polling)
        self.tsserver_loop = task.LoopingCall(p.tsserver_polling)
        return p


    def stop(self):

        # Stop loops. TODO: be more dynamic ffs - stop all loops?
        if self.feed_loop is not None and self.feed_loop.running:
            log.info(f'Stopping feed loop...')
            self.feed_loop.stop()
        else:
            log.warning('Feed loop not running.')

        if self.rfserver_loop is not None and self.rfserver_loop.running:
            log.info(f'Stopping rfserver loop...')
            self.rfserver_loop.stop()
        else:
            log.warning('rfserver loop not running.')
        
        log.info('Shutting down...')
        reactor.stop()
        print('Goodbye.')


    def clientConnectionLost(self, connector, reason):
        if self.quit_mode in ('reconnect', 'error'):
            log.info('Trying to reconnect...')
            connector.disconnect()
            self.relax_more('reconnect', 0, 2)
            connector.connect()
        elif self.quit_mode == 'die':
            log.info('Disconnected.')
            self.stop()
        else:
            log.error(f'Connection lost: {reason}')

    def clientConnectionFailed(self, connector, reason):
        log.error(f'Connection failed: {reason}')
        self.stop()


def run_irc_bot(**kwargs):
    log.info(__version_long__)

    connect_secure = cfg.get('IRC', 'ConnectSSL', fallback='no').lower()

    irc_server = cfg.get('IRC', 'ConnectServer')
    irc_port = int(cfg.get('IRC', 'ConnectPort'))
    irc_channels = cfg.get('IRC', 'Channel')

    log.info(f'Connect: {irc_server}:{irc_port}')
    
    try:
        if not isinstance(irc_channels, str) or len(irc_channels) == 0: 
            raise ValueError('Invalid channels value: \'{irc_channels}\'')
        factory = IRCFactory(channels=list(map(lambda a: a.strip(), irc_channels.split(','))))
    except (AttributeError, ValueError) as e:
        log.error(e)
        log.error('No channel specified.')
        return
    
    if connect_secure in ('y', 'yes', '1'):
        log.info('SSL: yes')
        options = ssl.optionsForClientTLS(irc_server)
        reactor.connectSSL(irc_server, irc_port, factory, options)
    else:
        log.info('SSL: no')
        reactor.connectTCP(irc_server, irc_port, factory)

    reactor.run()
