import re

from fsbot import log, cast_from_ctype
from fsbot.connectivity import http
from fsbot.forex import ForexStructure
from fsbot.forex.sources import ForexSourceError, ForexInvalidRequestError, BaseSource


__all__ = [
    'OpenExchangeRates'
]


class OpenExchangeRates(BaseSource):
    """
    Get forex data from openexchangerates.org
    """
    idx = 1
    name = 'open exchange rates'
    info = '(currencies: all fiat & BTC, data: price only, query: multiple pairs supported) https://openexchangerates.org'

    base_url = 'https://openexchangerates.org/api/'

    url_params = {
        # app_id
        'app_id': '899de9c03b9e47108c6c42ebbfc3f8ff'
    }

    result = []


    def __init__(self):
        #type: () -> None
        super(OpenExchangeRates, self).__init__()
 
        
    def rates(self, pairs):
        currency_pairs = []

        # Transform curency pairs into something useful [('base','quote'),...]
        for pair in pairs:
            p = pair.split(':', 1) if ':' in pair else [pair[:3],pair[3:]]
            currency_pairs.append((p[0].upper(), p[1].upper()))

        # Get unique set of all currencies in question, base and quote.
        currency_set = set(map(lambda x: x[0], currency_pairs)).union(set(map(lambda y: y[1], currency_pairs)))
        
        # symbols not added to url params to avoid encoding of commas by requests/urlencode
        symbols = ','.join(currency_set)
        self.base_url = f'{self.base_url.rstrip("/")}/latest.json?symbols={symbols}' 
        
        try:
            oxr_result = http.request(url=self.base_url, params=self.url_params, json_decode=True)
        except http.NetError as e:
            log.error('Error getting data from {}. Error: {}'.format(self.name, e))
            raise ForexSourceError(e)

        if not oxr_result or 'rates' not in oxr_result or len(oxr_result['rates']) == 0:
            raise ForexInvalidRequestError('No data')

        # Since we don't know whether paid or freee account is used, get base from result.
        base_currency = oxr_result['base']

        forex_data = []

        for currency_pair in currency_pairs:

            # Skip when quote currency is base currency
            if currency_pair[0] == base_currency and currency_pair[1] == base_currency: continue

            rate_struct = ForexStructure()

            # Base currency is always 'USD' with free oxr account so we need to do a manual conversion.
            try:
                # If base is the same no conversion to be made
                if currency_pair[0] == base_currency:
                    ask = float(oxr_result['rates'][currency_pair[1]])
                else:
                    ask = (1 / float(oxr_result['rates'][currency_pair[0]])) * float(oxr_result['rates'][currency_pair[1]])
            except KeyError:
                continue

            log.debug(f'Found forex rate: {ask} {currency_pair[1]}')

            # Set correct type
            try:
                forex_rate_timestamp = cast_from_ctype(value=oxr_result['timestamp'], c_type=dict(rate_struct._fields_)['closetime'])
                forex_rate_pair = cast_from_ctype(value=currency_pair[0]+currency_pair[1], c_type=dict(rate_struct._fields_)['pair'])
                forex_rate_value = cast_from_ctype(value=ask, c_type=dict(rate_struct._fields_)['ask'])
            except (IndexError, ValueError) as e:
                raise ForexSourceError(e)
                    
            # Set ForexStructure rate_struct to value
            setattr(rate_struct, 'closetime', forex_rate_timestamp)
            setattr(rate_struct, 'pair', forex_rate_pair)
            setattr(rate_struct, 'ask', forex_rate_value)

            forex_data.append(rate_struct) 

        return forex_data


    def list(self):
        self.base_url = self.base_url.rstrip('/') + '/currencies.json'

        try:
            oxr_result = http.request(url=self.base_url, params=self.url_params, json_decode=True)
        except http.NetError as e:
            log.error('Error getting data from {}. Error: {}'.format(self.name, e))
            raise ForexSourceError(e)

        return oxr_result
