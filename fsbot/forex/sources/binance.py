from fsbot.exceptions import throw
from fsbot import log, cast_from_ctype
from fsbot.connectivity import http
from fsbot.forex import ForexStructure
from fsbot.forex.sources import ForexSourceError, ForexInvalidRequestError, ForexInvalidSymbolError, BaseSource


__all__ = [
    'Binance'
]


class Binance(BaseSource):
    """
    Get forex data from Binance
    """
    
    idx = 2
    name = 'Binance Exchange'
    info = '(currencies: crypto, data: bid/ask/vol, query: single pair only) https://binance.com'

    base_url = 'https://www.binance.com/api/v1/ticker/24hr'

    # {"user_used_alias": "actiol_symbol_for_this_exchange"}
    symbol_aliases = {'IOT': 'IOTA', 'USD': 'USDT'}

    url_params = {}

    result = []


    def __init__(self):
        #type: () -> None

        super(Binance, self).__init__()
 
        
    def rates(self, pairs):

        # Currently this API supports either 1 pair per query or all pairs.
        # Since getting all pairs carries more weight toward usage limits 
        # we will support querying only one at a time for now.
        pair = pairs[0].replace(':','').upper()

        # Check for and match/replace aliases
        for alias, actual in self.symbol_aliases.items():
            # Swap base if alias used
            if pair[:len(alias)] == alias and pair[:len(actual)] != actual:
                pair = actual + pair[len(alias):]
            # Swap quote if alias used
            if pair[-len(alias):] == alias and pair[-len(actual):] != actual:
                pair = pair[:-len(alias)] + actual

        self.base_url = self.base_url.rstrip("/")
        
        self.url_params['symbol'] = pair

        try:
            binance_result = http.request(url=self.base_url, params=self.url_params, json_decode=True, ignore_error_codes=[400])
        except http.NetError as e:
            log.error('Error getting data from {}. Error: {}'.format(self.name, e))
            raise ForexSourceError(e)

        if 'code' in binance_result and binance_result['code'] == -1121:
            raise throw(
                exc=ForexInvalidSymbolError(f'Invalid pair/symbol {pair}.'),
                context={
                    'symbol': pair
                }
            )

        if not binance_result or 'askPrice' not in binance_result:
            raise ForexSourceError(f'No data: {binance_result}')

        log.debug(f'Found binance data: {binance_result}')
        
        exchange_data = []

        data_struct = ForexStructure()

        # Set correct type
        try:
            exchange_data_closetime = cast_from_ctype(value=binance_result['closeTime'], c_type=dict(data_struct._fields_)['closetime'])
            exchange_data_pair = cast_from_ctype(value=pair, c_type=dict(data_struct._fields_)['pair'])
            exchange_data_ask = cast_from_ctype(value=binance_result['askPrice'], c_type=dict(data_struct._fields_)['ask'])
            exchange_data_bid = cast_from_ctype(value=binance_result['bidPrice'], c_type=dict(data_struct._fields_)['bid'])
            exchange_data_volume = cast_from_ctype(value=binance_result['volume'], c_type=dict(data_struct._fields_)['volume'])
        except (IndexError, ValueError) as e:
            raise ForexSourceError(e)
                
        # Set ForexStructure rate_struct to value
        setattr(data_struct, 'closetime', exchange_data_closetime / 1000)
        setattr(data_struct, 'pair', exchange_data_pair)
        setattr(data_struct, 'ask', exchange_data_ask)
        setattr(data_struct, 'bid', exchange_data_bid)
        setattr(data_struct, 'volume', exchange_data_volume)

        exchange_data.append(data_struct) 

        return exchange_data


    def list(self):
        
        self.base_url = self.base_url.rstrip('/') + '/currencies.json'

        try:
            oxr_result = http.request(url=self.base_url, params=self.url_params, json_decode=True)
        except http.NetError as e:
            log.error('Error getting data from {}. Error: {}'.format(self.name, e))
            raise ForexSourceError(e)

        return oxr_result
