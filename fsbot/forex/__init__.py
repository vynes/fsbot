from ctypes import c_int, c_wchar_p, c_float, c_double
from fsbot.struct import BaseStructure



class ForexStructure(BaseStructure):
    _fields_ = [
        ('closetime', c_double),
        ('pair', c_wchar_p),
        ('bid', c_float),
        ('ask', c_float),
        ('volume', c_double)
    ]

    _defaults_ = {
        'closetime': -99999999.999,
        'pair': 'n/a',
        'bid': 0.00,
        'ask': 0.00,
        'volume': -99999999.999
    }



from .report import *
