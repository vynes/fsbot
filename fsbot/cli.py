import sys

from fsbot import log, is_empty
from fsbot.commands import command_registry, CommandError, NoResultsError

__all__ = [
    'run_user_cli'
]


def run_user_cli(**kwargs):
    """
    Gets commands already parsed as dictionary.
    """ 
    log.debug('kwargs: {}'.format(kwargs))
    
    try:
        command_cls = command_registry[kwargs['command']]['cls']
    except KeyError:
        return "unknown command: '{}'".format(kwargs['command'])
    
    try:
        result = command_cls(parsed_args=kwargs)
    except NoResultsError as e:
        log.debug(e)
        if 'cli_error' in e.context:
            result = e.context['cli_error']
        else:
            sys.exit()
    except CommandError as e:
        log.debug(e)
        if 'human_error' in e.context:
            result = e.context['human_error']
        else:
            sys.exit()

    if not is_empty(result): print(result)
