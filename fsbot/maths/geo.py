import math

from fsbot import log
from fsbot.exceptions import throw


__all__ = [
    'GeoError',
    'GeoInputError',
    'earth_radius',
    'geodist',
    'get_proximity_bounding_coordinates',
    'bearing',
    'fg_deg_to_dec'
]


class GeoError(Exception):
    pass


class GeoInputError(Exception):
    pass


def earth_radius(unit='m'):
    # type: (str) -> float
    """
    Return Earth radius in meters or foot.
    """
    R = 6371000.0
    
    if unit == 'm' or not unit:
        return R 
    elif unit == 'km':
        return R / 1000 
    elif unit == 'ft':
        return R * 3.280839895013

    raise GeoInputError("{} is not an accepted unit for Earth radius.".format(unit))


def get_proximity_bounding_coordinates(lat_degrees, lon_degrees, proximity_radius_km, radians=False):
    # type: (float, float, int, bool) -> dict
    """
    Get Min/Max bounding coordinates for the given coordinates and proximity limit/distance.

    Returns dict:
    {"angular_radius": angular_radius, "min":(lat_min, lon_min), "max":(lat_max, lon_max)}
    """

    # Convert to rads
    lat = math.radians(lat_degrees)
    lon = math.radians(lon_degrees)

    # Get angular radius
    angular_radius = proximity_radius_km / earth_radius('km')

    # Compute Min/Max Latitude
    lat_min = lat - angular_radius
    lat_max = lat + angular_radius

    # Compute Min/Max Longitude
    
    # lat_tangent = math.asin(math.sin(lat)/math.cos(angular_radius))
    # lon_delta = math.acos((math.cos(angular_radius)-math.sin(lat_tangent)*math.sin(lat))/(math.cos(at_tangent)*math.cos(lat)))
    # =
    lon_delta = math.asin(math.sin(angular_radius)/math.cos(lat))

    lon_min = lon - lon_delta
    lon_max = lon + lon_delta

    # Check if poles or 180 meridian inside query area
    if lat_max > math.pi/2:
        # North pole as well as all meridians are inside query area
        lon_min = -math.pi
        lat_max = math.pi/2
        lon_min = math.pi
    elif lat_min < -math.pi/2:
        # South pole is within query area
        lat_min = -math.pi/2
        lon_min = -math.pi
        lon_max = math.pi
    elif lon_min < -math.pi or lon_max < -math.pi or lon_min > math.pi or lon_max > math.pi:
        # 180th meridian is in the query area
        # To get a better shape here, two bounding boxes could be used (see ref)
        lon_min = -math.pi
        lon_max = math.pi

    # Convert back to degrees
    if radians == False:
        lat_min = math.degrees(lat_min)
        lon_min = math.degrees(lon_min)
        lat_max = math.degrees(lat_max)
        lon_max = math.degrees(lon_max)

    return {"angular_radius": angular_radius, "min":(lat_min, lon_min), "max":(lat_max, lon_max)}


def dist_haversine(lat1, lon1, lat2, lon2, is_rads=False):
    # type: (float, float, float, float, bool) -> float
    """
    Haversine Formula

    a = sin²(Δφ/2) + cos φ1 ⋅ cos φ2 ⋅ sin²(Δλ/2)
    c = 2 ⋅ atan2( √a, √(1−a) )
    d = R ⋅ c 
        where   φ is latitude, λ is longitude, R is earth's radius (mean radius = 6371km);
        angles need to be in radians to pass to trig functions.

    The haversine formula 'remains particularly well-conditioned for numerical computa­tion 
    even at small distances' - unlike calcula­tions based on the spherical law of cosines. 
    The '(re)versed sine' is 1−cosθ, and the 'half-versed-sine' is (1−cosθ)/2 or sin²(θ/2) 
    as used above. 

    Once widely used by navigators, it was described by Roger Sinnott in Sky & Telescope 
    magazine in 1984 ("Virtues of the Haversine"): Sinnott explained that the angular 
    separa­tion between Mizar and Alcor in Ursa Major - 0°11'49.69" - could be accurately 
    calculated on a TRS-80 using the haversine.

    For the curious, c is the angular distance in radians, and a is the square of half the 
    chord length between the points.

    If atan2 is not available, c could be calculated from 2 ⋅ asin( min(1, √a) ) 
    (including protec­tion against rounding errors).
    """

    ER = earth_radius()
    
    # Convert to rads if given in spherical coords degrees
    if is_rads == False:
        lat1 = math.radians(lat1)
        lat2 = math.radians(lat2)
        lon1 = math.radians(lon1)
        lon2 = math.radians(lon2)

    distLat = lat2 - lat1
    distLon = lon2 - lon1

    log.debug('distLat {0}, distLon {1}, lat1 {2}, lon1 {3}, lat2 {4}, lat2 {5}'.format(distLat, distLon, lat1, lon1, lat2, lon2))
    
    a = math.sin(distLat / 2) * math.sin(distLat / 2) + math.sin(distLon / 2) * math.sin(distLon / 2) * math.cos(lat1) * math.cos(lat2);
    log.debug('a: {}'.format(a))
    
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    log.debug('c: {}'.format(c))
    
    d = ER * c
    log.debug('Measured (d): {}'.format(d))
    
    return d


def geodist(lat1, lon1, lat2, lon2, dec=2, method='haversine'):
    # type: (float, float, float, float, int, str) -> float
    """
    Distance between coordinates returned in meters.
    """
    
    lat1 = float(lat1)
    lon1 = float(lon1)
    lat2 = float(lat2)
    lon2 = float(lon2)
    
    #more methods to follow: 
    if method == 'haversine':
        dfunc = dist_haversine    
    else:
        dfunc = dist_haversine    
    
    d = dfunc(lat1, lon1, lat2, lon2)
    #distance = '{dist:.{decimals}f}'.format(dist=d, decimals=dec)
    log.debug('Measured distance: {}'.format(d))
    
    return d


def fg_deg_to_dec(coord):
    # type: (str) -> float
    """
    Convert coordinate degree notation to decimal.
    Input format: N30 27.345641

    This is not used anywhere any more in fsbot as far as I know.
    """    
    sign = ''
    nesw = coord[0]
    coord = coord.lstrip(coord[0])

    if nesw == 'S' or nesw == 'W':
        sign = '-'
    log.debug('Splitting coord: {}'.format(coord))
    
    coord = coord.split(' ')
    result = '{0}{1:.8f}'.format(sign, int(coord[0]) + float(coord[1]) / 60)
    log.debug('_degtodec: {}'.format(result))
    
    return result


def degree_length(lat, lon, degrees=1):
    #type: (float, float, int) -> list
    """
    Determine length of degrees degrees laterally and longitudinally.
    """
    log.debug("Calculating degree length for lat: {0}, lon: {1}".format(lat, lon))

    if type(degrees) is not int:
        raise TypeError("Reference degrees must be of type int. Type given: {}.".format(type(degrees).__name__))
    
    lat_ref = float(lat) + degrees
    lon_ref = float(lon) + degrees
    lat_ref = lat_ref - 90 if lat_ref > 90 else lat_ref
    lon_ref = lon_ref - 180 if lon_ref > 180 else lon_ref
    log.debug("lat ref: {0}, lon ref: {1}".format(lat_ref, lon_ref))
    
    n_degrees_lat_length = geodist(lat, lon, lat_ref, lon)
    n_degrees_lon_length = geodist(lat, lon, lat, lon_ref)
    log.debug("lat deg length: {0}, lon deg length: {1}".format(n_degrees_lat_length, n_degrees_lon_length))
    
    return [n_degrees_lat_length, n_degrees_lon_length]


def bearing(lat1, lon1, lat2, lon2, is_rads=False):
    # type: (float, float, float, float, bool, bool) -> tuple
    """
    Return initial and final bearings between 2 sets of coordinates.

    Math Description:
    source: http://www.movable-type.co.uk/scripts/latlong.html

    In general, your current heading will vary as you follow a great circle path (orthodrome); the final 
    heading will differ from the initial heading by varying degrees according to distance and latitude 
    (if you were to go from say 35°N,45°E (≈ Baghdad) to 35°N,135°E (≈ Osaka), you would start on a heading 
    of 60° and end up on a heading of 120°!).

    This formula is for the initial bearing (sometimes referred to as forward azimuth) which if followed in 
    a straight line along a great-circle arc will take you from the start point to the end point:1

    Formula:    
    θ = atan2( sin Δλ ⋅ cos φ2 , cos φ1 ⋅ sin φ2 − sin φ1 ⋅ cos φ2 ⋅ cos Δλ )
    where φ1,λ1 is the start point, φ2,λ2 the end point (Δλ is the difference in longitude)

    JavaScript:
    (all angles in radians)
    var y = Math.sin(λ2-λ1) * Math.cos(φ2);
    var x = Math.cos(φ1)*Math.sin(φ2) - Math.sin(φ1)*Math.cos(φ2)*Math.cos(λ2-λ1);
    var brng = Math.atan2(y, x).toDegrees();

    Excel:
    (all angles in radians)
    =ATAN2(COS(lat1)*SIN(lat2)-SIN(lat1)*COS(lat2)*COS(lon2-lon1), SIN(lon2-lon1)*COS(lat2)) 
    *note that Excel reverses the arguments to ATAN2 – see notes below.

    Since atan2 returns values in the range -π ... +π (that is, -180° ... +180°), to normalise the result to 
    a compass bearing (in the range 0° ... 360°, with −ve values transformed into the range 180° ... 360°), 
    convert to degrees and then use (θ+360) % 360, where % is (floating point) modulo.

    For final bearing, simply take the initial bearing from the end point to the start point and reverse it 
    (using θ = (θ+180) % 360).
    """
    
    # Convert to rads if given in spherical coords degrees
    try:
        if is_rads == False:
            lat1 = math.radians(float(lat1))
            lat2 = math.radians(float(lat2))
            lon1 = math.radians(float(lon1))
            lon2 = math.radians(float(lon2))
    except (TypeError, ValueError) as e:
        raise GeoInputError('Invalid type in icao paramters (float expected): [{}]'.format(','.join([type(lat1).__name__, type(lat2).__name__, type(lon1).__name__, type(lon2).__name__])))

    #intial bearing
    ys = math.sin(lon2-lon1) * math.cos(lat2);
    xs = math.cos(lat1) * math.sin(lat2) - math.sin(lat1) * math.cos(lat2) * math.cos(lon2-lon1);
    start_to_end_bearing = math.degrees(math.atan2(ys, xs));

    # Normalise
    initial_bearing = (start_to_end_bearing + 360) % 360

    if lat1 == lat2 and lon1 == lon2:
        final_bearing = initial_bearing
    else:
        ye = math.sin(lon1-lon2) * math.cos(lat1);
        xe = math.cos(lat2) * math.sin(lat1) - math.sin(lat2) * math.cos(lat1) * math.cos(lon1-lon2);
        end_to_start_bearing = math.degrees(math.atan2(ye, xe));
        final_bearing = (((end_to_start_bearing + 360) % 360) + 180) % 360

    return (initial_bearing, final_bearing)
