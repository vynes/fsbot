from fsbot import log
from fsbot.exceptions import throw


__all__ = [
    'ConversionInputError',
    'ConversionError',
    'convert_unit',
    'list_conversion_symbols',
    'get_conversion_table'
]


class ConversionInputError(Exception):
    pass


class ConversionError(Exception):
    pass


def fg_deg_to_dec(coord):
    """
    Converts FlightGear coordinates format to decimal.

    Input format: N30 27.345641
    """
    sign = ''

    nesw = coord[0]

    coord = coord.lstrip(coord[0])

    if nesw == 'S' or nesw == 'W':
        sign = '-'

    log.debug('Splitting coord: {}'.format(coord))
    coord = coord.split(' ')

    result = '{0}{1:.8f}'.format(sign, int(coord[0]) + float(coord[1]) / 60)

    log.debug('fg_deg_to_dec: {}'.format(result))

    return result


def convert_coordinates(value, from_unit, to_unit):
    """Geographical coordinates conversion not implemented yet."""
    pass


def convert_temperature(value, from_unit, to_unit):
    """
    from_unit and to_unit must be of ('C', 'F', 'K', 'R')
    """
    abs_zero_f = 459.67
    abs_zero_c = 273.15

    if from_unit == to_unit:
        return value

    def f_to_c(value_c):
        return (value_c - 32) * (5 / 9)

    def c_to_f(value_f):
        return value_f / (5 / 9) + 32

    def c_to_k(value_c):
        return value_c + abs_zero_c

    def k_to_c(value_k):
        return value_k - abs_zero_c

    def f_to_r(value_f):
        return value_f + abs_zero_f

    def r_to_f(value_r):
        return value_r - abs_zero_f

    def c_to_r(value_c):
        return f_to_r(c_to_f(value_c))

    def r_to_c(value_r):
        return f_to_c(r_to_f(value_r))

    def f_to_k(value_f):
        return c_to_k(f_to_c(value_f))

    def k_to_f(value_k):
        return c_to_f(k_to_c(value_k))

    def k_to_r(value_k):
        return f_to_r(k_to_f(value_k))

    def r_to_k(value_r):
        return c_to_k(r_to_c(value_r))

    return locals()[from_unit.lower() + '_to_' + to_unit.lower()](value)


def get_conversion_table():
    # type: () -> dict
    """
    Returns a dict of conversion units matrix.

    Add a 'function' element to category dict if it is not a direct conversion.
    """
    return {
        'distance': {
            'conversion': [
                {'symbol': ['m', 'meter', 'metre'], 'value': 1.0, 'name': 'Meter', 'singular': 'metre', 'plural': 'metres'},
                {'symbol': ['yd', 'yds'], 'value': 1.093613298338, 'name': 'Yard', 'singular': 'yard', 'plural': 'yards'},
                {'symbol': ['km'], 'value': 0.001, 'name': 'Kilometer', 'singular': 'kilometer', 'plural': 'kilometers'},
                {'symbol': ['NM', 'nmi'], 'value': 0.00053995680345572354, 'name': 'Nautical mile', 'singular': 'nautical mile', 'plural': 'nautical miles'},
                {'symbol': ['mi'], 'value': 0.00062150403977625854, 'name': 'Statutory mile', 'singular': 'Statutory mile', 'plural': 'Statutory miles'},
                {'symbol': ['ft', 'foot'], 'value': 3.280839895013, 'name': 'Foot', 'singular': 'foot', 'plural': 'feet'},
                {'symbol': ['mm'], 'value': 1000.0, 'name': 'Millimetre', 'singular': 'millimetre', 'plural': 'millimetres'},
                {'symbol': ['nm'], 'value': 1000000000.0, 'name': 'Nanometre', 'singular': 'nanometre', 'plural': 'nanometres'},
                {'symbol': ['cm'], 'value': 100.0, 'name': 'Centimetre', 'singular': 'centimetre', 'plural': 'centimetres'},
                {'symbol': ['in', 'inches', 'inch', '"'], 'value': 39.37008, 'name': 'Inch', 'singular': 'inch', 'plural': 'inches'}
            ]
        },
        'pressure': {
            'conversion': [
                {'symbol': ['Pa'], 'value': 1.0, 'name': 'Pascal', 'singular': 'pascal', 'plural': 'pascal'},
                {'symbol': ['inHg', 'inhg'], 'value': 0.00029529983071445, 'name': 'Inch of mercury 0 ºC', 'singular': 'inch of mercury', 'plural': 'inches of mercury'},
                {'symbol': ['mmHg', 'mmhg'], 'value': 0.0075006156130264, 'name': 'Millimeter of mercury 0 ºC', 'singular': 'millimeters of mercury', 'plural': 'millimeters of mercury'},
                {'symbol': ['mb', 'mbar'], 'value': 0.01, 'name': 'Millibar', 'singular': 'millibar', 'plural': 'millibars'},
                {'symbol': ['hPa'], 'value': 0.01, 'name': 'Hectopascal', 'singular': 'hectopascal', 'plural': 'hectopascal'},
                {'symbol': ['kPa'], 'value': 0.001, 'name': 'Kilopascal', 'singular': 'kilopascal', 'plural': 'kilopascal'},
                {'symbol': ['atm'], 'value': 0.000009869233, 'name': 'Standard atmosphere', 'singular': 'standard atmosphere', 'plural': 'standard atmospheres'},
                {'symbol': ['cmH2O'], 'value': 0.010197162129779282, 'name': 'Centimeter of water 4 ºC', 'singular': 'centimeter of water', 'plural': 'centimeters of water'},
                {'symbol': ['torr'], 'value': 0.0075006168270417, 'name': 'Torr', 'singular': 'torr', 'plural': 'torr'},
                {'symbol': ['psi'], 'value': 0.00014503773800722, 'name': 'Pound per square inch', 'singular': 'pound per square inch', 'plural': 'pounds per square inch'}
            ]
        },
        'temperature': {
            'conversion': [
                {'symbol': ['C', '°C', 'celcius', 'degC'], 'value': 1.0, 'name': 'Celcius', 'singular': 'degrees celcius', 'plural': 'degrees celcius'},
                {'symbol': ['F', '°F', 'farenheit', 'degF'], 'value': 0.0, 'name': 'Farenheit', 'singular': 'degrees farenheit', 'plural': 'degrees farenheit'},
                {'symbol': ['K', '°K', 'kelvin', 'degK'], 'value': 0.0, 'name': 'Kelvin', 'singular': 'degrees kelvin', 'plural': 'degrees kelvin'},
                {'symbol': ['R', '°R', 'rankine', 'degR'], 'value': 0.0, 'name': 'Rankine', 'singular': 'degrees rankine', 'plural': 'degrees rankine'}
            ],
            'function': convert_temperature
        }

    }


def list_conversion_symbols():
    # type: () -> dict
    """
    Return a dict symbol lists for use with unit conversion.
    """
    symbol_list = {}

    conversion_table = get_conversion_table()

    for conversion_type in conversion_table:
        symbol_list[conversion_type] = [unit['symbol'][0] for unit in conversion_table[conversion_type]['conversion']]

    return symbol_list



def convert_unit(value, from_unit, to_unit, verbose=False):
    # type (Union(str, int, float), str, str, bool) -> Union(float, Dict)
    """
    Convert one numeric unit value to another.

    To convert 314.1 Nautical Miles to Kilometer: convert_unit(314.1, 'NM', 'km')

    """
    conversion_table = get_conversion_table()

    from_idx, to_idx = ([None, None], [None, None])

    conversion_used = None

    symbols_matched = {
        from_unit: len(conversion_table),
        to_unit: len(conversion_table)
    }

    for conversion_type in conversion_table:
        for idx, unit in enumerate(conversion_table[conversion_type]['conversion']):
            # Check for symbol in conversion table and convert.
            if from_unit in conversion_table[conversion_type]['conversion'][idx]['symbol']:
                from_idx = [conversion_type, idx]

            if to_unit in conversion_table[conversion_type]['conversion'][idx]['symbol']:
                to_idx = [conversion_type, idx]

        if from_idx[1] == None and to_idx[1] == None:
            symbols_matched[from_unit] -= 1
            symbols_matched[to_unit] -= 1
        elif from_idx[1] == None:
            symbols_matched[from_unit] -= 1
        elif to_idx[1] == None:
            symbols_matched[to_unit] -= 1
        else:
            conversion_used = conversion_type
            break

    log.debug('symbols_matched: {}'.format(symbols_matched))

    # Check for valid symbols
    if symbols_matched[from_unit] == 0 or symbols_matched[to_unit] == 0 or conversion_used == None:
        raise ConversionInputError('Invalid symbols: {}'.format(','.join((s if symbols_matched[s] == 0 else '') for s in symbols_matched).strip(',')))

    # Check if symbols found in same conversion type
    if from_idx[0] != to_idx[0] and from_idx[0] is not None and to_idx[0] is not None:
        raise ConversionInputError('Symbols are incompatible: {} and {}...'.format(from_idx[0], to_idx[0]))

    try:
        if 'function' in conversion_table[conversion_type]:
            # Unit specific conversions
            answer = conversion_table[conversion_type]['function'](
                value,
                conversion_table[conversion_type]['conversion'][from_idx[1]]['symbol'][0],
                conversion_table[conversion_type]['conversion'][to_idx[1]]['symbol'][0]
            )
        else:
            # Direct conversions
            answer = float(value) * (conversion_table[conversion_type]['conversion'][to_idx[1]]['value'] / conversion_table[conversion_type]['conversion'][from_idx[1]]['value'])

        if verbose:
            return {
                'answer': answer,
                'meta': {
                    'from_unit': conversion_table[conversion_type]['conversion'][from_idx[1]],
                    'to_unit': conversion_table[conversion_type]['conversion'][to_idx[1]]
                }
            }
        else:            
            return answer

    except TypeError as e:
        raise ConversionError(e)

    raise ConversionError('Conversion error ocurred.')
