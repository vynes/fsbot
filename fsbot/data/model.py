from os import path
from peewee import Model, SqliteDatabase

from fsbot import cfg


__all__ = [
    'BaseModel'
]


# Switched to peewee ORM
# Subclass BaseModel when creating new DB Models
database = SqliteDatabase(path.abspath(path.expanduser(cfg.get('DATABASE', 'AirportSqliteFile'))), **{})


class BaseModel(Model):
    class Meta:
        database = database
