import re, sqlite3

from fsbot import log


__all__ = [
    'DB',
    'DBError',
    'OperationalError'
]


class DBError(Exception):
    pass


class OperationalError(sqlite3.OperationalError):
    pass


def s(value):
    """sanitise input params"""
    if not re.search('drop table|insert into|delete from', value.lower()):
        return value
    e = 'Bogus db input: {}'.format(value)
    log.error(e)
    raise DBError(e)


class DB(object):
    """docstring for DB"""

    def __init__(self, dbname, mapping='Default'):
        log.debug(f'using db: \'{dbname}\'')
        self.dbname = dbname

        self.conn = sqlite3.connect(dbname)

        # Reference row indices by col name instead of int
        if mapping == 'Row':
            self.conn.row_factory = sqlite3.Row

        self.c = self.conn.cursor()


    def executescript(self, sql):
        # type: (str) -> DB
        self.c.executescript(sql)
        return self


    def execute(self, query, values=None):
        # type: (str, Tuple) -> DB
        self.c.execute(query, values)
        return self


    def truncate(self, table, force=False, delseq=True):

        self.c.execute('SELECT * FROM sqlite_master WHERE type=? AND name=?', ('table', table))
        existing = self.c.fetchall()
        
        if len(existing) > 0:

            self.c.execute('SELECT count(*) FROM {}'.format(s(table)))
            records = self.c.fetchall()
            
            # Do not continue truncating if table is empty, unless force == True
            if len(records) < 1 and force != True:
                return

            self.c.execute('DELETE FROM {}'.format(s(table)))

            # Delete auto increment sequences
            if delseq == True:
                try:
                    self.c.execute('DELETE FROM sqlite_sequence WHERE name=?', table)
                except sqlite3.OperationalError as e:
                    log.debug(e)
                    log.debug('Skipping sequence table cleanup.')

            self.commit()


    def reindex(self):
        try:
            self.c.execute('REINDEX')
        except sqlite3.OperationalError as e:
            log.debug(e)
            log.debug('Skipping sequence table cleanup.')
            raise DBError


    def all(self, table):
        """Select * from table"""
        self.c.execute('SELECT * FROM {}'.format(s(table)))
        return self.c.fetchall()


    def one(self, table, data, cols='*'):
        """Select one where"""
        q = 'SELECT {cols} FROM {table} WHERE {x}=?'.format(cols=cols, table=table, x=data[0])
        self.c.execute(q, (data[1],))
        return self.c.fetchone()


    def insert(self, table, data):
        """Insert data"""
        log.debug('COLS: ({}), VALUES: ({})'.format(data[0], data[1]))
        self.c.execute('INSERT INTO {table} ({cols}) VALUES ({values})'.format(
                table=s(table), 
                cols=s(','.join(data[0])),
                values=','.join('?' for v in data[1])
            ),
            list(data[1])
        )
        return self.c.rowcount


    def commit(self):
        self.conn.commit()


    def close(self):
        self.conn.close()


    def finish(self):
        self.conn.commit()
        self.conn.close()
