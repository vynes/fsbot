from ctypes import Structure


__all__ = [
    'struct_factory',
    'BaseStructure'
]


class BaseStructure(Structure):

    def __init__(self, **kwargs):
        """
        ctypes.Structure with integrated defaults.

        Subclass this and create a _defaults_ dict attribute containig all defaults.

        :param kwargs: values different to defaults
        :type kwargs: dict

        https://stackoverflow.com/questions/7946519/default-values-in-a-ctypes-structure
        """
        values = type(self)._defaults_.copy()
        
        for (key, val) in kwargs.items():
            values[key] = val

        super(BaseStructure, self).__init__(**values)



def struct_factory(func):
    # type: (function) -> function
    """
    Decorator for creating ctype Structures
    
    Exists so ctype Structure _field_ elements can be dynamically assigned.

    Functions decorated by this decorator is expected to have following naming convention:
    <name>_struct. Example: pilot_struct. More than one '_' can be used. The reason is 
    so struct names can be easily deduced mainly when type hinting.
    """
    def factory_wrapper(*args, **kwargs):
        
        # Capitalise struct name
        struct_name = ''.join(sect.capitalize() for sect in func.__name__.split('_'))
        
        # Ger function return
        func_return = func(*args, **kwargs)
        
        # Return struct type with defaults
        return type(struct_name, (BaseStructure,), {"_fields_": func_return['fields'], "_defaults_": func_return['defaults']})
    
    return factory_wrapper
