from typing import Optional, Dict, List
import logging, sys, time
import pkg_resources
from os import path


__all__ = [
    '__version__',
    '__app_name__',
    '__url__',
    '__creator__',
    '__version_long__',
    '__version_medium__',
    '__version_short__',
    '__description__',
    '__user_agent__',
    'app_state',
    'log',
    'cfg',
    'init_config',
    'ConfigMissingOptionError',
    'ConfigMissingSectionError',
    'ConfigValueError',
]


# Python check
PYOK = sys.version_info.major >= 3

if PYOK:
    import configparser as ConfigParser
else:
    raise Exception('Requires Python >= 3!')

class ConfigValueError(Exception):
    pass

class ConfigMissingOptionError(ConfigParser.NoOptionError):
    pass

class ConfigMissingSectionError(ConfigParser.NoSectionError):
    pass


### Version information ###
__version__ = pkg_resources.require("fsbot")[0].version
__app_name__ = pkg_resources.require("fsbot")[0].project_name
__url__ = "https://bitbucket.org/vynes/fsbot"
__creator__ = "curvian"
__description__ = "IRC Bot"
__version_short__ = __app_name__ + ' ' + __version__
__version_medium__ = __app_name__ + ' ' + __version__ + ' - ' + __description__
__version_long__ = __app_name__ + ' ' + __version__ + ' - ' + __description__ + ' (' + __url__ + ')'
__user_agent__ = __app_name__ + ' ' + __version__


### Some app state info  ###
app_state = {}

### Config parsing ###
cfg = ConfigParser.RawConfigParser()

config_file_locations = [
    path.abspath('config.ini'),
    path.abspath(path.expanduser('~/.fsbot/config.ini')),
    path.abspath(path.expanduser('~/.config/fsbot.ini')),
    path.abspath(path.expanduser('~/.config/fsbot_config.ini')),
    path.abspath(path.expanduser('~/.config/fsbot/config.ini')),
    path.abspath(path.expanduser('~/.local/share/fsbot/config.ini'))
]

config_defaults = {
    # Headers used for scraping titles from websites etc.
    # Placed here so it can be set by users in config.ini if needed.
    'BROWSER_HEADERS': {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36',
        'Accept': 'application/json, text/plain, */*',
        'X-Requested-With': 'XMLHttpRequest',
        'Accept-Language': 'en-US,en;q=0.9',
        'Connection': 'keep-alive'
    },
    'POLLING': {
        'MinPollingLoopInterval': 10,
        'MinPollingScheduledInterval': 30,
        'FirstRunTimeOffset': 300,
        'FirstRunTimeIncrement': 1200
    },
    'CACHE': {
        'CacheDir': '~/.local/share/fsbot/cache'
    }
}


def set_config(userfile: Optional[str] = None):
    """
    If a config file is specified on the commandline and not found, no other locations will be 
    searched for a valid config file and fsbot will exit with an error.

    Default locations are searched for config files if --config/-c cli options not specified.
    """
    global cfg

    config_file_name = None

    if userfile is None:
        # Try default locations.
        for file_name in config_file_locations:
            if path.isfile(file_name):
                config_file_name = file_name
                break
        
        # Nothing found, quit.
        if config_file_name is None:
            print('Could not read any config file.')
            print('Tried the following locations: {}'.format(', '.join(config_file_locations)))
            sys.exit(2)
    else:
        config_file_name = path.abspath(path.expanduser(userfile))
        
        if not path.isfile(config_file_name):
            # Exit if user_file specified but not found:
            print('Could not read config file {}'.format(config_file_name))
            sys.exit(2)
   
    cfg.read(config_file_name)

    # Set some defaults.
    for section in config_defaults:
        if not section in cfg:
            cfg.add_section(section)
        for item, value in config_defaults[section].items():
            if not item in cfg[section]: cfg.set(section, item, value)


def init_config(args: List):
    """
    Set some some initial config vars, called early from main.
    """
    # Set some app state vars
    app_state['start_time'] = time.time()

    # Set config file
    if '--config' in args or '-c' in args:
        for i,v in enumerate(args):
            if v == '--config' or v == '-c':
                set_config(args[i+1])
                break
    else:
        set_config()

    # Set debug level
    if '--debug' in args or '-d' in args:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)


### Logging ###
# Configure logging
#log.basicConfig(format='[%(levelname)s] %(asctime)s %(message)s', level=log.INFO)

# create logger
log = logging.getLogger('fsbotdeflog')
log.setLevel(logging.NOTSET)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.NOTSET)

# create formatter
formatter = logging.Formatter('[%(levelname)s] %(asctime)s %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
log.addHandler(ch)
