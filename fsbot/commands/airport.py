import re
from argparse import Action

from fsbot import is_empty, log, cfg
from fsbot.commands import BaseCommand, CommandError, NoResultsError
from fsbot.exceptions import throw
from fsbot.airport.sources import source_registry
from fsbot.airport import Airport


__all__ = [
    'IcaoCommand',
    'RunwaysCommand',
    'AirportCommand',
    'SearchCommand',
    'NearestCommand'
]


def check_selected_source(source, command, parser):
    # type: (str, str, fsbot.commands.ArgumentParser) -> None
    """
    Check source against avaialble sources.

    Call parser's error method if not valid.
    """
    if source != None and source not in source_registry:
        parser.error('{} command valid sources:\n{}'.format(
                command, 
                '\n'.join('  [' + str(source['cls'].idx) + '] ' + source['cls'].name for source in list(source_registry.values()))
            )
        )


class ListSourcesAction(Action):
    """
    Custom action to make --list-sources behave like --help: print a list 
    of supported sources and exit.
    nargs=0 does the trick to make --list-sources not require an argument.
    """
    def __init__(self, option_strings, dest, nargs=0, **kwargs):
        super(ListSourcesAction, self).__init__(option_strings, dest, nargs=nargs, **kwargs)
        self.sources = source_registry
    
    def __call__(self, parser, namespace, values, option_string=None):
        # exit() overwritten for IRC subparser ArgumentParser in __init__.py
        # to throw CommandError with human_text context instead of spamming log with 
        # SystemExit traceback.
        sources_list = 'airport sources:\n{}'.format('\n'.join('  [' + str(self.sources[source]['cls'].idx) + '] ' + self.sources[source]['cls'].name + ' - ' + self.sources[source]['cls'].info for source in sorted(self.sources.keys())) if len(self.sources) > 0 else 'Nothing found.') 

        parser.exit('{}'.format(sources_list))


class IcaoCommand(BaseCommand):
    """
    IcaoCommand

    Use this Command class as an example for future Command classes.
        
    Command Arguments:
        mx_group:
            Specify a 'mx_group' key for mutually exclusive argument groups.
            Use a unique name for each argument group as the value.
            Start the command name with required_ to pass the required=True argument
                to add_mutually_exclusive_group() ArgumentParser method.


    Available through BaseCommand:
        max_results   :   as per config file or fallback
        origin        :   command origin 'irc' or 'cli'
        sep           :   output separator, set for origin
        lcap, rcap    :   output encapsulation brackets, set for origin
    """

    # Fallback max ICAOs
    fallback_max_icaos = 50
    fallback_max_icaos_irc = 5

    # Required - command name
    command = 'icao'

    # Required - Simple help text one-liner, no puncuation.
    help_text = 'Airport information by ICAO'

    # Arguments for this command.
    # Do not create --help or -h here. ArgumentParser will handle it for both cli and irc. 
    arguments = [
        {
            'names': ['icaos'],
            'options': {
                'metavar': 'ICAO',
                'nargs': '+',
                'help': 'airport ICAO(s)'
            }
        },
        {
            'names': ['-s', '--source'],
            'options': {
                'type': int,
                'metavar': 'SOURCE_ID',
                'help': 'airport data source (see --list-sources)'
            }
        },
        {
            'names': ['-l', '--list-sources'],
            'options': {
                'action': ListSourcesAction,
                'help': 'lists available airport data sources for use with --source'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):

        super(IcaoCommand, self).__init__(args=args, parsed_args=parsed_args)

        # Set maximum number of icaos accepted
        if self.origin == 'irc':
            self.max_icaos = cfg.getint('COMMAND_ICAO', 'MaxICAOsIRC', fallback=self.fallback_max_icaos_irc)
        else:
            self.max_icaos = cfg.getint('COMMAND_ICAO', 'MaxICAOs', fallback=self.fallback_max_icaos)


        # Discover sources for airport data
        self.sources = source_registry


        # Check valid source selected
        check_selected_source(source=self.kwargs['source'], command=self.command, parser=self.parser)


        # Get result
        self.result = Airport(use_source_idx=self.kwargs['source']).select(self.kwargs['icaos'][:self.max_icaos])

        # No result response
        # human_error context of NoResultsError will be returned to IRC and 
        # CLI will terminate (or return 'cli_error' if exists or not None) 
        if is_empty(self.result): 
            raise throw(
                exc=NoResultsError('No airport data found in {}'.format(self.__class__.__name__)),
                context={
                    'human_error': 'No results.'
                }
            )

        # Set icaos not found
        self.icaos_not_found = set([i.upper() for i in self.kwargs['icaos']]).difference(set([a.icao.upper() for a in self.result]))


    @property
    def response_value(self):
        """
        Return values of these response_* methods are __string__ representations of 
        the command class, depending on set verbosity level.
        """
        return '\n'.join(
            '{icao} ({iso_country}) {name} - {num_runways} runway{rs}{runway_length}{elevation}'.format(
                icao=airport.icao, 
                iso_country=airport.iso_country, 
                type=airport.type, 
                name=airport.name, 
                num_runways=len(airport.runways),
                rs='' if len(airport.runways) == 1 else 's',
                runway_length=', longest rwy: {:.0f} ft ({:.0f} m)'.format(airport.max_runway_length_ft, airport.max_runway_length_m),
                elevation=', elevation: {:.0f} ft ({:.0f} m)'.format(airport.elevation_ft, airport.elevation_m)
            ) for airport in self.result) + ('\nNot found: ' + ','.join(self.icaos_not_found) if not is_empty(self.icaos_not_found) and self.origin == 'irc' else '')


    @property
    def response_concise(self):
        return '\n'.join(
            '{icao} ({iso_country}) {name} - {type}: {num_runways} runway{rs}{runway_length}{elevation}'.format(
                icao=airport.icao, 
                iso_country=airport.iso_country, 
                type=airport.type, 
                name=airport.name, 
                num_runways=len(airport.runways),
                rs='' if len(airport.runways) == 1 else 's',
                runway_length=', longest rwy: {:.2f} ft ({:.2f} m)'.format(airport.max_runway_length_ft, airport.max_runway_length_m),
                elevation=', elevation: {:.2f} ft ({:.2f} m)'.format(airport.elevation_ft, airport.elevation_m)
            ) for airport in self.result) + ('\nNot found: ' + ','.join(self.icaos_not_found) if not is_empty(self.icaos_not_found) else '')
        
        
    @property
    def response_verbose(self):
        return self.response_concise


class RunwaysCommand(BaseCommand):
    """
    RunwaysCommand

    """
    # Fallback max ICAOs
    fallback_max_icaos = 50
    fallback_max_icaos_irc = 3

    command = 'runways'

    help_text = 'Runways information by ICAO'

    arguments = [
        {
            'names': ['icaos'],
            'options': {
                'metavar': 'ICAO',
                'nargs': '+',
                'help': 'airport ICAO(s)'
            }
        },
        {
            'names': ['-s', '--source'],
            'options': {
                'type': int,
                'metavar': 'SOURCE_ID',
                'help': 'airport data source (see --list-sources)'
            }
        },
        {
            'names': ['-l', '--list-sources'],
            'options': {
                'action': ListSourcesAction,
                'help': 'lists available airport data sources for use with --source'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):
        super(RunwaysCommand, self).__init__(args=args, parsed_args=parsed_args)

        # Set maximum number of icaos accepted
        if self.origin == 'irc':
            self.max_icaos = cfg.getint('COMMAND_RUNWAYS', 'MaxICAOsIRC', fallback=self.fallback_max_icaos_irc)
        else:
            self.max_icaos = cfg.get('COMMAND_RUNWAYS', 'MaxICAOs', fallback=self.fallback_max_icaos)


        # Discover sources for airport data
        self.sources = source_registry


        # Check valid source selected
        check_selected_source(source=self.kwargs['source'], command=self.command, parser=self.parser)


        # Get result
        self.result = Airport(use_source_idx=self.kwargs['source']).select(self.kwargs['icaos'][:self.max_icaos])

        # human_error context of NoResultsError will be returned to IRC and 
        # CLI will terminate (or return 'cli_error' if exists or not None) 
        if is_empty(self.result): 
            raise throw(
                exc=NoResultsError('No airport data found in {}'.format(self.__class__.__name__)),
                context={
                    'human_error': 'No result.'
                }
            )

        # Set icaos not found
        self.icaos_not_found = set([i.upper() for i in self.kwargs['icaos']]).difference(set([a.icao.upper() for a in self.result]))


    @property
    def response_value(self):

        result = []

        for airport in self.result:
            
            if len(airport.runways) == 0:
                continue

            result.append('\n'.join(
                '{icao} {runway_a_number}/{runway_b_number} {surface_type} {length_ft:.0f}x{width_ft:.0f} ft ({length_m:.0f}x{width_m:.0f} m)'.format(
                    icao=runway.icao, 
                    runway_a_number=runway.runway_a_number, 
                    runway_b_number=runway.runway_b_number, 
                    surface_type=runway.surface_type, 
                    length_ft=runway.length_ft,
                    length_m=runway.length_m,
                    width_ft=runway.width_ft,
                    width_m=runway.width_m
                ) for runway in airport.runways)
            )

        return '\n'.join(result) + ('\nNot found: ' + ','.join(self.icaos_not_found) if not is_empty(self.icaos_not_found) and self.origin == 'irc' else '')


    @property
    def response_concise(self):

        result = []

        for airport in self.result:
            
            if len(airport.runways) == 0:
                continue

            result.append('\n'.join(
                '{icao} runway: {runway_a_number}/{runway_b_number}, surface: {surface_type}, length: {length_ft:.2f} ft ({length_m:.2f} m), width: {width_ft:.2f} ft ({width_m:.2f} m), airport elevation: {elevation_ft:.2f} ft ({elevation_m:.2f} m)'.format(
                    icao=runway.icao, 
                    runway_a_number=runway.runway_a_number, 
                    runway_b_number=runway.runway_b_number, 
                    surface_type=runway.surface_type, 
                    length_ft=runway.length_ft,
                    length_m=runway.length_m,
                    width_ft=runway.width_ft,
                    width_m=runway.width_m,
                    elevation_ft=airport.elevation_ft,
                    elevation_m=airport.elevation_m
                ) for runway in airport.runways)
            )

        return '\n'.join(result) + ('\nNot found: ' + ','.join(self.icaos_not_found) if not is_empty(self.icaos_not_found) else '')


    @property
    def response_verbose(self):
        return self.response_concise        


class AirportCommand(IcaoCommand):
    """
    IcaoCommand alias
    """
    command = 'airport'
    help_text = 'Alias for icao command'


class SearchCommand(BaseCommand):

    command = 'search'

    help_text = 'Search for airports'

    arguments = [
        {
            'names': ['search_term'],
            'options': {
                'nargs': '*',
                'metavar': 'SEARCH_TERM',
                'help': 'term to search for in airport names'
            }
        },
        {
            'names': ['-a', '--icao'],
            'options': {
                'type': str,
                'metavar': 'ICAO_PATTERN',
                'help': 'Airport ICAO, wildcards supported (?,*,%%)'
            }
        },
        {
            'names': ['-i', '--iso-country'],
            'options': {
                'type': str,
                'metavar': 'ISO_COUNTRY',
                'help': 'country ISO code, examples: US, RU, RO, DE, NL, ZA'
            }
        },
        {
            'names': ['-r', '--runway-length'],
            'options': {
                'type': str,
                'metavar': 'RUNWAY_LENGTH_LIMITS',
                'help': 'runway length limits in ft. Example: -l 4000-6000'
            }
        },
        {
            'names': ['-e', '--elevation'],
            'options': {
                'type': str,
                'metavar': 'AIRPORT_ELEVATION_LIMITS',
                'help': 'airport elevation limits in ft. Example: -e 100-800'
            }
        },
        {
            'names': ['-s', '--source'],
            'options': {
                'type': int,
                'metavar': 'SOURCE_ID',
                'help': 'airport data source (see --list-sources)'
            }
        },
        {
            'names': ['-l', '--list-sources'],
            'options': {
                'action': ListSourcesAction,
                'help': 'lists available airport data sources for use with --source'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):
        super(SearchCommand, self).__init__(args=args, parsed_args=parsed_args)

        self.sources = source_registry

        self.sep = '\n'

        self.flag_runway_length = False
        self.flag_airport_elevation = False

        # Check valid source selected
        check_selected_source(source=self.kwargs['source'], command=self.command, parser=self.parser)

        # Check if a search term is specified
        if len(self.kwargs['search_term']) == 0 and [self.kwargs['icao'],self.kwargs['iso_country'],self.kwargs['runway_length'],self.kwargs['elevation']].count(None) == 4:
            self.parser.error('No search term specified.')

        # Runway length limits
        runway_length_limits = [None, None]
        if self.kwargs['runway_length'] != None: 
            range_sep = '-'
            for rsep in [':', '-']:
                if rsep in self.kwargs['runway_length']:
                    range_sep = rsep
                    break
            user_runway_length_limits = self.kwargs['runway_length'].split(range_sep, 1) 
            
            try:
                for i,l in enumerate(user_runway_length_limits): runway_length_limits[i] = int(l)
                self.flag_runway_length = True
            except (TypeError, ValueError, IndexError):
                self.parser.error('runway length must be either min-max range, ex: 1000-5000, or min value, ex: 1000')
        
        # Airport elevation limits
        airport_elevation_limits = [None, None]
        if self.kwargs['elevation'] != None: 
            range_sep = ''
            for rsep in [':', '-']:
                if rsep in self.kwargs['runway_length']:
                    range_sep = rsep
                    break
            user_airport_elevation_limits = self.kwargs['elevation'].split(range_sep, 1)
            
            try:
                for i,l in enumerate(user_airport_elevation_limits): airport_elevation_limits[i] = int(l)
                self.flag_airport_elevation = True
            except (TypeError, ValueError, IndexError):
                self.parser.error('elevation must be either min-max range, ex: 1000-5000, or min value, ex: 1000')

        # ICAO pattern
        icao_term = self.kwargs.get('icao')
        if icao_term != None:
            if isinstance(re.search('[^*?%]+?', self.kwargs.get('icao')), re.Match):
                icao_term=self.kwargs.get('icao')
            else:
                self.parser.error(f'Not a valid pattern: \'{self.kwargs.get("icao")}\'')


        # ISO Country
        country = self.kwargs['iso_country'] if self.kwargs['iso_country'] else None

        # Get result
        self.result = Airport(use_source_idx=self.kwargs['source']).search(
            search_term=' '.join(self.kwargs['search_term']),
            icao_term = icao_term,
            country=country,
            min_elevation=airport_elevation_limits[0],
            max_elevation=airport_elevation_limits[1],
            min_rwy_length=runway_length_limits[0], 
            max_rwy_length=runway_length_limits[1],
            limit=self.max_results
        )

        # Check result
        if is_empty(self.result): 
            raise throw(
                exc=NoResultsError('No airport data found in {}'.format(self.__class__.__name__)),
                context={
                    'human_error': 'No result.'
                }
            )


    @property
    def response_value(self):
        irc_title = '{} hits:\n'.format(len(self.result)) if self.origin == 'irc' else ''

        return irc_title + self.sep.join('{icao} ({iso_country}) {name} - {num_runways} runway{rs}{runway_length}{elevation}'.format(
                icao=airport.icao, 
                iso_country=airport.iso_country, 
                type=airport.type, 
                name=airport.name, 
                num_runways=len(airport.runways),
                rs='' if len(airport.runways) == 1 else 's',
                runway_length=', longest rwy: {:.2f} ft ({:.2f} m)'.format(airport.max_runway_length_ft, airport.max_runway_length_m),
                elevation=', elevation: {:.2f} ft ({:.2f} m)'.format(airport.elevation_ft, airport.elevation_m)
            ) for airport in self.result)


    @property
    def response_concise(self):
        irc_title = '{} hits:\n'.format(len(self.result)) if self.origin == 'irc' else ''

        return irc_title + self.sep.join('{icao} ({iso_country}) {name} - {type}: {num_runways} runway{rs}{runway_length}{elevation}'.format(
                icao=airport.icao, 
                iso_country=airport.iso_country, 
                type=airport.type, 
                name=airport.name, 
                num_runways=len(airport.runways),
                rs='' if len(airport.runways) == 1 else 's',
                runway_length=', longest rwy: {:.2f} ft ({:.2f} m)'.format(airport.max_runway_length_ft, airport.max_runway_length_m),
                elevation=', elevation: {:.2f} ft ({:.2f} m)'.format(airport.elevation_ft, airport.elevation_m)
            ) for airport in self.result)
        
        
    @property
    def response_verbose(self):
        return self.response_concise


class NearestCommand(BaseCommand):

    command = 'nearest'

    help_text = 'Find airports in vicinity of specified airport'

    arguments = [
        {
            'names': ['icao'],
            'options': {
                'type': str,
                'metavar': 'ICAO',
                'help': 'Airport ICAO'
            }
        },
        {
            'names': ['-r', '--radius'],
            'options': {
                'type': float,
                'metavar': 'RADIUS',
                'default': 162,
                'help': 'radius in NM for calculating proximity bounding coordinates'
            }
        },
        {
            'names': ['-m', '--minimum-distance'],
            'options': {
                'type': float,
                'metavar': 'MIN_DISTANCE',
                'default': 1,
                'help': 'constrain result to airports with at least MIN_DISTANCE in NM from given airport.'
            }
        },
        {
            'names': ['-M', '--max-results'],
            'options': {
                'type': str,
                'metavar': 'RESULT_LIMIT',
                'help': 'maximum number of results to return'
            }
        },
        {
            'names': ['-s', '--source'],
            'options': {
                'type': int,
                'metavar': 'SOURCE_ID',
                'help': 'airport data source (see --list-sources)'
            }
        },
        {
            'names': ['-l', '--list-sources'],
            'options': {
                'action': ListSourcesAction,
                'help': 'lists available airport data sources for use with --source'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):

        super(NearestCommand, self).__init__(args=args, parsed_args=parsed_args)

        self.sources = source_registry

        # Check valid source selected
        check_selected_source(source=self.kwargs['source'], command=self.command, parser=self.parser)

        proximity_radius = self.kwargs['radius'] if self.kwargs['minimum_distance'] <= 1 else self.kwargs['minimum_distance'] + 162

        self.result = Airport(use_source_idx=self.kwargs['source']).nearest(
            icao=self.kwargs['icao'],
            proximity_radius=proximity_radius,
            min_distance=self.kwargs['minimum_distance'],
            nmi=True
        )

        if is_empty(self.result):
            raise throw(
                exc=NoResultsError('No airport data found in {}'.format(self.__class__.__name__)),
                context={
                    'human_error': 'Not found.'
                }
            )


    @property
    def response_value(self):
        return self.sep.join('{} {:.2f} NM ({:.2f} km)'.format(airport[0], airport[1], airport[2]) for airport in self.result[:self.max_results])


    @property
    def response_concise(self):
        return self.response_value
        
        
    @property
    def response_verbose(self):
        return self.response_value
