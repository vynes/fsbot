from argparse import Action

from fsbot import is_empty, log, cfg
from fsbot.commands import BaseCommand, CommandError, NoResultsError
from fsbot.exceptions import throw
from fsbot.mp.sources import source_registry
from fsbot.mp import MP
from fsbot.maths import convert_unit
from fsbot.airport import Airport


__all__ = [
    'MPCommand'
]


def check_selected_source(source, command, parser):
    # type: (str, str, fsbot.commands.ArgumentParser) -> None
    """
    Check source against avaialble sources.

    Call parser's error method if not valid.
    """
    if source != None and source not in source_registry:
        parser.error('{} command valid sources:\n{}'.format(
                command, 
                '\n'.join('  [' + str(source['cls'].idx) + '] ' + source['cls'].name for source in list(source_registry.values()))
            )
        )


class ListSourcesAction(Action):
    """
    Custom action to make --list-sources behave like --help: print a list 
    of supported sources and exit.
    nargs=0 does the trick to make --list-sources not require an argument.
    """
    def __init__(self, option_strings, dest, nargs=0, **kwargs):
        super(ListSourcesAction, self).__init__(option_strings, dest, nargs=nargs, **kwargs)
        self.sources = source_registry
    
    def __call__(self, parser, namespace, values, option_string=None):
        # exit() overwritten for IRC subparser ArgumentParser in __init__.py
        # to throw CommandError with human_text context instead of spamming log with 
        # SystemExit traceback.
        sources_list = 'mp sources:\n{}'.format('\n'.join('  [' + str(self.sources[source]['cls'].idx) + '] ' + self.sources[source]['cls'].name + ' - ' + self.sources[source]['cls'].info for source in sorted(self.sources.keys())) if len(self.sources) > 0 else 'Nothing found.') 

        parser.exit('{}'.format(sources_list))


class MPCommand(BaseCommand):
    """
    MPCommand

    """
    # Required. command name
    command = 'mp'

    # Required. Simple one-liner, no puncuation.
    help_text = 'Search MP servers for pilot information'

    # Arguments for this command.
    # Do not create --help or -h here. ArgumentParser will handle it for both cli and irc. 
    arguments = [
        {
            'names': ['-p', '--pilot-callsign'],
            'options': {
                'type': str,
                'metavar': 'CALLSIGN',
                'help': 'filter: find the pilot with callsign CALLSIGN'
            }
        },
        {
            'names': ['-f', '--find-callsign'],
            'options': {
                'type': str,
                'metavar': 'CALLSIGN',
                'help': 'filter: search for pilots with callsigns like CALLSIGN'
            }
        },
        {
            'names': ['-i', '--near-icao'],
            'options': {
                'type': str,
                'metavar': 'ICAO',
                'help': 'filter: search for pilots around ICAO'
            }
        },
        {
            'names': ['-t', '--client-type'],
            'options': {
                'type': str,
                'metavar': 'TYPE',
                'help': 'filter: search by client type (atc, pilot..)'
            }
        },
        {
            'names': ['-a', '--aircraft'],
            'options': {
                'type': str,
                'metavar': 'AIRCRAFT',
                'help': 'filter: search by aircraft name'
            }
        },
        {
            'names': ['-m', '--more'],
            'options': {
                'action': 'store_true',
                'help': 'Show more mp info if available. Takes longer, more queries are made...'
            }
        },
        {
            'names': ['-s', '--source'],
            'options': {
                'type': int,
                'metavar': 'SOURCE_ID',
                'help': 'airport data source (see --list-sources)'
            }
        },
        {
            'names': ['-l', '--list-sources'],
            'options': {
                'action': ListSourcesAction,
                'help': 'lists available airport data sources for use with --source'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):

        super(MPCommand, self).__init__(args=args, parsed_args=parsed_args)

        # Discover sources for airport data
        self.sources = source_registry


        # Check valid source selected
        check_selected_source(source=self.kwargs['source'], command=self.command, parser=self.parser)
        

        context_kwargs = {
            'callsign': self.kwargs['pilot_callsign'], 
            'find_callsign': self.kwargs['find_callsign'], 
            'near_icao': self.kwargs['near_icao'], 
            'client_type': self.kwargs['client_type'],
            'aircraft': self.kwargs['aircraft'],
            'more': self.kwargs['more']
        }


        # Get result
        self.result = MP(use_source_idx=self.kwargs['source']).search(**context_kwargs)


        # Contextual 'no result' response
        if is_empty(self.result): 
            no_result_context = ''

            for arg in context_kwargs:
                if context_kwargs[arg] is not None:
                    if arg == 'callsign':
                        no_result_context += 'Pilot "{}" not found.'.format(context_kwargs[arg])
                        break
                    if arg == 'find_callsign':
                        no_result_context += 'Pilot callsign containing "{}" not found.'.format(context_kwargs[arg])
                        break
                else:
                    continue

            # human_error context of NoResultsError will be returned to IRC and 
            # CLI will terminate (or return 'cli_error' if exists or not None) 
            raise throw(
                exc=NoResultsError('No mp data found in {}'.format(self.__class__.__name__)),
                context={
                    'human_error':  no_result_context if no_result_context else 'No result.'
                }
            )


    @property
    def response_value(self):
        if len(self.result) == 1:
            return self.response_verbose

        return ', '.join('{}{}'.format(
            pilot.callsign,
            ' ({:.2f} NM from {})'.format(convert_unit(pilot.reference_point_value, 'm', 'NM'), pilot.reference_point_name) if self.kwargs['near_icao'] else ''
        ) for pilot in self.result)

        
    @property
    def response_concise(self):
        if len(self.result) == 1:
            return self.response_verbose

        return '{} online:\n'.format(len(self.result)) + ', '.join('{}{}'.format(
            pilot.callsign,
            ' ({:.2f} NM from {})'.format(convert_unit(pilot.reference_point_value, 'm', 'NM'), pilot.reference_point_name) if self.kwargs['near_icao'] else ''
        ) for pilot in self.result)
    

    @property
    def response_verbose(self):

        result = []

        for pilot in self.result[:self.max_results]:

            if pilot.latitude != -9999 and pilot.longitude != -9999:
                nearest_airports = Airport().nearest(proximity_radius=600, limit=5, coords=[pilot.latitude, pilot.longitude])
                try:
                    nearest = '{}({:.0f} NM)'.format(nearest_airports[0][0], nearest_airports[0][1])
                except IndexError:
                    nearest = 'n/a'
            else:
                nearest = 'n/a'

            result.append('callsign: {}{}, type: {}, model: {}, hdg: {:.0f}, alt: {:.0f}, depart: {}, dest: {}, nearest icao: {}, connected: {}'.format(
                    pilot.callsign,
                    ' ({:.2f} NM from {})'.format(convert_unit(pilot.reference_point_value, 'm', 'NM'), pilot.reference_point_name) if self.kwargs['near_icao'] else '',
                    pilot.client_type,
                    pilot.aircraft_info,
                    pilot.heading,
                    pilot.altitude,
                    pilot.planned_departure_airport,
                    pilot.planned_destination_airport,
                    nearest,
                    pilot.start_time
                )
            )

        return '\n'.join(result)
