import configparser
from argparse import Action
from html import unescape

from fsbot import log, is_empty, is_number, cfg
from fsbot.commands import BaseCommand, CommandError, NoResultsError
from fsbot.ai import Query
from fsbot.ai.sources import source_registry, AiInvalidRequestError
from fsbot.exceptions import throw


__all__ = [
    'AiCommand'
]


def check_selected_source(source, command, parser):
    # type: (str, str, fsbot.commands.ArgumentParser) -> None
    """
    Check source against avaialble sources.

    Call parser's error method if not valid.
    """
    if source != None and source not in source_registry:
        parser.error('{} command valid sources:\n{}'.format(
                command, 
                '\n'.join('  [' + str(source['cls'].idx) + '] ' + source['cls'].name for source in list(source_registry.values()))
            )
        )


class ListSourcesAction(Action):
    """
    Custom action to make --list-sources behave like --help: print a list 
    of supported sources and exit.
    nargs=0 does the trick to make --list-sources not require an argument.
    """
    def __init__(self, option_strings, dest, nargs=0, **kwargs):
        super(ListSourcesAction, self).__init__(option_strings, dest, nargs=nargs, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        # exit() overwritten for IRC subparser ArgumentParser in __init__.py
        # to throw CommandError with human_text context instead of spamming log with 
        # SystemExit traceback.
        sources = source_registry

        sources_list = 'ai sources:\n{}'.format('\n'.join('  [' + str(sources[source]['cls'].idx) + '] ' + sources[source]['cls'].name + ' - ' + sources[source]['cls'].info for source in sorted(sources.keys())) if len(sources) > 0 else 'Nothing found.') 

        parser.exit('{}'.format(sources_list))


class AiCommand(BaseCommand):
    """AiCommand"""

    # # API to use: https://openrouter.ai/deepseek/deepseek-r1:free/api 
    # API KEY: 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
    # curl https://openrouter.ai/api/v1/chat/completions \
    # -H "Content-Type: application/json" \
    # -H "Authorization: Bearer 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" \
    # -d '{"model": "deepseek/deepseek-r1:free", "messages": [{"role": "user", "content": "explain this metar: FQMA 261900Z 15011KT 9999 FEW020 27/21 Q1012 NOSIG"}]}'

    command = 'ai'

    help_text = "Returns ai responses"

    arguments = [
        {
            'names': ['query'],
            'options': {
                'nargs': '+',
                'metavar': 'QUERY',
                'help': 'Question (words) for the AI'
            }
        },
        {
            'names': ['-s', '--source'],
            'options': {
                'type': int,
                'metavar': 'SOURCE_ID',
                'help': 'AI source to use (see --list-sources)'
            }
        },
        {
            'names': ['-l', '--list-sources'],
            'options': {
                'action': ListSourcesAction,
                'help': 'lists available AI sources for use with --source'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):

        super(AiCommand, self).__init__(args=args, parsed_args=parsed_args)

        # Discover sources
        self.sources = source_registry

        # Check valid source selected
        check_selected_source(source=self.kwargs['source'], command=self.command, parser=self.parser)

        if len(self.kwargs['query']) > 1:
            query = ' '.join(self.kwargs['query'])
        elif len(self.kwargs['query']) == 1:
            query = self.kwargs['query'][0]
        else:
            # should never happen
            query = self.kwargs['query']


        try:
            self.result = Query(use_source_idx=self.kwargs['source']).query(q=query, username=self.irc_username, channel=self.irc_channel)
        except AiInvalidRequestError as e:
            raise throw(
                exc=NoResultsError(f'{e}'),
                context={
                    'human_error': 'Invalid request - the source is retarded, probably.',
                    'cli_error': 'Invalid request - who knows.'
                }
            )

        if is_empty(self.result):
            raise throw(
                exc=NoResultsError('No response from ai: {}'.format(self.__class__.__name__)),
                context={
                    'human_error': 'No response :('
                }
            )

        log.debug(f'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX: {self.irc_username}: {self.irc_channel}')

    @property
    def response_value(self):
        return self.result


    @property
    def response_concise(self):
        return self.response_value


    @property
    def response_verbose(self):
        return self.response_value
