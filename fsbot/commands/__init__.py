from abc import ABCMeta, abstractmethod, abstractproperty
from importlib import import_module
from pkgutil import walk_packages
from inspect import isabstract as is_abstract, isclass as is_class, \
    getmembers as get_members
import argparse

from fsbot import log, cfg
from fsbot.exceptions import throw


__all__ = [
    'BaseCommand',
    'CommandError',
    'NoResultsError',
    'command_registry',
    'discover_commands',
    'parse_args'
]


class NoResultsError(Exception):
    """
    Use for commands with no results to return.

    human_error context of the exception will be 
    returned to irc users, cli_error to stdout 
    (or no return if cli_error does not exist or is None).
    """
    pass


class CommandError(Exception):
    """
    Use only for user errors in command / malformed commands
    not otherwise detected/handled by ArgumentParser.
    """
    pass


command_registry = {}


def discover_commands(package, recursively=True):
    """
    Automatically discover commands in the specified package.

    Parameters
    ----------
    package : Union[ModuleType, Text]
        Package path or reference.
    recursively : boolean
        If True, will descend recursively into sub-packages.

    Returns
    -------
    Dict[Text, 'CommandMeta']
        All commands discovered in the specified package, indexed by
        command name (note: not class name).
    
    # http://stackoverflow.com/a/25562415/
    """
    if isinstance(package, str):
        package = import_module(package) # type: ModuleType

    commands = {}

    for _, name, is_package in walk_packages(package.__path__):
        # Loading the module is good enough; the CommandMeta metaclass will
        # ensure that any commands in the module get registered.
        sub_package = import_module(package.__name__ + '.' + name)
        log.debug('sub_package: {}'.format(sub_package))

        # Index any command classes that we find.
        for (_, obj) in get_members(sub_package):
            if is_class(obj) and isinstance(obj, CommandMeta):
                
                command_name = getattr(obj, 'command')
                
                log.debug('command_name: {}'.format(command_name))
                
                if command_name:
                    commands[command_name] = {'cls': obj}

        if recursively and is_package:
            commands.update(discover_commands(sub_package))
        
    log.debug('commands: {}'.format(commands))

    return commands


def parse_args(parser, argument_definitions, arguments=None, parse_arguments=True):
    """
    Adds argument_definitions to parser and optionally parses the arguments 
    against the definitions.

    Parameters
    ----------
    parser : ArgumentParser
        argparse.ArgumentParser instance
    argument_definitions : Tuple[List[Dict]]
        A tuple of argument definition lists
    arguments : Optional[List[Str]]
        The arguments to be parsed
    parse_arguments : boolean
        Parse arguments if true. Set false to get access to a command's full help text 
        without parsing.

    Returns
    -------
    Optional[Dict]

    """
    mx_group = {}

    for definition_set in argument_definitions:

        # Add arguments
        for cmd in definition_set:
            # Create mutually exclusive groups
            # And add arguments in mutually exclusive groups
            if 'mx_group' in cmd and isinstance(cmd['mx_group'], str):
                if not mx_group.get(cmd['mx_group']):
                    # Check if mutually exclusive group has required flag set
                    required = True if cmd['mx_group'].split('_', 1)[0] == 'required' else False
                    # Create mutually exclusive group
                    mx_group[cmd['mx_group']] = parser.add_mutually_exclusive_group(required=required)
                # Add argument to group
                mx_group[cmd['mx_group']].add_argument(*cmd['names'], **cmd['options'])
            else:
                # Add arguments not part of mutually exclusive group
                parser.add_argument(*cmd['names'], **cmd['options'])      
    
    if parse_arguments:
        return vars(parser.parse_args(arguments))


class ArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        raise throw(
            exc=CommandError('Error: {}'.format(message)),
            context={
                'human_error': '{}'.format(message)
            }
        )

    def exit(self, message=None, status=0):
        raise throw(
            exc=CommandError('Error: {}'.format(message)),
            context={
                'human_error': '{}'.format(message)
            }
        )


class CommandMeta(ABCMeta):
    """
    Automatically register new command and enforce some properties.
    """
    # noinspection PyShadowingBuiltins
    
    required = ['arguments', 'command', 'help_text']

    def __init__(cls, what, bases=None, dict=None):
        super(CommandMeta, cls).__init__(what, bases, dict)

        if not is_abstract(cls):

            for prop in cls.required:
                if not hasattr(cls, prop):
                    raise NotImplementedError('must define {} in {}.'.format(prop, cls))

            command = getattr(cls, 'command')

            # All commands are enabled by default unless user disabled in config.
            disabled = cfg.get('COMMAND_' + command.upper(), 'disabled', fallback='no')

            if not disabled.lower() in ('1', 'y', 'yes', 'true'):
                command_registry[command] = {'cls': cls}


class BaseCommand(metaclass=CommandMeta):
    """
    Command classes must subclass this BaseCommand Class
    """

    command = None


    fallback_max_results = 4096
    fallback_irc_max_results = 10


    base_arguments = [
        {
            'names': ['-v', '--verbose'],
            'options': {
                'action': 'count',
                'help': 'set verbosity level'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):

        # Create an argument parser
        self.parser = ArgumentParser(
            add_help = False
        )

        self.irc_username = None
        self.irc_channel = None

        # Set some paramaters according to command origin, 
        # IRC or CLI.
        if isinstance(args, list) and '__origin_irc__' in args:
            # Sets max_results to config value or fallback value.
            try:
                self.max_results = cfg.getint('COMMAND_' + self.command.upper(), 'CommandIrcMaxResults', fallback=self.fallback_irc_max_results)
            except ValueError as e:
                log.error(f'Config error at COMMAND_{self.command.upper()}: {e}. Using fallback value: {self.fallback_irc_max_results}.')
                self.max_results = self.fallback_irc_max_results

            count_strip = 0
            for i in args:
                if i.startswith('__origin_irc_'):
                    count_strip = count_strip + 1
                    if i.startswith('__origin_irc_nick_'):
                        self.irc_username = i[18:][0:-2]
                    elif i.startswith('__origin_irc_channel_'):
                        self.irc_channel = i[21:][0:-2]


            # origin
            self.origin = 'irc'
            # seperator
            self.sep = ', '
            # encapsulation brackets
            self.lcap = '[' 
            self.rcap = ']' 

            # strip origin flag from args
            self.args = args[0:-count_strip]
        else:
            try:
                self.max_results = cfg.getint('COMMAND_' + self.command.upper(), 'CommandMaxResults', fallback=self.fallback_max_results)
            except ValueError as e:
                log.error(f'Config error at COMMAND_{self.command.upper()}: {e}. Using fallback value: {self.fallback_max_results}.')
                self.max_results = self.fallback_max_results

            # If 0 value is set in config, meaning unlimited, set max_results to 
            # None (a_list[:None] means all/unlimited) for cli mode.
            if self.max_results < 1: self.max_results = None

            # origin
            self.origin = 'cli'
            # seperator
            self.sep = '\n'
            # encapsulation brackets
            self.lcap = '' 
            self.rcap = '' 

            self.args = args

        self.kwargs = parse_args(self.parser, (self.base_arguments, self.arguments), self.args, parse_arguments=True) if not parsed_args else parsed_args


    def __str__(self):

        if not isinstance(self.verbosity, int):
            return str(self.response_value)

        if self.verbosity > 1:
            return str(self.response_verbose)

        if self.verbosity == 1:
            return str(self.response_concise)

        return str(self.response_value)


    @property
    def verbosity(self):
        # type: () -> Union(None, int)
        try:
            return self.kwargs['verbose']
        except (KeyError, IndexError, AttributeError):
            return None


    @abstractproperty
    def response_verbose(self):
        # type: () -> str
        """Format return for verbose output.

        Its return string is output of the instance __string__ method, depending on verbosity level.
        """
        raise NotImplementedError(
            'Not implemented in {cls}.'.format(cls=type(self).__name__),
        )


    @abstractproperty
    def response_concise(self):
        # type: () -> str
        """Format return for concise output.

        Its return string is output of the instance __string__ method, depending on verbosity level.
        """
        raise NotImplementedError(
            'Not implemented in {cls}.'.format(cls=type(self).__name__),
        )


    @abstractproperty
    def response_value(self):
        # type: () -> str
        """Format return for minimal output - typically console.

        Its return string is output of the instance __string__ method, depending on verbosity level.
        """
        raise NotImplementedError(
            'Not implemented in {cls}.'.format(cls=type(self).__name__),
        )


# Autodiscover commands in this package.
discover_commands(__name__)
