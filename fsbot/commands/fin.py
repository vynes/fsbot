import configparser
from argparse import Action
from html import unescape

from fsbot import log, is_empty, is_number, cfg
from fsbot.commands import BaseCommand, CommandError, NoResultsError
from fsbot.forex import Report
from fsbot.forex.sources import source_registry, ForexInvalidRequestError, ForexInvalidSymbolError
from fsbot.exceptions import throw


__all__ = [
    'ForexCommand'
]


def check_selected_source(source, command, parser):
    # type: (str, str, fsbot.commands.ArgumentParser) -> None
    """
    Check source against avaialble sources.

    Call parser's error method if not valid.
    """
    if source != None and source not in source_registry:
        parser.error('{} command valid sources:\n{}'.format(
                command, 
                '\n'.join('  [' + str(source['cls'].idx) + '] ' + source['cls'].name for source in list(source_registry.values()))
            )
        )


class ListSourcesAction(Action):
    """
    Custom action to make --list-sources behave like --help: print a list 
    of supported sources and exit.
    nargs=0 does the trick to make --list-sources not require an argument.
    """
    def __init__(self, option_strings, dest, nargs=0, **kwargs):
        super(ListSourcesAction, self).__init__(option_strings, dest, nargs=nargs, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        # exit() overwritten for IRC subparser ArgumentParser in __init__.py
        # to throw CommandError with human_text context instead of spamming log with 
        # SystemExit traceback.
        sources = source_registry

        sources_list = 'forex sources:\n{}'.format('\n'.join('  [' + str(sources[source]['cls'].idx) + '] ' + sources[source]['cls'].name + ' - ' + sources[source]['cls'].info for source in sorted(sources.keys())) if len(sources) > 0 else 'Nothing found.') 

        parser.exit('{}'.format(sources_list))


class ForexCommand(BaseCommand):
    """ForexCommand"""

    command = 'forex'

    help_text = "Returns forex data"

    fallback_precision = 4
    fallback_verbose_precision = 16

    arguments = [
        {
            'names': ['pairs'],
            'options': {
                'nargs': '+',
                'metavar': 'PAIR',
                'help': 'Space separated list of pairs in format <BASE><QUOTE> (<BASE>:<QUOTE> if non-standard symbols)'
            }
        },
        {
            'names': ['-x', '--multiply-with'],
            'options': {
                'type': float,
                'metavar': 'MULTIPLIER',
                'help': 'A number to multiply the result with.'
            }
        },
        {
            'names': ['-p', '--precision'],
            'options': {
                'type': int,
                'metavar': 'NUM_DECIMALS',
                'help': 'Number of decimals for output formatting.'
            }
        },
        {
            'names': ['-s', '--source'],
            'options': {
                'type': int,
                'metavar': 'SOURCE_ID',
                'help': 'Forex source to use (see --list-sources)'
            }
        },
        {
            'names': ['-l', '--list-sources'],
            'options': {
                'action': ListSourcesAction,
                'help': 'lists available forex sources for use with --source'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):

        super(ForexCommand, self).__init__(args=args, parsed_args=parsed_args)

        # Discover sources
        self.sources = source_registry

        # Check valid source selected
        check_selected_source(source=self.kwargs['source'], command=self.command, parser=self.parser)

        # Assign shorthand m to multiplier
        self.m = self.kwargs["multiply_with"]

        # Assign shorthands p,pv to precision, depending on verbosity or user input
        if is_number(self.kwargs["precision"]):
            self.pv = self.p = int(self.kwargs["precision"])
        else:
            self.p = cfg.get('COMMAND_FOREX', 'DefaultPrecision', fallback=self.fallback_precision)
            self.pv = cfg.get('COMMAND_FOREX', 'DefaultVerbosePrecision', fallback=self.fallback_verbose_precision)

        try:
            self.result = Report(use_source_idx=self.kwargs['source']).rates(pairs=self.kwargs['pairs'])
        except ForexInvalidRequestError as e:
            raise throw(
                exc=NoResultsError(f'{e}'),
                context={
                    'human_error': 'Invalid request - the source might not like the symbol(s) requested.',
                    'cli_error': 'Invalid request - the source might not like the symbol(s) requested.'
                }
            )
        except ForexInvalidSymbolError as e:
            herror = f'Invalid symbol/pair: {e.context["symbol"]}.'
            raise throw(
                exc=NoResultsError(f'{e}'),
                context={
                    'human_error': herror,
                    'cli_error': herror
                }
            )

        if is_empty(self.result):
            raise throw(
                exc=NoResultsError('No rates found in {}'.format(self.__class__.__name__)),
                context={
                    'human_error': 'Nothing found :(.'
                }
            )


    @property
    def response_value(self):
        result = []
        for x in self.result:
            result.append(f'{x.pair}: {x.ask:.{self.p}f}' + (f' (ANS x {self.m} = {(self.m*x.ask):.{self.p}f})' if self.m else ''))
        return ','.join(result)


    @property
    def response_concise(self):
        result = []
        for x in self.result:
            result.append(f'{x.pair}: {x.bid:.{self.p}f}/{x.ask:.{self.p}f}/{x.volume}' + (f' (ANS x {self.m} = {(self.m*x.ask):.{self.p}f})' if self.m else ''))
        return '\n'.join(result)


    @property
    def response_verbose(self):
        result = []
        for x in self.result:
            result.append(f'[close: {x.closetime}] pair: {x.pair}, bid: {x.bid:.{self.pv}f}, ask: {x.ask:.{self.pv}f}, vol24h: {x.volume}' + (f' (ANS x {self.m} = {(self.m*x.ask):.{self.pv}f})' if self.m else ''))
        return '\n'.join(result)
