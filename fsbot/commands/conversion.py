from argparse import Action

from fsbot import log, is_number
from fsbot.commands import BaseCommand, CommandError
from fsbot.maths import ConversionInputError, ConversionError, convert_unit, list_conversion_symbols
from fsbot.nav.morse import MorseConverter, MorseInputError
from fsbot.exceptions import throw


__all__ = [
    'ConvertCommand'
]


class ListUnitsAction(Action):
    """
    Custom action to make --list-units behave like --help: print a list 
    of supported symbols and exit.
    nargs=0 does the trick to make --list-units not require an argument.
    """
    def __init__(self, option_strings, dest, nargs=0, **kwargs):
        super(ListUnitsAction, self).__init__(option_strings, dest, nargs=nargs, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        # exit() overwritten for IRC subparser ArgumentParser in __init__.py
        # to throw CommandError with human_text context instead of spamming log with 
        # SystemExit traceback.
        parser.exit('{}'.format(list_conversion_symbols()))


class ConvertCommand(BaseCommand):
    """ConvertCommand"""

    command = 'convert'

    help_text = 'Unit converter'

    arguments = [
        {
            'names': ['carg_from_value'],
            'options': {
                'type': float,
                'metavar': 'FROM_VALUE',
                'help': 'value to convert from'
            }
        },
        {
            'names': ['carg_from_symbol'],
            'options': {
                'metavar': 'FROM_SYMBOL',
                'help': 'unit symbol to convert from'
            }
        },
        {
            'names': ['carg_to_symbol'],
            'options': {
                'metavar': 'TO_SYMBOL',
                'help': 'unit symbol to convert to'
            }
        },
        {
            'names': ['-l', '--list-units'],
            'options': {
                'action': ListUnitsAction,
                'help': 'get list of support conversion units'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):
        super(ConvertCommand, self).__init__(args=args, parsed_args=parsed_args)

        try:
            self.result = convert_unit(
                value=self.kwargs['carg_from_value'], 
                from_unit=self.kwargs['carg_from_symbol'], 
                to_unit=self.kwargs['carg_to_symbol'], 
                verbose=True
            )

        except ConversionInputError as e:
            raise throw(
                exc=CommandError(e),
                context={
                    'human_error': e,
                    'cli_error': e
                }
            )


    @property
    def response_value(self):
        return '{}'.format(self.result['answer'])

    
    @property
    def response_concise(self):
        return '{} {}'.format(self.result['answer'], self.kwargs['carg_to_symbol'])
    

    @property
    def response_verbose(self):
        return '{} {} is equal to {} {}'.format(
            float(self.kwargs['carg_from_value']), 
            self.result['meta']['from_unit']['singular'] if float(self.kwargs['carg_from_value']) == 1 else self.result['meta']['from_unit']['plural'],
            self.result['answer'], 
            self.result['meta']['to_unit']['singular'] if self.result['answer'] == 1 else self.result['meta']['to_unit']['plural']
        )


class MorseCommand(BaseCommand):
    """MorseCommand"""

    command = 'morse'

    help_text = 'Simple Morse Code converter (dit:\'.\', dah:\'_\', sep: <space>|<spacex2>|<spacex4>)'

    arguments = [
        {
            'names': ['input_text'],
            'options': {
                'type': str,
                'nargs': '+',
                'metavar': 'INPUT_TEXT',
                'help': 'input text to convert from/to, ex: "− −−− •−• − ••− •−• •"'
            }
        },
        {
            'names': ['-s', '--separator'],
            'options': {
                'type': str,
                'default': ' ',
                'help': 'output separator to use'
            }
        },
        {
            'names': ['-C', '--capitalize'],
            'options': {
                'action': 'store_true',
                'help': 'whether to capitalize sentences'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):

        super(MorseCommand, self).__init__(args=args, parsed_args=parsed_args)

        try:
            self.result = MorseConverter(input_text=' '.join(self.kwargs['input_text']), output_separator=self.kwargs['separator'], capitalize=self.kwargs['capitalize'])

        except MorseInputError as e:
            raise throw(
                exc=CommandError(e),
                context={
                    'human_error': e,
                    'cli_error': e
                }
            )


    @property
    def response_value(self):
        return '{}'.format(self.result)

    
    @property
    def response_concise(self):
        return self.response_value
    

    @property
    def response_verbose(self):
        return self.response_value


class CWCommand(MorseCommand):
    """
    MorseCommand alias
    """
    command = 'cw'
    help_text = 'alias for morse command'
