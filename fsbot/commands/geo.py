from fsbot import log, is_number, is_empty
from fsbot.commands import BaseCommand, CommandError, NoResultsError
from fsbot.maths import ConversionInputError, ConversionError, convert_unit, \
    list_conversion_symbols, geodist, bearing
from fsbot.utils import timezone_information, icao_timezone_information, sym_degrees
from fsbot.airport import get_airport_coordinates
from fsbot.exceptions import throw


__all__ = [
    'DistanceCommand',
    'BearingCommand',
    'TimeCommand'

]


class DistanceCommand(BaseCommand):
    """DistanceCommand"""

    command = 'distance'

    help_text = 'Great circle distance calculator'

    arguments = [
        {
            'names': ['from_icao'],
            'options': {
                'type': str,
                'metavar': 'FROM_ICAO',
                'help': 'ICAO or coordinates (decimal: lat,lon)'
            }
        },
        {
            'names': ['to_icao'],
            'options': {
                'type': str,
                'metavar': 'TO_ICAO',
                'help': 'ICAO or coordinates (decimal: lat,lon)'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):

        super(DistanceCommand, self).__init__(args=args, parsed_args=parsed_args)

        self.is_coordinates = False

        coordinates_from = ''.join(self.kwargs['from_icao'].split()).split(',')
        coordinates_to = ''.join(self.kwargs['to_icao'].split()).split(',')

        try:
            self.coordinates_from = [float(coordinates_from[0]), float(coordinates_from[1])]
            self.coordinates_to = [float(coordinates_to[0]), float(coordinates_to[1])]
            self.is_coordinates = True
        except (IndexError, ValueError):
            self.is_coordinates = False
        
        if self.is_coordinates == True:
            self.result = geodist(self.coordinates_from[0], self.coordinates_from[1], self.coordinates_to[0], self.coordinates_to[1])
        else:
            try:
                self.icao_coordinates = get_airport_coordinates([self.kwargs['from_icao'], self.kwargs['to_icao']])
            except ConversionInputError as e:
                raise throw(
                    exc=CommandError(e),
                    context={
                        'human_error': e,
                        'cli_error': e
                    }
                )
            if None in self.icao_coordinates.values():
                self.result = 'Coordinates not found for: '
                for icao in [self.kwargs['from_icao'], self.kwargs['to_icao']]:
                    if self.icao_coordinates[icao] == None:
                        self.result += icao.upper() + ' '
            else:
                self.result = geodist(self.icao_coordinates[self.kwargs['from_icao']][0], self.icao_coordinates[self.kwargs['from_icao']][1], self.icao_coordinates[self.kwargs['to_icao']][0], self.icao_coordinates[self.kwargs['to_icao']][1], 2)


    @property
    def response_value(self):
        if self.is_coordinates:
            return f'{convert_unit(self.result, "m", "NM"):.4f} NM'
        
        return '{}->{}: {:.2f} NM ({:.2f} km)'.format(
            self.kwargs['from_icao'].upper(), 
            self.kwargs['to_icao'].upper(), 
            convert_unit(self.result, 'm', 'NM'), 
            convert_unit(self.result, 'm', 'km'),
        ) if is_number(self.result) else self.result


    @property
    def response_concise(self):
        if self.is_coordinates:
            return '{:.4f} NM ({:.4f} km)'.format(convert_unit(self.result, 'm', 'NM'), convert_unit(self.result, 'm', 'km'))
        
        return '{}->{}: {:.2f} NM ({:.2f} km), {:.2f}{}'.format(
            self.kwargs['from_icao'].upper(), 
            self.kwargs['to_icao'].upper(), 
            convert_unit(self.result, 'm', 'NM'), 
            convert_unit(self.result, 'm', 'km'),
            bearing(
                self.icao_coordinates[self.kwargs['from_icao']][0], 
                self.icao_coordinates[self.kwargs['from_icao']][1], 
                self.icao_coordinates[self.kwargs['to_icao']][0], 
                self.icao_coordinates[self.kwargs['to_icao']][1]
            )[0],
            sym_degrees
        ) if is_number(self.result) else self.result
        

    @property
    def response_verbose(self):
        if self.is_coordinates:
            return '[{}] -> [{}]: {:.4f} NM ({:.4f} km)'.format(
                ','.join(str(cf) for cf in self.coordinates_from),
                ','.join(str(tc) for tc in self.coordinates_to),
                convert_unit(self.result, 'm', 'NM'), 
                convert_unit(self.result, 'm', 'km')
            )
        return self.response_concise


class BearingCommand(BaseCommand):
    """BearingCommand"""

    command = 'bearing'

    help_text = 'Initial bearing (on a great circle) of START_ICAO toward END_ICAO (or coordinates)'

    arguments = [
        {
            'names': ['start_icao'],
            'options': {
                'type': str,
                'metavar': 'START_ICAO',
                'help': 'Start ICAO (or coordinates, decimal)'
            }
        },
        {
            'names': ['end_icao'],
            'options': {
                'type': str,
                'metavar': 'END_ICAO',
                'help': 'End ICAO (or coordinates, decimal)'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):

        super(BearingCommand, self).__init__(args=args, parsed_args=parsed_args)

        self.is_coordinates = False

        coordinates_from = ''.join(self.kwargs['start_icao'].split()).split(',')
        coordinates_to = ''.join(self.kwargs['end_icao'].split()).split(',')

        try:
            self.coordinates_from = [float(coordinates_from[0]), float(coordinates_from[1])]
            self.coordinates_to = [float(coordinates_to[0]), float(coordinates_to[1])]
            self.is_coordinates = True
        except (IndexError, ValueError):
            self.is_coordinates = False

        if self.is_coordinates == True:
            self.result = bearing (
                coordinates_from[0], 
                coordinates_from[1], 
                coordinates_to[0], 
                coordinates_to[1]
            )
        else:
            try:
                icao_coordinates = get_airport_coordinates([self.kwargs['start_icao'], self.kwargs['end_icao']])
                
                if None in icao_coordinates.values():
                    self.result = 'Coordinates not found for: '
                    for icao in [self.kwargs['start_icao'], self.kwargs['end_icao']]:
                        if icao_coordinates[icao] == None:
                            self.result += icao.upper() + ' '
                else:    
                    self.result = bearing (
                        icao_coordinates[self.kwargs['start_icao']][0], 
                        icao_coordinates[self.kwargs['start_icao']][1], 
                        icao_coordinates[self.kwargs['end_icao']][0], 
                        icao_coordinates[self.kwargs['end_icao']][1]
                    )

            except TypeError as e:
                raise throw(
                    exc=CommandError(e),
                    context={
                        'human_error': e,
                        'cli_error': e
                    }
                )


    @property
    def response_value(self):
        return '{:.2f}{}'.format(self.result[0], sym_degrees) if isinstance(self.result, tuple) else self.result


    @property
    def response_concise(self):
        if self.is_coordinates:
            return '[{}] -> [{}]: {:.2f}{}'.format(
                ','.join(str(cf) for cf in self.coordinates_from), 
                ','.join(str(ct) for ct in self.coordinates_to),
                self.result[0],
                sym_degrees
            ) if isinstance(self.result, tuple) else self.result

        return '{}->{}: {:.2f}{}'.format(
            self.kwargs['start_icao'].upper(), 
            self.kwargs['end_icao'].upper(), 
            self.result[0],
            sym_degrees
        ) if isinstance(self.result, tuple) else self.result
        

    @property
    def response_verbose(self):
        if self.is_coordinates:
            return '[{}] -> [{}]: {:.2f}{} (initial) -> {:.2f}{} (final)'.format(
                ','.join(str(cf) for cf in self.coordinates_from), 
                ','.join(str(ct) for ct in self.coordinates_to),
                self.result[0],
                sym_degrees,
                self.result[1],
                sym_degrees
            ) if isinstance(self.result, tuple) else self.result
        
        return '{}->{}: {:.2f}{} (initial) -> {:.2f}{} (final)'.format(
            self.kwargs['start_icao'].upper(), 
            self.kwargs['end_icao'].upper(), 
            self.result[0],
            sym_degrees,
            self.result[1],
            sym_degrees
        ) if isinstance(self.result, tuple) else self.result


class TimeCommand(BaseCommand):
    """TimeCommand"""

    command = 'time'

    help_text = 'ICAO or coordinates timezone information'

    arguments = [
        {
            'names': ['icao'],
            'options': {
                'type': str,
                'metavar': 'ICAO',
                'help': 'Airport ICAO or coordinates (decimal)'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):

        super(TimeCommand, self).__init__(args=args, parsed_args=parsed_args)


        self.is_coordinates = False

        coordinates = ''.join(self.kwargs['icao'].split()).split(',')

        try:
            self.coordinates = [float(coordinates[0]), float(coordinates[1])]
            self.is_coordinates = True
        except (IndexError, ValueError):
            self.is_coordinates = False

        if self.is_coordinates == True:
            self.result = timezone_information(*coordinates)
        else:
            self.icao = self.kwargs['icao'].upper()

            try:
                self.result = icao_timezone_information(self.icao)
            except ConversionInputError as e:
                raise throw(
                    exc=CommandError(e),
                    context={
                        'human_error': e,
                        'cli_error': e
                    }
                )

        if is_empty(self.result) or not isinstance(self.result, dict):
            raise throw(
                exc=NoResultsError('No timezone data found in {}'.format(self.__class__.__name__)),
                context={
                    'human_error': 'No timezone data found for "{}".'.format(self.kwargs['icao'].upper())
                }
            )


    @property
    def response_value(self):
        if self.is_coordinates:
            return '[{coords}]: {time} {tzid}'.format(
                coords = ','.join(str(cf) for cf in self.coordinates),
                time = self.result.get('time') or '-',
                tzid = self.result.get('timezoneId') or '-'
            )
        return '{icao}: {time} {tzid}'.format(
            icao = self.icao, 
            time = self.result.get('time') or '-', 
            tzid = self.result.get('timezoneId') or '-'
        )


    @property
    def response_concise(self):
        if self.is_coordinates:
            return '[{coords}]: {time} {tzid}\n  GMT: {gmt}\n  DST: {dst}'.format(
                coords = ','.join(str(cf) for cf in self.coordinates),
                time = self.result.get('time') or '-',
                tzid = self.result.get('timezoneId') or '-',
                gmt = self.result.get('gmtOffset') or '-',
                dst = self.result.get('dstOffset') or '-'
        
            )
        
        return '{icao}: {time} {tzid}\n  GMT: {gmt}\n  DST: {dst}'.format(
            icao = self.icao,
            time = self.result.get('time') or '-',
            tzid = self.result.get('timezoneId') or '-',
            gmt = self.result.get('gmtOffset') or '-',
            dst = self.result.get('dstOffset') or '-'
        )


    @property
    def response_verbose(self):
        if self.is_coordinates:
            return '[{coords}] ({countrycode}): {time} {tzid} ({countryname})\n  GMT: {gmt}\n  DST: {dst}\n  Sunrise: {sunrise}\n  Sunset: {sunset}\n  coords: {lat},{lon}'.format(
                coords = ','.join(str(cf) for cf in self.coordinates),
                countrycode = self.result.get('countryCode') or '-',
                time = self.result.get('time') or '-',
                tzid = self.result.get('timezoneId') or '-',
                countryname = self.result.get('countryName') or '-',
                gmt = self.result.get('gmtOffset') or '-',
                dst = self.result.get('dstOffset') or '-',
                sunrise = self.result.get('sunrise') or '-',
                sunset = self.result.get('sunset') or '-',
                lat = self.result.get('lat') or '-',
                lon = self.result.get('lng') or '-'
            )

        return '{icao} ({countrycode}): {time} {tzid} ({countryname})\n  GMT: {gmt}\n  DST: {dst}\n  Sunrise: {sunrise}\n  Sunset: {sunset}\n  coords: {lat},{lon}'.format(
            icao = self.icao,
            countrycode = self.result.get('countryCode') or '-',
            time = self.result.get('time') or '-',
            tzid = self.result.get('timezoneId') or '-',
            countryname = self.result.get('countryName') or '-',
            gmt = self.result.get('gmtOffset') or '-',
            dst = self.result.get('dstOffset') or '-',
            sunrise = self.result.get('sunrise') or '-',
            sunset = self.result.get('sunset') or '-',
            lat = self.result.get('lat') or '-',
            lon = self.result.get('lng') or '-'
        )
