from argparse import Action
from urllib.parse import quote as url_quote

from fsbot import log, is_number, is_empty
from fsbot.commands import BaseCommand, CommandError, NoResultsError
from fsbot.glossary import search
from fsbot.exceptions import throw


__all__ = [
    'ConvertCommand'
]


class GlossCommand(BaseCommand):
    """GlossCommand"""

    command = 'gloss'

    help_text = 'Glossary of terms'

    arguments = [
        {
            'names': ['term'],
            'options': {
                'nargs': '+',
                'metavar': 'TERM',
                'help': 'Terms to search for.'
            }
        },
        {
            'names': ['-t', '--in-definition'],
            'options': {
                'action': 'store_true',
                'help': 'Search in definition text.'
            }
        },
        {
            'names': ['-e', '--exact-term'],
            'options': {
                'action': 'store_true',
                'help': 'Search for the exact term (case insensitive).'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):

        super(GlossCommand, self).__init__(args=args, parsed_args=parsed_args)

        self.term = ' '.join(self.kwargs['term'])

        try:
            if self.kwargs.get('in_definition') == True:
                self.result = search(gloss_term=None, description_term=self.term, limit=10)
            else:
                self.result = search(gloss_term=self.term, description_term=None, exact_term=self.kwargs['exact_term'], limit=self.max_results)

            if is_empty(self.result):
                raise throw(
                    exc=NoResultsError('No results for "{}".'.format(self.term)),
                    context={
                        'human_error': 'No results for "{}".'.format(self.term)
                    }
                )

        except KeyError as e:
            raise throw(
                exc=CommandError(e),
                context={
                    'human_error': e,
                    'cli_error': e
                }
            )


    @property
    def response_value(self):
        return self.response_concise

    
    @property
    def response_concise(self):
        return '\n'.join('[{}]: {}'.format(a.term, a.description) for a in self.result[:self.max_results])
    

    @property
    def response_verbose(self):
        return self.response_concise
