import argparse
from urllib.parse import urlparse

from fsbot import log
from fsbot.validation import is_empty
from fsbot.commands import BaseCommand, CommandError, NoResultsError
from fsbot.exceptions import throw
from fsbot.teamspeak import session_info, source_registry, TsServerError


__all__ = [
    'TsCommand'
]


def check_selected_source(idx: int, command: str, parser: argparse.ArgumentParser) -> None:
    """
    Check source against avaialble sources.
    Call parser's error method if not valid.
    """
    if idx not in source_registry:
        parser.error('{} command valid sources:\n{}'.format(
                command,
                '\n'.join('  [' + str(sidx) + '] ' + source_registry[sidx]['name'] for sidx in source_registry)
            )
        )


class ListSourcesAction(argparse.Action):
    """
    Custom action to make --list-sources behave like --help: print a list 
    of supported sources and exit.
    nargs=0 does the trick to make --list-sources not require an argument.
    """
    def __init__(self, option_strings, dest, nargs=0, **kwargs):
        super(ListSourcesAction, self).__init__(option_strings, dest, nargs=nargs, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        """
        exit() overwritten for IRC subparser ArgumentParser in __init__.py
        to throw CommandError with human_text context instead of spamming log with 
        SystemExit traceback.
        """
        sources_list = 'teamspeak sources:\n{}'.format(
            '\n'.join(
                '  [' + str(sidx) + '] ' + source_registry[sidx]['name'] + ' - ' + urlparse(source_registry[sidx]['base_url']).netloc.split(':',-1)[0] for sidx in sorted(source_registry.keys())) if len(source_registry) > 0 else 'Nothing found.')
        parser.exit(sources_list)


class TsCommand(BaseCommand):
    """TsCommand"""
    command = 'ts'

    help_text = 'Teamspeak server session query command'

    arguments = [
        {
            'names': ['-s', '--source'],
            'options': {
                'type': int,
                'metavar': 'SOURCE_ID',
                'default': 1,
                'help': 'Teamspeak server sources (see --list-sources). Default: 1.'
            }
        },
        {
            'names': ['-l', '--list-sources'],
            'options': {
                'action': ListSourcesAction,
                'help': 'List available Teamspeak servers (for use with --source)'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):
        super().__init__(args=args, parsed_args=parsed_args)

        # Check valid source selected
        check_selected_source(idx=self.kwargs['source'], command=self.command, parser=self.parser)

        try:
            self.result = session_info(self.kwargs['source'])
            log.debug(self.result)

        except TsServerError as e:
            raise throw(
                exc=NoResultsError(e),
                context={
                    'cli_error': e,
                    'human_error': 'error/offline'
                }
            )


    @property
    def response_value(self):
        if self.result is None or is_empty(self.result.get('body')):
            result = 'No users found.'
        else:
            result = ', '.join(u['client_nickname'] for u in self.result['body'])

        return result


    @property
    def response_concise(self):
        return self.response_value


    @property
    def response_verbose(self):
        return self.response_value
