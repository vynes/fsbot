from argparse import Action

from fsbot import cfg, log, is_empty
from fsbot.commands import BaseCommand, CommandError, NoResultsError
from fsbot.weather import Report
from fsbot.weather.sources import source_registry
from fsbot.exceptions import throw
from fsbot.weather import tweak_raw_taf


__all__ = [
    'MetarCommand',
    'TafCommand',
    'WxrCommand'
]


def check_selected_source(source, command, parser):
    # type: (str, str, fsbot.commands.ArgumentParser) -> None
    """
    Check source against avaialble sources.

    Call parser's error method if not valid.
    """
    if source != None and source not in source_registry:
        parser.error('{} command valid sources:\n{}'.format(
                command, 
                '\n'.join('  [' + str(source['cls'].idx) + '] ' + source['cls'].name for source in list(source_registry.values()))
            )
        )


class ListSourcesAction(Action):
    """
    Custom action to make --list-sources behave like --help: print a list 
    of supported sources and exit.
    nargs=0 does the trick to make --list-sources not require an argument.
    """
    def __init__(self, option_strings, dest, nargs=0, **kwargs):
        super(ListSourcesAction, self).__init__(option_strings, dest, nargs=nargs, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        # exit() overwritten for IRC subparser ArgumentParser in __init__.py
        # to throw CommandError with human_text context instead of spamming log with 
        # SystemExit traceback.
        sources_list = 'weather sources:\n{}'.format('\n'.join('  [' + str(source_registry[source]['cls'].idx) + '] ' + source_registry[source]['cls'].name + ' - ' + source_registry[source]['cls'].info for source in sorted(source_registry.keys())) if len(source_registry) > 0 else 'Nothing found.') 

        parser.exit('{}'.format(sources_list))


class MetarCommand(BaseCommand):
    """MetarCommand"""

    command = 'metar'

    help_text = "Returns METAR data for given ICAOs"

    fallback_irc_max_icao_number = 3
    fallback_max_icao_number = 3

    arguments = [
        {
            'names': ['icaos'],
            'options': {
                'metavar': 'ICAO',
                'nargs': '+',
                'help': 'airport ICAO(s)'
            }
        },
        {
            'names': ['-s', '--source'],
            'options': {
                'type': int,
                'metavar': 'SOURCE_ID',
                'help': 'weather service to use (see --list-sources)'
            }
        },
        {
            'names': ['-l', '--list-sources'],
            'options': {
                'action': ListSourcesAction,
                'help': 'lists available weather sources for use with --source'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):

        super(MetarCommand, self).__init__(args=args, parsed_args=parsed_args)

        # Discover sources for airport data
        self.sources = source_registry

        # Check valid source selected
        check_selected_source(source=self.kwargs['source'], command=self.command, parser=self.parser)

        # Set maximum number of icaos accepted
        if self.origin == 'irc':
            self.max_icao_number = cfg.getint(f'COMMAND_{self.command.upper()}', 'MaxICAOsIRC', fallback=self.fallback_irc_max_icao_number)
        else:
            self.max_icao_number = cfg.getint(f'COMMAND_{self.command.upper()}', 'MaxICAOs', fallback=self.fallback_max_icao_number)

        self.report = Report(
            icaos=list(set(self.kwargs['icaos']))[:self.max_icao_number],
            use_source_idx=self.kwargs['source'],
            report_types='metar'
        ).find()
        
        if not is_empty(self.kwargs['icaos']) and (is_empty(self.report) or (len(self.report[0]) == 0 and len(self.report[1]) == 0)): 
            raise throw(
                exc=NoResultsError('No metar data found in {}'.format(self.__class__.__name__)),
                context={
                    'human_error': 'No metar results for {}.'.format(','.join(self.kwargs['icaos']))
                }
            )

        # Set icaos not found
        report_icaos_found = [metar.station_id.upper() for metar in self.report[0]['metar']] if 'metar' in self.report[0] else []
        report_icaos_nearest = [icao.upper() for icao in self.report[1]['metar']] if 'metar' in  self.report[1] else []

        self.icaos_not_found = set([i.upper() for i in self.kwargs['icaos']]).difference(set(report_icaos_found).union(set(report_icaos_nearest)))


    @property
    def response_value(self):
        result = ''

        if 'metar' in self.report[0] and len(self.report[0]['metar']) > 0:
            result += '\n'.join(metar.raw_text for metar in self.report[0]['metar'])

        if len(self.report[1]) > 0 and 'metar' in self.report[1]:
            result += ('\n') + '\n'.join('{} << {:.2f} NM >> '.format(icao.upper(), self.report[1]['metar'][icao]['distance_nm']) + \
                self.report[1]['metar'][icao]['result'].raw_text for icao in self.report[1]['metar'])

        return result.strip('\n') + ('\nNot found: ' + ','.join(self.icaos_not_found) if not is_empty(self.icaos_not_found) and self.origin == 'irc' else '')


    @property
    def response_concise(self):
        return self.response_verbose
        
    @property
    def response_verbose(self):

        result = ''
        # <visibility_statute_mi>6.21</visibility_statute_mi>
        # <flight_category>MVFR</flight_category>
        if 'metar' in self.report[0] and len(self.report[0]['metar']) > 0:
            result += '\n'.join('{raw_text}\n  {fcategory}\n  {visibility}\n  {obs_time}\n  {sta_elev}\n  {lat_lon}'.format(
                raw_text=metar.raw_text,
                fcategory='flight category: {}'.format(metar.flight_category),
                visibility='visibility: {:.2f} mi'.format(metar.visibility_statute_mi),
                obs_time='observation time: {}'.format(metar.observation_time),
                sta_elev='station elevation: {} m'.format(metar.elevation_m),
                lat_lon='station coordinates: {:.2f},{:.2f}'.format(metar.latitude, metar.longitude)
            ) for metar in self.report[0]['metar']) + '\n  source: {}'.format(self.report[0]['source'])


        if len(self.report[1]) > 0 and 'metar' in self.report[1]:
            result += ('\n') + '\n'.join('{} << {:.2f} NM, {:.0f}\u00b0T >> '.format(icao.upper(), self.report[1]['metar'][icao]['distance_nm'], self.report[1]['metar'][icao]['bearing']) + \
                '{raw_text}\n  {fcategory}\n  {visibility}\n  {obs_time}\n  {sta_elev}\n  {lat_lon}\n  {source}'.format(
                    fcategory='flight category: {}'.format(self.report[1]['metar'][icao]['result'].flight_category),
                    visibility='visibility: {:.2f} mi'.format(self.report[1]['metar'][icao]['result'].visibility_statute_mi),
                    raw_text=self.report[1]['metar'][icao]['result'].raw_text,
                    obs_time='observation time: {}'.format(self.report[1]['metar'][icao]['result'].observation_time),
                    sta_elev='station elevation: {} m'.format(self.report[1]['metar'][icao]['result'].elevation_m),
                    lat_lon='station coordinates: {:.2f},{:.2f}'.format(self.report[1]['metar'][icao]['result'].latitude, self.report[1]['metar'][icao]['result'].longitude),
                    source='source: {}'.format(self.report[1]['metar'][icao]['source'])
                ) for icao in self.report[1]['metar']) 


        return result.strip('\n') + ('\nNot found: ' + ','.join(self.icaos_not_found) if not is_empty(self.icaos_not_found) else '')


class TafCommand(BaseCommand):
    """
    TafCommand
    """
    command = 'taf'

    help_text = "Returns TAF data for given ICAOs"

    fallback_irc_max_icao_number = 3
    fallback_max_icao_number = 3

    arguments = MetarCommand.arguments


    def __init__(self, args=None, parsed_args=None):

        super(TafCommand, self).__init__(args=args, parsed_args=parsed_args)

        # Discover sources for airport data
        self.sources = source_registry

        # Check valid source selected
        check_selected_source(source=self.kwargs['source'], command=self.command, parser=self.parser)

        # Set maximum number of icaos accepted
        if self.origin == 'irc':
            self.max_icao_number = cfg.getint(f'COMMAND_{self.command.upper()}', 'MaxICAOsIRC', fallback=self.fallback_irc_max_icao_number)
        else:
            self.max_icao_number = cfg.getint(f'COMMAND_{self.command.upper()}', fallback=self.fallback_max_icao_number)

        self.report = Report(
            icaos=list(set(self.kwargs['icaos']))[:self.max_icao_number], 
            use_source_idx=self.kwargs['source'], 
            report_types='taf'
        ).find()

        if not is_empty(self.kwargs['icaos']) and (is_empty(self.report) or (len(self.report[0]) == 0 and len(self.report[1]) == 0)): 
            raise throw(
                exc=NoResultsError('No taf data found in {}'.format(self.__class__.__name__)),
                context={
                    'human_error': 'No taf results for {}.'.format(','.join(self.kwargs['icaos']))
                }
            )

        # Set icaos not found
        report_icaos_found = [taf.station_id.upper() for taf in self.report[0]['taf']] if 'taf' in self.report[0] else []
        report_icaos_nearest = [icao.upper() for icao in self.report[1]['taf']] if 'taf' in  self.report[1] else []

        self.icaos_not_found = set([i.upper() for i in self.kwargs['icaos']]).difference(set(report_icaos_found).union(set(report_icaos_nearest)))


    @property
    def response_value(self):
        result = ''

        if 'taf' in self.report[0] and len(self.report[0]['taf']) > 0:
            result += '\n'.join(tweak_raw_taf(taf.station_id, taf.raw_text) for taf in self.report[0]['taf'])

        if len(self.report[1]) > 0 and 'taf' in self.report[1]:
            result += ('\n') + '\n'.join('{} << {:.2f} NM >> {}'.format(
                icao.upper(), 
                self.report[1]['taf'][icao]['distance_nm'],
                tweak_raw_taf(self.report[1]['taf'][icao]['result'].station_id, self.report[1]['taf'][icao]['result'].raw_text)
            ) for icao in self.report[1]['taf'])

        return result.strip('\n') + ('\nNot found: ' + ','.join(self.icaos_not_found) if not is_empty(self.icaos_not_found) and self.origin == 'irc' else '')


    @property
    def response_concise(self):
        return self.response_verbose

        
    @property
    def response_verbose(self):
        result = ''

        if 'taf' in self.report[0] and len(self.report[0]['taf']) > 0:
            result += '\n'.join('{raw_text}\n  bulletin time: {obs_time}\n  station elevation: {sta_elev}m\n  station coordinates: {lat:.2f},{lon:.2f}\n  source: {source}'.format(
                raw_text=tweak_raw_taf(taf.station_id, taf.raw_text),
                obs_time=taf.bulletin_time,
                sta_elev=taf.elevation_m,
                lat=taf.latitude,
                lon=taf.longitude,
                source=self.report[0]['source']
            ) for taf in self.report[0]['taf'])

        if len(self.report[1]) > 0:
            log.debug(self.report[1])
            result += ('\n') + '\n'.join('{icao} << {distance_nm:.2f}NM, {bearing:.0f}\u00b0T >> {raw_text}\n  bulletin time: {obs_time}\n  station elevation: {sta_elev}\n  station coordinates: {lat:.2f},{lon:.2f}\n  source: {source}'.format(
                    icao=icao.upper(), 
                    distance_nm=self.report[1]['taf'][icao]['distance_nm'],
                    bearing=self.report[1]['taf'][icao]['bearing'],
                    raw_text=self.report[1]['taf'][icao]['result'].raw_text,
                    obs_time=self.report[1]['taf'][icao]['result'].bulletin_time,
                    sta_elev=self.report[1]['taf'][icao]['result'].elevation_m,
                    lat=self.report[1]['taf'][icao]['result'].latitude, 
                    lon=self.report[1]['taf'][icao]['result'].longitude,
                    source=self.report[1]['taf'][icao]['source']
                ) for icao in self.report[1]['taf']) 

        return result.strip('\n') + ('\nNot found: ' + ','.join(self.icaos_not_found) if not is_empty(self.icaos_not_found) else '')


class WxrCommand(BaseCommand):
    """
    WxrCommand
    """
    command = 'wxr'

    help_text = "Returns METAR + TAF data for given ICAO"

    fallback_irc_max_icao_number = 3
    fallback_max_icao_number = 3

    arguments = MetarCommand.arguments


    def __init__(self, args=None, parsed_args=None):

        super(WxrCommand, self).__init__(args=args, parsed_args=parsed_args)

        # Check valid source selected
        check_selected_source(source=self.kwargs['source'], command=self.command, parser=self.parser)

        # Set maximum number of icaos accepted
        if self.origin == 'irc':
            self.max_icao_number = cfg.getint(f'COMMAND_{self.command.upper()}', 'MaxICAOsIRC', fallback=self.fallback_irc_max_icao_number)
        else:
            self.max_icao_number = cfg.getint(f'COMMAND_{self.command.upper()}', 'MaxICAOs', fallback=self.fallback_max_icao_number)

        self.report = Report(icaos=self.kwargs['icaos'][:self.max_icao_number], use_source_idx=self.kwargs['source'], report_types='metar,taf').find()

        if not is_empty(self.kwargs['icaos']) and (is_empty(self.report) or (len(self.report[0]) == 0 and len(self.report[1]) == 0)): 
            raise throw(
                exc=NoResultsError('No weather data found in {}'.format(self.__class__.__name__)),
                context={
                    'human_error': 'No weather results for {}.'.format(','.join(self.kwargs['icaos']))
                }
            )

        # Set METAR icaos not found
        metar_report_icaos_found = [metar.station_id.upper() for metar in self.report[0]['metar']] if 'metar' in self.report[0] else []
        metar_report_icaos_nearest = [icao.upper() for icao in self.report[1]['metar']] if 'metar' in  self.report[1] else []

        self.metar_icaos_not_found = set([i.upper() for i in self.kwargs['icaos']]).difference(set(metar_report_icaos_found).union(set(metar_report_icaos_nearest)))


        # Set TAF icaos not found
        taf_report_icaos_found = [taf.station_id.upper() for taf in self.report[0]['taf']] if 'taf' in self.report[0] else []
        taf_report_icaos_nearest = [icao.upper() for icao in self.report[1]['taf']] if 'taf' in  self.report[1] else []

        self.taf_icaos_not_found = set([i.upper() for i in self.kwargs['icaos']]).difference(set(taf_report_icaos_found).union(set(taf_report_icaos_nearest)))


    @property
    def response_value(self):
        result = ''

        log.debug(self.report)

        if 'metar' in self.report[0] and len(self.report[0]['metar']) > 0:
            result += '\n'.join(metar.raw_text for metar in self.report[0]['metar'])

        if len(self.report[1]) > 0 and 'metar' in self.report[1]:
            result += ('\n') + '\n'.join('{} << {:.2f} NM >> '.format(icao.upper(), self.report[1]['metar'][icao]['distance_nm']) + \
                self.report[1]['metar'][icao]['result'].raw_text for icao in self.report[1]['metar'])

        if 'taf' in self.report[0] and len(self.report[0]['taf']) > 0:
            result +=  ('\n') + '\n'.join(tweak_raw_taf(taf.station_id, taf.raw_text) for taf in self.report[0]['taf'])

        if len(self.report[1]) > 0 and 'taf' in self.report[1]:
            result += ('\n') + '\n'.join('{} << {:.2f} NM >> '.format(icao.upper(), self.report[1]['taf'][icao]['distance_nm']) + \
                tweak_raw_taf(self.report[1]['taf'][icao]['result'].station_id, self.report[1]['taf'][icao]['result'].raw_text) for icao in self.report[1]['taf'])

        return result.strip('\n') + \
            ('\nMETARs not found: ' + ','.join(self.metar_icaos_not_found) if not is_empty(self.metar_icaos_not_found) else '') + \
            ('\n  TAFs not found: ' + ','.join(self.taf_icaos_not_found) if not is_empty(self.taf_icaos_not_found) else '')


    @property
    def response_concise(self):
        return self.response_value

        
    @property
    def response_verbose(self):
        return self.response_value
