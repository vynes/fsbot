import argparse, time

from fsbot import log, is_empty
from fsbot.commands import BaseCommand, CommandError, NoResultsError
from fsbot.exceptions import throw
from fsbot.feeds import rss_get, source_registry, FeedSourceError


__all__ = [
    'RSSCommand'
]


def check_selected_source(idx: int, command: str, parser: argparse.ArgumentParser) -> None:
    """
    Check source against avaialble sources.
    Call parser's error method if not valid.
    """
    if idx not in source_registry:
        parser.error('{} command valid sources:\n{}'.format(
                command, 
                '\n'.join('  [' + str(sidx) + '] ' + source_registry[sidx]['name'] for sidx in source_registry)
            )
        )


class ListSourcesAction(argparse.Action):
    """
    Custom action to make --list-sources behave like --help: print a list 
    of supported sources and exit.
    nargs=0 does the trick to make --list-sources not require an argument.
    """
    def __init__(self, option_strings, dest, nargs=0, **kwargs):
        super(ListSourcesAction, self).__init__(option_strings, dest, nargs=nargs, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        """
        exit() overwritten for IRC subparser ArgumentParser in __init__.py
        to throw CommandError with human_text context instead of spamming log with 
        SystemExit traceback.
        """
        sources_list = 'feed sources:\n{}'.format(
            '\n'.join(
                '  [' + str(sidx) + '] ' + source_registry[sidx]['name'] + ' - ' + source_registry[sidx]['url'] for sidx in sorted(source_registry.keys())) if len(source_registry) > 0 else 'Nothing found.')
        parser.exit(sources_list)


class RSSCommand(BaseCommand):
    """RSSCommand"""

    command = 'rss'

    help_text = "Returns minimal information from RSS feeds"


    arguments = [
        {
            'names': ['-n', '--number'],
            'options': {
                'type': int,
                'metavar': 'NUMBER',
                'default': 1,
                'help': 'number of news items to return'
            }
        },
        {
            'names': ['-C', '--category-filters'],
            'options': {
                'nargs': '*',
                'metavar': 'FILTERS',
                'help': 'category filters (prefix filter term with \'^\' to negate.)'
            }
        },
        {
            'names': ['-T', '--title-filters'],
            'options': {
                'nargs': '*',
                'metavar': 'FILTERS',
                'help': 'title filters (prefix term with \'^\' to negate.)'
            }
        },
        {
            'names': ['-s', '--source'],
            'options': {
                'type': int,
                'metavar': 'SOURCE_ID',
                'help': 'feed source to use (see --list-sources)'
            }
        },
        {
            'names': ['-l', '--list-sources'],
            'options': {
                'action': ListSourcesAction,
                'help': 'lists available feed sources for use with --source'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):

        super().__init__(args=args, parsed_args=parsed_args)

        # Discover sources
        self.sources = source_registry

        # Check valid source selected
        check_selected_source(idx=self.kwargs['source'], command=self.command, parser=self.parser)
        
        try:
            self.result = rss_get(self.kwargs['source'], limit=self.kwargs['number'], category_filters=self.kwargs['category_filters'], title_filters=self.kwargs['title_filters'])
        except FeedSourceError as e:
            raise throw(
                exc=NoResultsError(e),
                context={
                    'cli_error': e,
                    'human_error': 'error/offline'
                }
            )
        
        if is_empty(self.result): 
            raise throw(
                exc=NoResultsError('No feed data in {}'.format(self.__class__.__name__)),
                context={
                    'human_error': 'No data :(.'
                }
            )


    @property
    def response_value(self):
        def get_published_parsed(item) -> str:
            """feedparser decided to drop published_parsed for updated_parsed
            """
            if hasattr(item, 'published_parsed'):
                timestamp_parsed = time.strftime("%Y-%m-%d",item.published_parsed)
            elif hasattr(item, 'updated_parsed'):
                timestamp_parsed = time.strftime("%Y-%m-%d",item.updated_parsed)
            else:
                timestamp_parsed = "--unknown-timestamp--"
            return timestamp_parsed

        return '\n'.join(f'[{get_published_parsed(item)}] {item.title} - {item.link}' for item in self.result)
        

    @property
    def response_concise(self):
        return self.response_value


    @property
    def response_verbose(self):
        return self.response_value        
