import argparse

from fsbot import log
from fsbot.commands import BaseCommand, CommandError, NoResultsError
from fsbot.exceptions import throw
from fsbot.rfactor import live_info, session_info, source_registry, RfServerError


__all__ = [
    'RfSessionCommand',
    'RfLiveInfoCommand'
]


def check_selected_source(idx: int, command: str, parser: argparse.ArgumentParser) -> None:
    """
    Check source against avaialble sources.
    Call parser's error method if not valid.
    """
    if idx not in source_registry:
        parser.error('{} command valid sources:\n{}'.format(
                command, 
                '\n'.join('  [' + str(sidx) + '] ' + source_registry[sidx]['name'] for sidx in source_registry)
            )
        )


class ListSourcesAction(argparse.Action):
    """
    Custom action to make --list-sources behave like --help: print a list 
    of supported sources and exit.
    nargs=0 does the trick to make --list-sources not require an argument.
    """
    def __init__(self, option_strings, dest, nargs=0, **kwargs):
        super(ListSourcesAction, self).__init__(option_strings, dest, nargs=nargs, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        """
        exit() overwritten for IRC subparser ArgumentParser in __init__.py
        to throw CommandError with human_text context instead of spamming log with 
        SystemExit traceback.
        """
        sources_list = 'rfactor sources:\n{}'.format(
            '\n'.join(
                '  [' + str(sidx) + '] ' + source_registry[sidx]['name'] + ' - ' + source_registry[sidx]['web'] for sidx in sorted(source_registry.keys())) if len(source_registry) > 0 else 'Nothing found.')
        parser.exit(sources_list)


class RfSessionCommand(BaseCommand):
    """RfSessionCommand"""

    command = 'rfs'

    help_text = 'rFactor general server/session information'

    arguments = [
        {
            'names': ['-s', '--source'],
            'options': {
                'type': int,
                'metavar': 'SOURCE_ID',
                'default': 1,
                'help': 'rFactor ServerInfo data source (see --list-sources). Default 0.'
            }
        },
        {
            'names': ['-l', '--list-sources'],
            'options': {
                'action': ListSourcesAction,
                'help': 'List available rfactor ServerInfo sources for use with --source'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):
        super().__init__(args=args, parsed_args=parsed_args)

        # Check valid source selected
        check_selected_source(idx=self.kwargs['source'], command=self.command, parser=self.parser)

        try:
            self.result = self.result = session_info(self.kwargs['source'])
            log.debug(self.result)

        except RfServerError as e:
            raise throw(
                exc=NoResultsError(e),
                context={
                    'cli_error': e,
                    'human_error': 'error/offline'
                }
            )


    @property
    def response_value(self):
        result = f'{self.result.name} ({self.result.details.game_name} build {self.result.build})'
        
        result += f'\n  Players: {self.result.playerCount}/{self.result.maxPlayers}'

        result += f'\n  Mod: {self.result.mod.name} {self.result.details.mod_version}'
        result += f'\n  Location: {self.result.mod.location}'
        result += f'\n  Layout: {self.result.mod.layout}'

        result += f'\n  Session: {self.result.details.session_type}'
        
        time_left_min = int(self.result.details.time_left / 60)
        time_left_sec = self.result.details.time_left % 60
        result += f'\n  Time left: {time_left_min:02d}:{time_left_sec:02d}'

        return result
    
    
    @property
    def response_concise(self):
        return self.response_verbose
    

    @property
    def response_verbose(self):
        result = self.response_value

        result += f'\n  Password protected: {"yes" if self.result.passwordProtected else "no"}'

        result += f'\n  Aids: {self.result.details.aids}'
        result += f'\n  Damage: {self.result.details.damage_mult}'
        result += f'\n  Shared skins: {"yes" if self.result.details.shared_skins else "no"}'
        result += f'\n  Shared vehicles: {"yes" if self.result.details.shared_vehs else "no"}'

        return result


class RfLiveInfoCommand(BaseCommand):
    """RfLiveInfoCommand

    Requires rF2 LiveInfo plugin running on server.
    """

    command = 'rf'

    help_text = 'rFactor live driver/session data (needs LiveInfo plugin)'

    arguments = [
        {
            'names': ['-s', '--source'],
            'options': {
                'type': int,
                'metavar': 'SOURCE_ID',
                'default': 1,
                'help': 'rFactor LiveInfo data source (see --list-sources). Default 0.'
            }
        },
        {
            'names': ['-a', '--include-ai'],
            'options': {
                'action': 'store_true',
                'help': 'Include AI in results.'
            }
        },
        {
            'names': ['-l', '--list-sources'],
            'options': {
                'action': ListSourcesAction,
                'help': 'List available rfactor LiveInfo sources for use with --source'
            }
        },
        {
            'names': ['-L', '--limit'],
            'options': {
                'metavar': 'LIMIT_AMOUNT',
                'type': int,
                'default': None,
                'help': 'Limit number of vehicles/drivers in output.'
            }
        }
    ]


    def __init__(self, args=None, parsed_args=None):
        super().__init__(args=args, parsed_args=parsed_args)

        # Check valid source selected
        check_selected_source(idx=self.kwargs['source'], command=self.command, parser=self.parser)

        self.include_ai = self.kwargs['include_ai']
        self.limit_vehicles = self.kwargs['limit']

        try:
            self.result = live_info(self.kwargs['source'])
            log.debug(self.result)

        except RfServerError as e:
            raise throw(
                exc=NoResultsError(e),
                context={
                    'cli_error': e,
                    'human_error': 'error/offline'
                }
            )


    @property
    def response_value(self):

        num_humans_online = sum(map(lambda d: 1 if not d.ControlIsAI else 0, self.result.VehicleInfo))
        num_ai_online = sum(map(lambda a: 1 if a.ControlIsAI else 0, self.result.VehicleInfo))
        num_ai_str =  f' + {num_ai_online} AI' if num_ai_online > 0 else ''
        max_laps_str = self.result.MaxLaps if self.result.MaxLaps < 2147483647 else '*'

        place_padding = 2 if self.result.NumVehicles > 9 else 1

        if not self.result.SessionIsRace:
            if not self.result.SessionIsOver:
                tdelta_secs = (self.result.EndET - self.result.CurET)
                hours_frag = int(tdelta_secs / 3600)
                hours = hours_frag if hours_frag > 0 else 0
                mins = int((tdelta_secs - hours*3600) / 60)
                secs = int(tdelta_secs % 60)
                race_time_left = f' -{hours:02d}:{mins:02d}:{secs:02d}'
            else:
                race_time_left = '00:00'
        else:
            race_time_left = ''

        fcyellow_state = f'*{self.result.YellowFlagStateName}' if self.result.PhaseIsSafetyCar else ''

        rain = '' if not self.result.WxIsRaining else f', {self.result.WxRainType}'

        result_head = f'{self.result.TrackName} - Players: {num_humans_online}{num_ai_str}' \
            f'\n[{self.result.RacePhase}{fcyellow_state}] [{self.result.SessionType}{race_time_left}] ' \
            f'[AT {self.result.AmbientTemp:.0f}°C, TT {self.result.TrackTemp:.0f}°C{rain}] '

        result = []

        if self.result.NumVehicles > 0:

            for driver in sorted(self.result.VehicleInfo, key=lambda k: k.Place)[:self.limit_vehicles]:
                if self.include_ai == False and driver.ControlIsAI: continue

                driver_info = "  "

                if driver.Best[2] != -1.00 and driver.Laps > 0:

                    if self.result.SessionIsRace:
                        if driver.LapsBehindLeader < 1:
                            time_behind_leader = f' +{float(driver.TimeBehindLeader):06.3f}'
                        else:
                            time_behind_leader = f' {" " * (7-len(str(driver.LapsBehindLeader))+2)}+{driver.LapsBehindLeader}L'
                    else: 
                        time_behind_leader = ''

                    driver_info += f'[{str(driver.Place).rjust(place_padding," ")}]{time_behind_leader} [L{int(driver.Laps):02d}/{max_laps_str}] ' \
                    f'[B {int(driver.Best[2] / 60):02d}:{(driver.Best[2] % 60):06.3f}] '

                else:
                    driver_info += f'[-] [L{int(driver.Laps):02d}/{max_laps_str}] [B 00:00.000] '

                result.append(
                    driver_info +

                    # Driver is in pits?
                    ('[PIT] ' if driver.InPits != 0 else '') +

                    # Driver name indicating if AI
                    ('ª' if driver.ControlIsAI else '') +

                    # Driver name space trimmmed and length truncated.
                    f'{" ".join(driver.Driver.strip().split())[:24]}' \
                            f' - {driver.Vehicle[:25]}... ({driver.VehicleClass})'
                )

        result_head += '' if len(result) == 0 else '\n'

        return result_head + '\n'.join(result)


    @property
    def response_concise(self):
        return self.response_value
    

    @property
    def response_verbose(self):
        return self.response_value
