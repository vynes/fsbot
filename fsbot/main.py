#! /usr/bin/env python

import os, sys, argparse

from abc import ABCMeta, abstractmethod

from fsbot import __version_long__, __version_medium__, __creator__, \
    log, init_config


# Set some configs before arguments are parsed by ArgumentParser
# and before modules requiring configuration are loaded.
init_config(sys.argv[1:])


from fsbot.irc import run_irc_bot
from fsbot.cli import run_user_cli
from fsbot.update import update, Update
from fsbot.commands import command_registry, parse_args


class BaseCliCommand(metaclass=ABCMeta):
    """
    Base CliCommand class.

    args are arguments only ie does not include sys.argv[0].
    """

    def __init__(self, args):
        # type: (list) -> None

        self.args = args


        self.parser = argparse.ArgumentParser(
            description = self.__doc__,
            epilog      = __version_long__,
            formatter_class = argparse.RawDescriptionHelpFormatter
        )

        self.parser.add_argument(
            '-c', '--config',
            metavar='CONFIG_FILE',
            type = str,
            help = 'Set config file. Default locations scanned if not specified.'
        )

        self.parser.add_argument(
            '-d', '--debug',
            action='store_true',
            help = 'Enable debug mode.'
        )


    def run(self):
        kwargs = vars(self.parser.parse_args(self.args))

        self.command_func()(**kwargs)


    @abstractmethod
    def command_func(self):
        # type: () -> function
        """
        Must return entry point function to command.
        """ 
        raise NotImplementedError(
            'Not implemented in {cls}.'.format(cls=type(self).__name__),
        )


class IRCCliCommand(BaseCliCommand):
    """
    FSBot IRC Bot.
    """

    def __init__(self, args):
        # type: (list) -> None
        super(IRCCliCommand, self).__init__(args=args)


    def command_func(self):
        return run_irc_bot


class UserCliCommand(BaseCliCommand):
    """
    FSBot user cli.
    """

    def __init__(self, args):
        # type: (list) -> None

        super(UserCliCommand, self).__init__(args=args)


        subparsers = self.parser.add_subparsers(dest='command')
        subparsers.required = True

        for command_name, command_class in command_registry.items():
            # Create subparser of sub commands
            sub_parser = subparsers.add_parser(command_name, help=command_class['cls'].help_text)
            # Add argument definitions (do not parse)
            parse_args(sub_parser, (command_class['cls'].base_arguments, command_class['cls'].arguments), parse_arguments=False)


    def command_func(self):
        return run_user_cli


class UpdateCliCommand(BaseCliCommand):
    """
    Update data sources.
    """

    def __init__(self, args):
        # type: (list) -> None
        super(UpdateCliCommand, self).__init__(args=args)

        self.parser.epilog = """
        {}
        
        Extra airport data acquired from Our Airports (http://ourairports.com/data/)

        Starting from scratch: If a database file does not exist in --db-file location (default or specified), 
        the user will be prompted to confirm creation of a new database. Answer yes. To populate the database, 
        copy apt.dat and relevant ourairport files (see README.md) to the data location of choice (specify in 
        config.ini, at commandline or use current directory) and run 'fsbot-update all`.
        """.format(__version_long__)

        self.parser.add_argument(
            'section',
            metavar='SECTION',
            type=str,
            choices=Update.sections,
            #default=Update.sections[0],
            help='sections to update (%(choices)s)'
        )

        self.parser.add_argument(
            '--db-file',
            type=str,
            help='sqlite3 database file to update (loaded from config.ini if not specified)'
        )

        self.parser.add_argument(
            '--aptdat-file',
            type=str,
            help='apt.dat file to update from (loaded from config.ini if not specified)'
        )

        self.parser.add_argument(
            '--aptdat-file-encoding',
            type=str,
            help='character encoding of apt.dat file (loaded from config.ini if not specified, should normally be ISO-8859-1)'
        )
        
        self.parser.add_argument(
            '--oa-airports-file',
            type=str,
            help='OurAirports airports.csv file to update from (loaded from config.ini if not specified)'
        )
        
        self.parser.add_argument(
            '--oa-runways-file',
            type=str,
            help='OurAirports runways.csv file to update from (loaded from config.ini if not specified)'
        )
        
        self.parser.add_argument(
            '--oa-countries-file',
            type=str,
            help='OurAirports countries.csv file to update from (loaded from config.ini if not specified)'
        )
        
    def command_func(self):
        return update


def main():
    cli_registry = {
        'fsbot': UserCliCommand,
        'fsbot-irc': IRCCliCommand,
        'fsbot-update': UpdateCliCommand
    }

    cli_basename = os.path.basename(sys.argv[0])

    cli_registry[cli_basename](sys.argv[1:]).run()
