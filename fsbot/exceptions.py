from typing import Dict

def throw(exc: Exception, context: Dict = {}) -> Exception:
    """
    Attaches a ``context`` value to an Exception.

    Before::
         exc = Exception('Frog blast the vent core!')
         exc.context = { ... }
         raise exc

    After::
         raise throw(Exception('Frog blast the vent core!'), { ... })
    """
    if not hasattr(exc, 'context'):
        exc.context = {}

    exc.context.update(context)
    return exc
