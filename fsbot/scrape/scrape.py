from typing import Optional, List
import re
import time
import json

# Optional imports
try:
    from selenium import webdriver
    from selenium.webdriver.firefox.options import Options
    from selenium.webdriver.support.wait import WebDriverWait
    #from selenium.webdriver.support import expected_conditions as EC
    from selenium.common.exceptions import TimeoutException, WebDriverException
except ImportError:
    webdriver = None

from fsbot.connectivity import http
from fsbot import cfg, log


def title_from_js_app(url: str):
    """
    Parse js app and find title.

    PARAMS
    ------
    url: str
        A URL - can be data URL ('data:[<mediatype>][;base64],<data>')

    RETURN
    ------
    title: Optional[str]
        Web page title
    """
    if webdriver is None:
        log.warning('Selenium is not installed. Skipping js app parse attempt.')
        return None

    wait_time = int(cfg.get('IRC_TITLE', 'JsTitleTimeout', fallback=10))

    options = Options()
    options.headless = True

    try:
        driver = webdriver.Firefox(options=options)
        driver.get(url)
        time.sleep(wait_time)
        title = driver.title
        # WebDriverWait(driver, 10).until(EC.title_contains('Dotcom'))
    except TimeoutException as e:
        log.error(f'Timeout: {e}')
        return None
    except WebDriverException as e:
        log.error(f'Could not load web driver for selenium: {e}')
        return None
    except ConnectionRefusedError as e:
        log.error(e)
        return None
    finally:
        driver.quit()

    return title


def get_source(url: str, bytes_count: Optional[int] = None) -> str:
    return http.request(
            url,
            bytes_count=bytes_count,
            headers=dict(cfg.items("BROWSER_HEADERS")),
            set_cookies=[('CONSENT', 'YES+0', '.youtube.com')],
            block_cookies=False
        )


def parse_url_rewrites(url: str, rewrites: str) -> List:
    """
    Rewrites url according to rewrite rules.
    Leaves url unchanged for no matches or error.

    PARAMS
    ------
    url: str
        A URL

    RETURN
    ------
    new_url: List[str, Optional[str]]
        [new_url, 'json'|None]

    """
    try:
        rdata = json.loads(rewrites)
    except (json.decoder.JSONDecodeError, TypeError):
        log.warning('WARNING: Could not parse rewrite json, skipping!')
        return [url, None]

    json_title_key = None
    new_url = url

    for rd in rdata:
        if len(rdata[rd]) != 3:
            log.warning(f'WARNING: Could not parse title regex for entry \'{rd}\'. Expected 3 parameters.')
            continue
        try:
            r = re.compile(rdata[rd][0])
        except re.error as e:
            log.warning(f'WARNING: Could not parse title regex for entry \'{rd}\'. Error was:\n{e}')
            continue

        # Match has rewrite value and json key name for title
        if len(rdata[rd][1]) > 0 and len(rdata[rd][2]) > 0:
            new_url = r.sub(rdata[rd][1], url)
            json_title_key = rdata[rd][2]

        if new_url != url:
            log.debug(f"Got match for: {rd}")
            log.debug(f'REWRITTEN URL: {new_url}')
            return [new_url, json_title_key]

    log.debug(f"NO REWRITE MATCHES FOUND")
    return [url, None]


def title(url: str) -> Optional[str]:
    """
    Return title of a webpage.

    PARAMS
    ------
    url: str
        A URL string.

    RETURN
    ------
    title: Optional[str]
        The title or None if not found.
    """
    title = None
    json_title_key = None

    rewrites = cfg.get('IRC_TITLE', 'Rewrites', fallback=None)
    log.debug(f'REWRITES: {rewrites}')
    if rewrites is not None:
        url, json_title_key = parse_url_rewrites(url, rewrites)

    jsdomains = cfg.get('IRC_TITLE', 'JsTitleDomains', fallback=None)

    if jsdomains is not None and len(jsdomains) > 0:
        jsdomains = jsdomains.strip().split(',')
    else:
        jsdomains = []

    if any([d.strip() in url for d in jsdomains]):
        # Try js parse if domain is listed for a js parse
        title = title_from_js_app(url)
        if isinstance(title, str) and len(title.strip()) == 0:
            title = None
    elif json_title_key is not None:
        result = get_source(url)

        if result is not None:
            title = None

            try:
                data = json.loads(result)
            except (json.decoder.JSONDecodeError, TypeError) as e:
                log.error(e)
                log.warning(f'WARNING: Could not parse json for {url}!')
            else:
                tkeys = json_title_key.split('.')
                t = data[tkeys[0]]
                try:
                    for i in tkeys[1:]:
                        t = t[i]
                except IndexError:
                    pass
                title = t
    else:
        result = get_source(url, 300000)

        if result is not None:
            meta_line = re.search(r'<meta name="(og:)?title".*?/?>', result, flags=re.DOTALL)
            if meta_line is not None:
                meta_title = re.search(r'content="(.*?)"', meta_line.group(0))
                if meta_title is not None:
                    title = meta_title.group(1)

            if title is None:
                html_title = re.search(r'<title.*?>(.*?)</title>', result, flags=re.DOTALL)
                if html_title is not None:
                    title = html_title.group(1)

            if title is None:
                # Try js parse if result was found with no title.
                title = title_from_js_app(url)
                if isinstance(title, str) and len(title.strip()) == 0:
                    title = None
        else:
            title = None

    return title
