from typing import Tuple, Dict, List, Any, Union, Optional
import requests, traceback, string
from http import cookiejar

from fsbot.exceptions import throw
from fsbot import __user_agent__, log


__all__ = [
    'NetError',
    'url_encode',
    'request'
]


class NetError(Exception):
    pass


class BlockCookies(cookiejar.CookiePolicy):
    """
    Disable cookies.
    See: https://stackoverflow.com/questions/17037668/how-to-disable-cookie-handling-with-the-python-requests-library

    """
    return_ok = set_ok = domain_return_ok = path_return_ok = lambda self, *args, **kwargs: False
    netscape = True
    rfc2965 = hide_cookie2 = False


def url_encode(data: Dict[str,Any]) -> str:
    return requests.models.RequestEncodingMixin._encode_params(data)


def request(url: str,
            method: str = 'GET',
            json_decode: bool = False, 
            bytes_count: Optional[int] = None, 
            params: Optional[Dict[str,Any]] = None, 
            data: Optional[Union[str, Dict[str,Any]]] = None, 
            headers: Optional[Dict[str,Any]] = None, 
            ignore_error_codes: List[int] = [],
            set_cookies: List[Tuple] = [],
            block_cookies: bool = False,
            clear_cookies_domains: List[str] = [],
            timeout: Tuple[int] = (10,30)) -> Optional[Union[str,Dict[str,Any]]]:
    """
    Send GET or POST requests with given options and parameters. Returns either text result, 
    dict from json or None if status 204 (No content).
    Use parameters for url query string parameters and data for POST payloads. If data is a 
    string, non-form-encoded data will be sent directly, for instance a json string. Be sure 
    to set 'Content-Type' header to 'application/json' in such a case.
    """
    log.debug(f'URL: {url}')

    headers_copy = headers
    if headers_copy is None: headers_copy = {}
    headers = {}


    user_agent = __user_agent__

    for key,value in headers_copy.items():

        if key.lower() == 'user-agent':
            user_agent = value

        headers[string.capwords(key, sep='-')] = value

    # Remove copy
    del headers_copy

    # Set header: User-Agent
    headers.update({'User-Agent': user_agent})

    # Set header: Range
    if bytes_count != None:
        headers.update({'Range': f'bytes=0-{int(bytes_count)}'})

    log.debug(f'HTTP HEADERS SET: {headers}')
    log.debug(f'POST DATA SET: {data}')

    s = requests.Session()
    s.cookies.set_policy(policy=cookiejar.DefaultCookiePolicy(strict_ns_domain=False))

    for cookie in set_cookies:
        s.cookies.set(cookie[0], cookie[1], domain=cookie[2])

    if len(clear_cookies_domains) > 0:
        log.debug(f'Clearing cookies for {",".join(clear_cookies_domains)}...')
        for domain in clear_cookies_domains:
            try:
                s.cookies.clear(domain)
            except KeyError:
                continue

    if block_cookies is True:
        log.debug('Disabling cookies...')
        s.cookies.set_policy(BlockCookies())

    log.debug(f'Session CookieJar: {s.cookies.get_dict() if s.cookies is not None else "None"}')
    log.debug(f'Session CookieJar Domains: {s.cookies.list_domains()}')
    log.debug(f'Local CookieJar: {requests.utils.dict_from_cookiejar(cookiejar.CookieJar())}')

    try:
        if method == 'GET':
            result = s.get(url, params=params, headers=headers, timeout=timeout)
        elif method == 'POST':
            result = s.post(url, data=data, headers=headers, timeout=timeout)
        else:
            raise throw(NetError(f'Invalid request method "{method}".'), context={'error': 'method'})
        #log.debug(f'final url {result.url}')
    except requests.exceptions.RequestException as e:
        raise throw (
            exc = NetError(f'Requests Error: {e}'),
            context = {'e': e, 'traceback': traceback.format_exc()}
        )

    if result.status_code not in (requests.codes.ok, requests.codes.partial) and result.status_code not in ignore_error_codes:
        if result.status_code == 204:
            return None
        raise throw(NetError(f'Http error {result.status_code}.'), context={'status_code': result.status_code})

    log.debug(f'HTTP HEADERS SENT: {result.request.headers}')
    log.debug(f'REQUEST COOKIEJAR: {s.cookies.get_dict()}')
    log.debug(f'HTTP STATSUS CODE: {result.status_code}')
    log.debug(f'HTTP RESULT TEXT: {result.text}')


    if json_decode:
        try:
            return result.json()
        except ValueError as e:
            # Requests doc:
            # In case the JSON decoding fails, r.json() raises an exception. For example, if
            # the response gets a 204 (No Content), or if the response contains invalid JSON, 
            # attempting r.json() raises ValueError: No JSON object could be decoded.
            
            #log.error(f'status code: {result.status_code}, error: {e}')
            raise throw(
                exc = NetError('Json decode error.'), 
                context = {
                    'status_code': result.status_code,
                    'error': 'json_decode',
                    'e': e
                }
            )
    else:
        return result.text
