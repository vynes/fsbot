from typing import List
import socket

# Bundled cpython's telnetlib.py due to imminent deprecation.
# See: ./_telnet.py.License
from ._telnetlib import Telnet as _Telnet

from fsbot import log


__all__ = [
    'Telnet',
    'TelNetError',
    'telnet_get'
]

class Telnet(_Telnet):
    pass

class TelNetError(Exception):
    pass


def telnet_get(host, port, user=None, passwd=None, timeout=30):
    # type: (str, int, Optional(str), Optional(str), int)
    """
    Connects to telnet server and returns all lines.
    """
    try:
        tn = Telnet(host, port, timeout)
    except (socket.gaierror, socket.timeout, ConnectionError, ConnectionAbortedError, ConnectionRefusedError, ConnectionResetError) as e:
        log.error(e)
        raise TelNetError(e)
        
    return tn.read_all().decode('utf-8')
