# coding=utf-8
from __future__ import absolute_import, division, print_function, \
    unicode_literals

from ctypes import Structure, c_int, c_wchar_p, c_float



class SKY_CONDITION(Structure):
    _fields_ = [
        ('sky_cover', c_wchar_p),
        ('cloud_base_ft_agl', c_int),
        ('cloud_type', c_wchar_p)
    ]



class TAF_FORECAST(Structure):
    _fields_ = [
        ('fcst_time_from', c_wchar_p),
        ('fcst_time_to', c_wchar_p),
        ('change_indicator', c_wchar_p),
        ('wind_dir_degrees', c_int),
        ('wind_speed_kt', c_int),
        ('visibility_statute_mi', c_wchar_p),
        ('probability', c_int),
        ('wx_string', c_wchar_p),

        # sky_condition, max 3
        ('sky_condition', SKY_CONDITION)
    ]



class TAF(Structure):
    _fields_ = [
        ('raw_text', c_wchar_p),
        ('station_id', c_wchar_p),
        ('issue_time', c_wchar_p),
        ('bulletin_time', c_wchar_p),
        ('valid_time_from', c_wchar_p),
        ('valid_time_to', c_wchar_p),
        ('latitude', c_float),
        ('longitude', c_float),
        ('elevation_m', c_float),

        # forecasts, max 5
        ('forecasts', TAF_FORECAST * 10)
    ]



class METAR(Structure):
    _fields_ = [
        ('raw_text', c_wchar_p),
        ('station_id', c_wchar_p),
        ('observation_time', c_wchar_p),
        ('latitude', c_float),
        ('longitude', c_float),
        ('temp_c', c_float),
        ('dewpoint_c', c_float),
        ('wind_dir_degrees', c_int),
        ('wind_speed_kt', c_int),
        ('visibility_statute_mi', c_wchar_p),
        ('altim_in_hg', c_float),
        ('altim_in_mm', c_float),

        # sky_condition, max 3
        ('sky_condition', SKY_CONDITION),
        ('flight_category', c_wchar_p),
        ('metar_type', c_wchar_p),
        ('elevation_m', c_float)
    ]



from .utils import *
from .report import *
