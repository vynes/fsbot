from fsbot import cfg, log
from fsbot.weather.sources import source_registry, WeatherSourceError
from fsbot.exceptions import throw
from fsbot.airport import Airport


__all__ = [
    'Report'
]


class WeatherReportError(Exception):
    pass


class Report(object):
    """
    Delegate weather report jobs, auto fallback if first fails.
    (Only falls back if no results or if error connecting).

    use_source: specify source id or use any/all if 'any' or None.

    icaos can be either a comma seperated string or list.
    
    report_types can be either a comma seperated string or list.
    """
    
    report_result = {}

    fallback_nearest_airport_tries = 4

    
    def __init__(self, icaos, use_source_idx=0, report_types='metar'):
        # type: (list, Union(int, list), Union(str, list)) -> None

        # Number of nearest airports to try if no results found
        try:
            self.nearest_airport_tries = cfg.getint('WEATHER', 'WeatherNearestAirportTries', fallback=self.fallback_nearest_airport_tries)
        except (KeyError, ValueError):
            self.nearest_airport_tries = self.fallback_nearest_airport_tries


        # ICAOs to get weather data for
        self.icaos = icaos.split(',') if isinstance(icaos, str) else icaos


        # Report types to request for each ICAO        
        self.types = report_types.split(',') if isinstance(report_types, str) else report_types


        # Find all metar sources
        self.sources = source_registry


        # Source(s) to use.
        # Only specified source will be used with no fallback, 
        # else first with fallback (on error) if none specified.
        if use_source_idx in (0, None, 'any', 'all'):
            self.use_sources = self.sources.keys()
        else:
            if use_source_idx in self.sources:
                self.use_sources = [use_source_idx]
            else:
                self.use_sources = []


    def try_sources(self, icaos, types):
        """
        Tries all available sources for weather results.

        icaos & types may contain missing from a previous try,
        so do not use self.icaos or self.types.
        """
        
        report_result = {}
        
        for source_id in sorted(self.use_sources):

            source_cls = self.sources[source_id]['cls']

            try:
                for report_type in types:

                    if report_type not in source_cls.available_types:
                        raise ValueError("Invalid report type '{report_type}'")

                    report = source_cls(icaos, report_type).get()

                    if len(report) > 0:
                        report_result[report_type] = report
                
                # If it makes it here without error, break out of sources. We're done.
                break

            except (ValueError, WeatherSourceError) as e:
                log.error(e.__str__())
                log.info("Skipping source, trying next available...")
                # Skip to next source
                continue
        
        if len(report_result) > 0:        
            report_result['source'] = source_cls.name

        return report_result


    def find_missing(self):
        # type: () -> list
        """
        Return a list of requested icaos for which weather data was not found.
        """

        missing = {
            'metar': [],
            'taf': []
        }

        for report_type in self.types:
            
            # All there, skip to next type
            if report_type in self.report_result and len(self.icaos) == len(self.report_result[report_type]):
                continue
            
            # Set all to missing
            type_missing_icaos = self.icaos
            
            if report_type in self.report_result:
                for report in self.report_result[report_type]:
                    for i,v in enumerate(self.icaos):
                        log.debug('checking {} == {}'.format(v, report.station_id))
                        if v.upper() == report.station_id:
                            del(type_missing_icaos[i])

            missing[report_type] = type_missing_icaos

        # remove types not missing
        if len(missing['metar']) == 0:
            del(missing['metar'])
        if len(missing['taf']) == 0:
            del(missing['taf'])

        log.debug('missing: {}'.format(missing))

        return missing


    def find(self):
        # type: () -> dict        
        self.report_result = self.try_sources(icaos=self.icaos, types=self.types)

        missing = self.find_missing()

        retry_results = {}

        if len(missing) > 0:
            for report_type in missing:

                for icao in missing[report_type]:
                    
                    nearest = Airport().nearest(icao=icao, metar_only=True)
                    
                    log.debug('nearest: {}'.format(nearest))

                    for nearest_icao in nearest[0:self.nearest_airport_tries+1]:
                        
                        # same airport is removed in function nearest. 
                        # but let's make doubly sure to skip it:
                        if nearest_icao == icao: continue

                        retry_result = self.try_sources(icaos=[nearest_icao[0]], types=[report_type])
                        
                        try:
                            if len(retry_result[report_type]) > 0:
                                if report_type not in retry_results:
                                    retry_results[report_type] = {}

                                retry_results[report_type][icao] = {
                                    'nearest_icao': nearest_icao[0],
                                    'result': retry_result[report_type][0],
                                    'distance_nm': nearest_icao[1],
                                    'distance_km': nearest_icao[2],
                                    'bearing': nearest_icao[3],
                                    'source': retry_result['source']
                                }
                                # Found report for an airport nearby, stop trying.
                                # Onto next ICAO with missing report.
                                break
                        except KeyError:
                            continue

        log.debug('report_result: {}'.format(self.report_result))
        log.debug('retry_results: {}'.format(retry_results))
        
        return (self.report_result, retry_results)
