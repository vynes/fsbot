# coding=utf-8
from __future__ import absolute_import, division, print_function, \
    unicode_literals


__all__ = [
    'tweak_raw_taf'
]


def tweak_raw_taf(icao, raw_taf_string):
    # type: (str, str) -> str
    """
    Make some changes to raw taf string for readability
    """

    # Strip "TAF" from start of raw_text
    if raw_taf_string[:4] == 'TAF ':
        raw_taf_string = raw_taf_string[4:]

    # Prepend ICAO to raw string
    if isinstance(icao, str) and raw_taf_string[:len(icao)] != icao:
        raw_taf_string = icao + ' ' + raw_taf_string

    return raw_taf_string
