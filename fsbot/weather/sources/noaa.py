from fsbot import log, cast_from_ctype
from fsbot.connectivity import http
from fsbot.weather import TAF, METAR
from fsbot.weather.sources import WeatherSourceError, BaseSource


__all__ = [
    'NOAA'
]


class NOAA(BaseSource):
    """
    Get METAR data from noaa.gov
    """
    idx = 1
    name = 'National Oceanic and Atmospheric Administration'
    info = 'https://www.noaa.gov'

    # base url ADD TRAILING SLASH!
    base_url = 'https://tgftp.nws.noaa.gov/data/'

    # Available report types
    available_types = ['metar', 'taf']


    def __init__(self, icaos, report_type='metar'):
        #type: (list, str) -> None

        super(NOAA, self).__init__(icaos=icaos, report_type=report_type)
 
        self.icaos = map(lambda icao: icao.upper(), icaos) 
        self.report_type = report_type


    def get_weather_data(self, base_url):
        # type: (str) -> list
        """
        Return list of weather data per ICAO
        """
        report_results = []

        for icao in self.icaos:
            try:
                icao_weather = list(map(lambda wline: wline.strip(), http.request('{}{}.TXT'.format(base_url, icao)).split('\n')))
            except http.NetError as e:
                if e.context.get('http_error_code') == 404:
                    # 404 - metar for ICAO might not be available, skip to next ICAO.
                    continue
                log.error('Error getting data from {}. Error: {}'.format(self.name, e))
                # Error other than 404, raise source error.
                raise WeatherSourceError(e)

            report_results.append(
                {
                    'station_id': icao, 'observation_time': icao_weather[0], 'raw_text': ' '.join(icao_weather[1:])
                }
            )

        log.debug('{} weather data results found from {}.'.format(len(report_results), self.name))

        return report_results


    def get(self):
        # type: () -> dict
        try:
            result = getattr(self, 'get_' + self.report_type + '_data', None)()
            log.debug('WEATHER RESULT: {}'.format(result))
        except TypeError as e:
            err = '{} not available in {}'.format(self.report_type, type(self).__name__)
            log.warning(e)
            raise WeatherSourceError(err)

        return result


    def get_metar_data(self):

        return_data = []

        metar_data = self.get_weather_data(self.base_url + 'observations/metar/stations/')
        log.debug('metar_data: {}'.format(metar_data))

        for metar_report in metar_data:

            metar = METAR()

            # Build result
            for metar_element in metar_report:
                if metar_element in dict(METAR._fields_):
                    # Set correct type
                    value = cast_from_ctype(value=metar_report[metar_element], c_type=dict(metar._fields_)[metar_element])
                    log.debug('METAR value: '.format(value))
                    # Set value
                    setattr(metar, metar_element, value)
                
            return_data.append(metar)

        return return_data


    def get_taf_data(self):

        return_data = []

        taf_data = self.get_weather_data(self.base_url + 'forecasts/taf/stations/')

        for taf_report in taf_data:

            taf = TAF()

            # Build result
            for taf_element in taf_report:
                if taf_element in dict(TAF._fields_):
                    # Set correct type
                    value = cast_from_ctype(value=taf_report[taf_element], c_type=dict(taf._fields_)[taf_element])
                    # Set value
                    setattr(taf, taf_element, value)
                
            return_data.append(taf)

        return return_data
