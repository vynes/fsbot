import xml.etree.ElementTree as ET
import traceback

from fsbot import log, cast_from_ctype
from fsbot.connectivity import http
from fsbot.weather import TAF, METAR, TAF_FORECAST, SKY_CONDITION
from fsbot.weather.sources import WeatherSourceError, BaseSource


__all__ = [
    'AviationWeather'
]


class AviationWeather(BaseSource):
    """
    Get METAR data from aviationweather.org
    """
    idx = 2
    name = 'Aviation Weather Center'
    info = 'https://aviationweather.gov'


    #base_url = 'https://aviationweather.gov/adds/dataserver_current/httpparam'
    base_url = 'https://aviationweather.gov/api/data/dataserver'

    # Available report types
    available_types = ['metar', 'taf']

    # https://aviationweather.gov/api/data/dataserver?
    # requestType=retrieve&
    # dataSource=tafs&
    # stationString=egte&
    # hoursBeforeNow=8&
    # format=xml&
    # mostRecent=true&
    # mostRecentForEachStation=constraint
    url_params = {
        'requestType': 'retrieve',
        'dataSource': '',
        'stationString': '',
        'hoursBeforeNow': '8',
        'format': 'xml',
        'mostRecent': 'true',
        'mostRecentForEachStation': 'constraint'
    }


    def __init__(self, icaos, report_type='metar'):
        #type: (list, str) -> None

        super(AviationWeather, self).__init__(icaos=icaos, report_type=report_type)
 
        self.icaos = icaos
        self.report_type = report_type

        # ',' join icaos for use in aviationweather api url
        self.url_params['stationString'] = ','.join(icaos).upper()


    def get_xml_root_obj(self):
        """
        Get aviation weather result xml and return root object.
        """
        try:
            xml = http.request('{}?{}'.format(self.base_url, http.url_encode(self.url_params)))
        except http.NetError as e:
            log.error('Error getting data from {}. Error: {}'.format(self.name, e))
            raise

        try:
            xml_root = ET.fromstring(xml)
        except ET.ParseError as e:
            raise ValueError('Error parsing XML: {}'.format(e))

        try:
            data_attribs = xml_root.find('data').attrib
        except AttributeError as e:
            log.error(e)
            log.debug('{}'.format(traceback.format_exc()))
            return None
        
        if 'num_results' not in data_attribs: 
            log.warning('Invalid XML data received: {}'.format(xml))
            raise ValueError('Invalid XML data received')

        if int(data_attribs['num_results']) == 0:
            log.debug('No {} results found.'.format(self.report_type.upper()))
            log.debug('data returned: {}'.format(data_attribs))
            return None

        log.debug('{} weather data results found from {}.'.format(data_attribs['num_results'], self.name))

        return xml_root


    def get(self):
        # type: () -> dict
        try:
            result = getattr(self, 'get_' + self.report_type + '_data', None)()
            log.debug('WEATHER RESULT: {}'.format(result))
        except TypeError as e:
            err = '{} not available in {}'.format(self.report_type, type(self).__name__)
            log.warning(e)
            raise WeatherSourceError(err)

        return result


    def get_metar_data(self):

        return_data = []

        self.url_params['dataSource'] = 'metars'

        try:
            metars_root = self.get_xml_root_obj()
        except ValueError as e:
            raise WeatherSourceError(e)

        # Nothing found
        if metars_root is not None:

            # METARS xml object
            metars_xml_object = metars_root.findall('.//data/*')

            for metar_xml_object in metars_xml_object:

                # Process METAR elements only
                if metar_xml_object.tag != 'METAR':
                    continue

                metar = METAR()

                # Skip METAR if station_id child does not exist
                try:
                    metar_xml_object.find('station_id').text
                except AttributeError as e:
                    log.warning('METAR result has no station_id element. Skipping it...')
                    continue

                # Build result
                for metar_child in metar_xml_object.findall('./*'):

                    # Process sky_condition METAR child element
                    if metar_child.tag == 'sky_condition':
                        sky_condition = SKY_CONDITION()
                        
                        for condition, condition_value in metar_child.attrib.items():
                            # Set correct type
                            value = cast_from_ctype(value=condition_value, c_type=dict(sky_condition._fields_)[condition])

                            # Set SKY_CONDITION sky_condition to value
                            setattr(sky_condition, condition, value)

                        # Add to SKY_CONDITION sky_condition to METAR struct
                        setattr(metar, metar_child.tag, sky_condition)

                        # Skip/continue to next element
                        continue

                    # Process rest of METAR child elements
                    if metar_child.tag in dict(METAR._fields_):
                        # Set correct type
                        value = cast_from_ctype(value=metar_child.text, c_type=dict(metar._fields_)[metar_child.tag])
                        # Set value
                        setattr(metar, metar_child.tag, value)

                return_data.append(metar)

        return return_data


    def get_taf_data(self):

        return_data = []

        self.url_params['dataSource'] = 'tafs'

        try:
            tafs_root = self.get_xml_root_obj()
        except ValueError as e:
            raise WeatherSourceError(e)

        # Nothing found
        if tafs_root is not None:

            # TAFS xml object
            tafs_xml_object = tafs_root.findall('.//data/*')

            for taf_xml_object in tafs_xml_object:

                # Process TAF elements only
                if taf_xml_object.tag != 'TAF':
                    continue

                taf = TAF()

                forecast_list = []

                # Skip TAF if station_id child does not exist
                try:
                    taf_xml_object.find('station_id').text
                except AttributeError as e:
                    log.warning('TAF result has no station_id element. Skipping it...')
                    continue

                # Build result
                for taf_child in taf_xml_object.findall('./*'):

                    if taf_child.tag == 'forecast':

                        forecast = TAF_FORECAST()

                        for fc_child in taf_child.findall('./*'):
                            
                            # Process sky_condition TAF child element
                            if fc_child.tag == 'sky_condition':
                                sky_condition = SKY_CONDITION()
                                
                                for condition, condition_value in fc_child.attrib.items():
                                    # Set correct type
                                    value = cast_from_ctype(value=condition_value, c_type=dict(sky_condition._fields_)[condition])

                                    # Set SKY_CONDITION sky_condition to value
                                    setattr(sky_condition, condition, value)

                                # Add to SKY_CONDITION sky_condition to TAF_FORECAST struct
                                setattr(forecast, fc_child.tag, sky_condition)

                                # Skip/continue to next forecast child element
                                continue

                            # Process rest of forecast child elements
                            if fc_child.tag in dict(TAF_FORECAST._fields_):
                                # Set correct type
                                value = cast_from_ctype(value=fc_child.text, c_type=dict(forecast._fields_)[fc_child.tag])
                                # Set value
                                setattr(forecast, fc_child.tag, value)

                        forecast_list.append(forecast)

                        # Skip/continue to next element
                        continue

                    # Process rest of TAF child elements
                    if taf_child.tag in dict(TAF._fields_):
                        # Set correct type
                        value = cast_from_ctype(value=taf_child.text, c_type=dict(taf._fields_)[taf_child.tag])

                        # Set value
                        setattr(taf, taf_child.tag, value)

                # Set forecasts
                for idx, forecast_obj in enumerate(forecast_list):
                    taf.forecasts[idx] = forecast_obj 

                return_data.append(taf)

        return return_data
