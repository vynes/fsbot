from builtins import input
import sys, re, csv

from fsbot import cfg, log
from fsbot.utils import full_path, path_is_file, print_line_sep
from fsbot.validation import is_valid_airport_icao_code
from fsbot.maths import geodist
from fsbot.data.db import DB, DBError


__all__ = [
    'update',
    'Update'
]


class Update(object):
    """Update"""
    
    # stats
    stats = []

    # valid updatable sections (also used to validate cli options during arg parse)
    sections = ['all','apt','ourairports']

    # Tables to truncate before update
    apt_truncate_tables = ('airports','runways','helipads','parkings','coms', 'nav')
    oa_truncate_tables = ('airports_oa','runways_oa','countries_oa')

    apt_db_conn = None
    db_file = None

    # Commit after every n airports processed
    commitintv = 1000

    def __call__(self, **kwargs):

        self.kwargs = kwargs
        
        # Wether or not to create new DB
        self.init_db = False

        # init DB
        if kwargs.get('db_file'):
            # Get database file path from commandline argument.
            db_file = kwargs.get('db_file')
        else:
            # Get separate database file path from config file, if specified...
            db_file = cfg.get('DATABASE', 'AirportUpdateSqliteFile', fallback=None)
            # ...else get main database file path from config file.
            if db_file is None: db_file = cfg.get('DATABASE', 'AirportSqliteFile', fallback=None)

        if db_file is None:
            print('Unable to determine DB file location. Please specify a file path or update your configuration.')
            self.finish(1,0)
        else:
            self.db_file = full_path(db_file)

        if not path_is_file(self.db_file):
            print('Unable to load DB file: {}.'.format(self.db_file))

            self.check_initialise()
            
            # Exit or initialise new database
            if self.init_db == True:
                self.initialise_database()
            else:
                self.finish(1,0,'Aborting...')
        
        print('Loading database file {}...'.format(self.db_file))
        self.apt_db_conn = DB(self.db_file)

        # Processing of Our Airports must always be run first - some data is borrowed by apt.dat update process.
        if kwargs['section'] in ['ourairports', 'all']: self.run_update_ourairports()
        
        if kwargs['section'] in ['apt', 'all']: self.run_update_apt()

        print('\nReindexing...')

        try:
            self.apt_db_conn.reindex()
        except DBError as e:
            log.debug(e)
            log.error('Reindexing FAILED!')
        
        print('Finished.')

        print_line_sep()

        for section in self.stats:
            print('{}:'.format(section['section']))
            for stat in section['stats']:
                print('    {}: {}'.format(stat, section['stats'][stat]))

        self.finish(0,1)


    def check_initialise(self):
        # type: () -> None
        """
        Prompt user asking if database should be initialised and sets flag accordingly.
        """
        print('If this is a first run, the database must be created.')
        
        try:
            answer = input('Would you like to create this new database? [y/N]: ')
        except EOFError:
            answer = 'EOF'

        if answer.lower() not in ['y','yes']:
            self.init_db = False
        else:
            self.init_db = True


    def initialise_database(self):
        """
        Creates new sql data file and initialises it with some data
        """
        from fsbot.data.db_init import INIT_DB_SQL
        print('\nCreating new database file {}...'.format(self.db_file))
        apt_db_conn = DB(self.db_file)
        apt_db_conn.executescript(INIT_DB_SQL)
        apt_db_conn.close()


    def run_update_ourairports(self):

        print('\n[OUR AIRPORTS]')
        
        self.truncate_tables(self.oa_truncate_tables)

        apt_count = self.oa_process_apt()
        rwy_count = self.oa_process_rwy()
        ctry_count = self.oa_process_ctry()

        self.stats.append(
            {
                'section': 'OUR AIRPORTS',
                'stats': {
                    'airports': apt_count,
                    'runways': rwy_count,
                    'countries': ctry_count,
                }
            }
        )


    def oa_process_apt(self):
        apt_count = 0

        try:
            filename_oa_airports = full_path(self.kwargs['oa_airports_file']) if self.kwargs.get('oa_airports_file') else full_path(cfg['OURAIRPORTS']['AirportsCSVFile'])
        except (IndexError, KeyError):
            print('Unable to load Airports CSV file from config file. Please specify a file path or update your config file.')
            self.finish(1,1)

        if not path_is_file(filename_oa_airports):
            print('Unable to load Airports CSV file {}'.format(filename_oa_airports))
            self.finish(1,1)

        print('Processing file {}...'.format(filename_oa_airports))
        
        file = self.openfile(filename_oa_airports)
        csvreader = csv.DictReader(file, delimiter=',', quotechar='"')

        # CSV format:
        #
        # "id","ident","type","name","latitude_deg","longitude_deg","elevation_ft","continent",
        # "iso_country","iso_region","municipality","scheduled_service","gps_code","iata_code",
        # "local_code","home_link","wikipedia_link","keywords"
        #
        # 3622,"KJFK","large_airport","John F Kennedy International Airport",40.63980103,-73.77890015,13,"NA",
        # "US","US-NY","New York","yes","KJFK","JFK",
        # "JFK","http://www.panynj.gov/CommutingTravel/airports/html/kennedy.html","http://en.wikipedia.org/wiki/John_F._Kennedy_International_Airport","Manhattan, New York City, NYC, Idlewild"


        for airport in csvreader:

            time_to_commit = True if (apt_count + 1) % self.commitintv == 0 else False
            
            if not is_valid_airport_icao_code(airport['ident']):
                print('\nAirport: {} <-- VALID ICAO CODE?'.format(airport['ident']))

            # update db
            try:

                # set empty strings to None
                for e in airport:
                    if airport[e] == '':
                        airport[e] = None

                self.apt_db_conn.insert(
                    'airports_oa', 
                    [
                        [
                            'id',
                            'ident',
                            'type',
                            'name',
                            'latitude_deg',
                            'longitude_deg',
                            'elevation_ft',
                            'continent',
                            'iso_country',
                            'iso_region',
                            'municipality',
                            'scheduled_service',
                            'gps_code',
                            'iata_code',
                            'local_code',
                            'home_link',
                            'wikipedia_link',
                            'keywords'
                        ],
                        [
                            airport['id'],
                            airport['ident'],
                            airport['type'],
                            airport['name'],
                            airport['latitude_deg'],
                            airport['longitude_deg'],
                            airport['elevation_ft'],
                            airport['continent'],
                            airport['iso_country'],
                            airport['iso_region'],
                            airport['municipality'],
                            airport['scheduled_service'],
                            airport['gps_code'],
                            airport['iata_code'],
                            airport['local_code'],
                            airport['home_link'],
                            airport['wikipedia_link'],
                            airport['keywords']
                        ]
                    ]
                )
            except Exception as e:
                log.error('Error insertng ICAO: {}'.format(airport['ident']))
                log.exception(e)
                self.finish(1,1)
            
            print('{}OA Airport: {} \tcount: {} \t\t{}'.format(
                '\r' if apt_count > 0 else '', 
                airport['ident'][:12] + ((12 - len(airport['ident'])) * ' '), 
                apt_count + 1,
                'committing...' if time_to_commit else '*' * 13
                ), end=''
            )
            apt_count += 1

            # commit
            if time_to_commit: self.apt_db_conn.commit()

        # do last commit, except if commit() called in last iteration
        if not time_to_commit: self.apt_db_conn.commit()

        # close csv file
        file.close()

        return apt_count


    def oa_process_rwy(self):
        rwy_count = 0

        try:
            filename_oa_runways = full_path(self.kwargs['oa_runways_file']) if self.kwargs.get('oa_runways_file') else full_path(cfg['OURAIRPORTS']['RunwaysCSVFile'])
        except (IndexError, KeyError):
            print('Unable to load Runways CSV file from config file. Please specify a file path or update your config file.')
            self.finish(1,1)

        if not path_is_file(filename_oa_runways):
            print('Unable to load Runways CSV file {}'.format(filename_oa_runways))
            self.finish(1,1)

        print('\nProcessing file {}...'.format(filename_oa_runways))
        
        file = self.openfile(filename_oa_runways)
        csvreader = csv.DictReader(file, delimiter=',', quotechar='"')

        # CSV format:
        #
        # "id","airport_ref","airport_ident","length_ft","width_ft","surface","lighted","closed",
        # "le_ident","le_latitude_deg","le_longitude_deg","le_elevation_ft","le_heading_degT",
        # "le_displaced_threshold_ft","he_ident","he_latitude_deg","he_longitude_deg","he_elevation_ft",
        # "he_heading_degT","he_displaced_threshold_ft",
        #
        # 244968,3622,"KJFK",11351,150,"ASP",1,0,"04L",40.622,-73.7856,12,30.7,,"22R",40.6488,-73.7647,13,210.7,2696
        # 244967,3622,"KJFK",8400,200,"ASP",1,0,"04R",40.6254,-73.7703,13,30.6,,"22L",40.6452,-73.7549,13,210.6,
        # 244970,3622,"KJFK",10000,150,"ASP",1,0,"13L",40.6578,-73.7902,13,120.9,905,"31R",40.6437,-73.7593,13,300.9,1030
        # 244969,3622,"KJFK",14572,150,"ASP",1,0,"13R",40.6484,-73.8167,13,120.6,2606,"31L",40.6279,-73.7716,13,300.6,3324

        for runway in csvreader:

            time_to_commit = True if (rwy_count + 1) % self.commitintv == 0 else False
            
            # update db
            try:
                runway_name = runway['le_ident'] + '/' + runway['he_ident'] 

                # set empty strings to None
                for e in runway:
                    if runway[e] == '':
                        runway[e] = None
    
                self.apt_db_conn.insert(
                    'runways_oa', 
                    [
                        [
                            'id',
                            'airport_ref',
                            'airport_ident',
                            'length_ft',
                            'width_ft',
                            'surface',
                            'lighted',
                            'closed',
                            'le_ident',
                            'le_latitude_deg',
                            'le_longitude_deg',
                            'le_elevation_ft',
                            'le_heading_degT',
                            'le_displaced_threshold_ft',
                            'he_ident',
                            'he_latitude_deg',
                            'he_longitude_deg',
                            'he_elevation_ft',
                            'he_heading_degT',
                            'he_displaced_threshold_ft'
                        ],
                        [
                            runway['id'],
                            runway['airport_ref'],
                            runway['airport_ident'],
                            runway['length_ft'],
                            runway['width_ft'],
                            runway['surface'],
                            runway['lighted'],
                            runway['closed'],
                            runway['le_ident'],
                            runway['le_latitude_deg'],
                            runway['le_longitude_deg'],
                            runway['le_elevation_ft'],
                            runway['le_heading_degT'],
                            runway['le_displaced_threshold_ft'],
                            runway['he_ident'],
                            runway['he_latitude_deg'],
                            runway['he_longitude_deg'],
                            runway['he_elevation_ft'],
                            runway['he_heading_degT'],
                            runway['he_displaced_threshold_ft']
                        ]
                    ]
                )
            except Exception as e:
                log.error('Error insertng runway: {}'.format(runway_name))
                log.exception(e)
                self.finish(1,1)

            print('{}OA Runway: {} \tcount: {} \t\t{}'.format(
                '\r' if rwy_count > 0 else '', 
                runway_name[:12] + ((12 - len(runway_name)) * ' '), 
                rwy_count + 1,
                'committing...' if time_to_commit else '*' * 13
                ), end=''
            )
            rwy_count += 1

            # commit
            if time_to_commit: self.apt_db_conn.commit()

        # do last commit, except if commit() called in last iteration
        if not time_to_commit: self.apt_db_conn.commit()

        file.close()

        return rwy_count


    def oa_process_ctry(self):
        ctry_count = 0

        try:
            filename_oa_countries = full_path(self.kwargs['oa_countries_file']) if self.kwargs.get('oa_countries_file') else full_path(cfg['OURAIRPORTS']['CountriesCSVFile'])
        except (IndexError, KeyError):
            print('Unable to load Countries CSV file from config file. Please specify a file path or update your config file.')
            self.finish(1,1)

        if not path_is_file(filename_oa_countries):
            print('Unable to load Countries CSV file {}'.format(filename_oa_countries))
            self.finish(1,1)

        print('\nProcessing file {}...'.format(filename_oa_countries))
        
        file = self.openfile(filename_oa_countries)
        csvreader = csv.DictReader(file, delimiter=',', quotechar='"')

        for country in csvreader:

            time_to_commit = True if (ctry_count + 1) % self.commitintv == 0 else False

            # update db
            try:

                # set empty strings to None
                for e in country:
                    if country[e] == '':
                        country[e] = None

                self.apt_db_conn.insert(
                    'countries_oa', 
                    [
                        [
                            'id',
                            'code',
                            'name',
                            'continent',
                            'wikipedia_link',
                            'keywords'
                        ],
                        [
                            country['id'],
                            country['code'],
                            country['name'],
                            country['continent'],
                            country['wikipedia_link'],
                            country['keywords']
                        ]
                    ]
                )
            except Exception as e:
                log.error('Error insertng runway: {}'.format(country['code']))
                log.exception(e)
                self.finish(1,1)


            print('{}OA Country: {} \tcount: {} \t\t{}'.format(
                '\r' if ctry_count > 0 else '', 
                country['code'][:12] + ((12 - len(country['code'])) * ' '), 
                ctry_count + 1,
                'committing...' if time_to_commit else '*' * 13
                ), end=''
            )
            ctry_count += 1

            # commit
            if time_to_commit: self.apt_db_conn.commit()

        # do last commit, except if commit() called in last iteration
        if not time_to_commit: self.apt_db_conn.commit()

        file.close()

        return ctry_count


    def run_update_apt(self):

        print('\n[APT.DAT]')

        time_to_commit = False
        current_apt = None
        apt_count = 0
        rwy_count = 0
        heli_count = 0
        com_count = 0
        park_count = 0

        # load APT file
        try:
            filename_apt = full_path(self.kwargs['aptdat_file']) if self.kwargs.get('aptdat_file') else full_path(cfg['APT']['AirportDatFile'])
        except (KeyError, IndexError):
            print('Unable to determine APT.DAT file location. Please specify a file path or update your configuration.')
            self.finish(1,1)

        file_encoding = self.kwargs['aptdat_file_encoding'] if self.kwargs.get('aptdat_file_encoding') else cfg['APT'].get('AirportDatFileEncoding', 'ISO-8859-1')

        file = self.openfile(filename_apt, byte_mode=True)

        # Regex compiled patterns
        prog_runways = re.compile('^(100|101)\s+(.*)')
        prog_helipads = re.compile('^(102)\s+(.*)')
        prog_airports = re.compile('^(1|16|17)\s+(.*)')
        prog_parkings = re.compile('^(15)\s+(.*)')
        prog_coms = re.compile('^(50|51|52|53|54|55|56)\s+(.*)')

        self.truncate_tables(self.apt_truncate_tables)

        print('Processing file {}...'.format(filename_apt))

        for line in file:
            
            #DEBUG STOP
            #if apt_count >= 100:
            #    self.finish(1,1)
            
            line = line.decode(file_encoding)

            log.debug('Line: {}'.format(line))
            
            # Check/Set start of new airport
            if line in ['\n', '\r\n']:
                current_apt = None
                continue

            if current_apt == None:
                # Match ICAO line (airports, heliports...)
                m = prog_airports.match(line)
                if m:

                    # Commit changes so far
                    if time_to_commit: self.apt_db_conn.commit()

                    # process airport
                    current_apt = self.apt_process_apt(m.group(1), m.group(2))
                    
                    apt_count += 1
                    
                    time_to_commit = True if (apt_count) % self.commitintv == 0 else False

                    if not is_valid_airport_icao_code(current_apt):
                        print('\nAirport: {} <-- VALID ICAO CODE?'.format(current_apt))

                    print('{}APT Airport: {} \tcount: {} \t\t{}'.format(
                        '\r' if apt_count > 0 else '', 
                        current_apt[:12] + ((12 - len(current_apt)) * ' '), 
                        apt_count,
                        'committing...' if time_to_commit else '*' * 13
                        ), end=''
                    )

            # Find rest of airport data for cuurent airport
            if current_apt != None:

                # Match runways (land and water)
                m = prog_runways.match(line)
                if m:
                    self.apt_process_rwy(m.group(1), current_apt, m.group(2))
                    rwy_count = rwy_count + 1
                    continue

                # Match helipads
                m = prog_helipads.match(line)
                if m:
                    self.apt_process_heli(m.group(1), current_apt, m.group(2))
                    heli_count = heli_count + 1
                    continue

                # Match parking
                m = prog_parkings.match(line)
                if m:
                    self.apt_process_parking(m.group(1), current_apt, m.group(2))
                    park_count = park_count + 1
                    continue

                # Match coms
                m = prog_coms.match(line)
                if m:
                    self.apt_process_coms(m.group(1), current_apt, m.group(2))
                    com_count = com_count + 1
                    continue

        self.apt_db_conn.commit()

        self.stats.append(
            {
                'section': 'APT.DAT',
                'stats': {
                    'airports': apt_count,
                    'runways': rwy_count,
                    'helipads': heli_count,
                    'parkings': park_count,
                    'coms': com_count,
                }
            }
        )

        file.close()


    def truncate_tables(self, tables):
        for table in tables:
            print('Truncating table {}'.format(repr(table)))
            self.apt_db_conn.truncate(table, force=True, delseq=True)
            self.apt_db_conn.commit()


    def apt_process_apt(self, xcode, data):
        aptdata = re.split('\s+', data.strip(), 4)
        
        airport_types = {
            '1': 'land_airport',
            '16': 'seaplane_base',
            '17': 'heliport'
        }

        # Look up this airport's country code from Our Airports tables.
        try:
            airport_oa = self.apt_db_conn.one('airports_oa', ['ident', aptdata[3]], 'iso_country')
            iso_country = 'n/a' if airport_oa is None else airport_oa[0]
        except Exception as e:
            log.error('Error rerieving country code for: {} {}'.format(type(aptdata[3]), aptdata[3]))
            log.error(e)
            log.debug('airports_oa result: {}'.format(airport_oa))
            self.finish(1,1)

        # Record to DB
        try:
            self.apt_db_conn.insert('airports', [
                ['xcode','icao','elevation', 'hastower', 'name', 'type', 'iso_country'],
                [
                    xcode, 
                    aptdata[3], 
                    aptdata[0], 
                    aptdata[1], 
                    aptdata[4], 
                    'n/a' if str(xcode) not in airport_types else airport_types[str(xcode)],
                    iso_country
                ]
            ])
        except Exception as e:
            log.error('Error insertng ICAO: {}'.format(aptdata[3]))
            log.exception(e)
            self.finish(1,1)
        
        return aptdata[3]


    def apt_process_rwy(self, xcode, icao, data):
        splitnum = 24 if int(xcode) == 100 else 7
        rwydata = re.split('\s+', data.strip(), splitnum)

        log.debug('Process {0} runway data: {1}'.format(xcode, data))

        fields = [
            'xcode',
            'icao',
            'width',
            'length',
            'surface_type',
            'shoulder_type',
            'smoothness',
            'centerline_lights',
            'edge_lights',
            'perimeter_buoys',
            'rwy_a_number',
            'rwy_a_end_lat',
            'rwy_a_end_lon',
            'rwy_a_disp_threshold_length',
            'rwy_a_blastpad_length',
            'rwy_a_marking_code',
            'rwy_a_approach_lighting_code',
            'rwy_a_touchdown_zone_lighting_code',
            'rwy_a_end_id_lights_code',
            'rwy_b_number',
            'rwy_b_end_lat',
            'rwy_b_end_lon',
            'rwy_b_disp_threshold_length',
            'rwy_b_blastpad_length',
            'rwy_b_marking_code',
            'rwy_b_approach_lighting_code',
            'rwy_b_touchdown_zone_lighting_code',
            'rwy_b_end_id_lights_code'
        ]

        if int(xcode) == 101:
            rwylength = geodist(rwydata[3],rwydata[4],rwydata[6],rwydata[7])

            values = [
                xcode,
                icao,
                rwydata[0],
                rwylength,
                0,
                0,
                0,
                0,
                0,
                rwydata[1],
                rwydata[2],
                rwydata[3],
                rwydata[4],
                0,
                0,
                0,
                0,
                0,
                0,
                rwydata[5],
                rwydata[6],
                rwydata[7],
                0,
                0,
                0,
                0,
                0,
                0,
            ]

        if int(xcode) == 100:
            rwylength = geodist(rwydata[8],rwydata[9],rwydata[17],rwydata[18])

            values = [
                xcode,
                icao,
                rwydata[0],
                rwylength,
                rwydata[1],
                rwydata[2],
                rwydata[3],
                rwydata[4],
                rwydata[5],
                0,
                rwydata[7],
                rwydata[8],
                rwydata[9],
                rwydata[10],
                rwydata[11],
                rwydata[12],
                rwydata[13],
                rwydata[14],
                rwydata[15],
                rwydata[16],
                rwydata[17],
                rwydata[18],
                rwydata[19],
                rwydata[20],
                rwydata[21],
                rwydata[22],
                rwydata[23],
                rwydata[24]
            ]

        try:
            self.apt_db_conn.insert('runways', [fields, values])
        except Exception as e:
            log.error('Error inserting runway for airport ICAO: {}'.format(icao))
            log.exception(e)
            self.finish(1,1)

        return True
    

    def apt_process_heli(self, xcode, icao, data):
        helidata = re.split('\s+', data.strip(), 10)

        log.debug('Process {0} helipad({1}) data: {2}'.format(xcode, icao, data))

        fields = [
            'xcode',
            'icao',
            'designator',
            'center_lat',
            'center_lon',
            'heading',
            'length',
            'width',
            'surface_code',
            'markings',
            'shoulder_type',
            'smoothness',
            'edge_lighting'
        ]

        values = [
            xcode,
            icao,
            helidata[0],
            helidata[1],
            helidata[2],
            helidata[3],
            helidata[4],
            helidata[5],
            helidata[6],
            helidata[7],
            helidata[8],
            helidata[9],
            helidata[10]
        ]

        try:
            self.apt_db_conn.insert('helipads', [fields, values])
        except Exception as e:
            log.error('Error inserting helipad for airport ICAO: {}'.format(icao))
            log.exception(e)
            self.finish(1,1)

        return True


    def apt_process_parking(self, xcode, icao, data):
        parkdata = re.split('\s+', data.strip(), 3)

        log.debug('Process {0} parking data: {1}'.format(xcode, data))

        fields = [
            'xcode',
            'icao',
            'lat',
            'lon',
            'heading',
            'name'
        ]

        # Check omitted description
        if len(parkdata) == 3:
            desc = '(?)'
        else:
            desc = parkdata[3]

        values = [
            xcode,
            icao,
            parkdata[0],
            parkdata[1],
            parkdata[2],
            desc
        ]

        try:
            self.apt_db_conn.insert('parkings', [fields, values])
        except Exception as e:
            log.error('Error inserting parking for airport ICAO: {}'.format(icao))
            log.exception(e)
            self.finish(1,1)

        return True


    def apt_process_coms(self, xcode, icao, data):
        comdata = re.split('\s+', data.strip(), 1)
        log.debug('Process {0} coms data: {1}'.format(xcode, data))

        fields = [
            'xcode',
            'icao',
            'freq',
            'name'
        ]

        # Check omitted description
        if len(comdata) == 1:
            desc = '(?)'
        else:
            desc = comdata[1]

        values = [
            xcode,
            icao,
            comdata[0],
            desc
        ]

        try:
            self.apt_db_conn.insert('coms', [fields, values])
        except Exception as e:
            log.error('Error inserting coms for airport ICAO: {}'.format(icao))
            log.exception(e)
            self.finish(1,1)

        return True


    def openfile(self, filename, byte_mode=False):
        try:
            if byte_mode:
                file = open(filename, 'rb')
            else:
                file = open(filename, 'r', newline='')
        except (IOError, TypeError):
            log.error('Unable to read file {}'.format(filename))
            self.finish(1)
        
        if not file:
            log.error('Unable to read file {}...'.format(filename))
            # terminate
            self.finish(1)

        return file


    def finish(self, code=0, closedb=0, msg=None):

        log.debug('Cleaning up...')

        if closedb != 0: self.apt_db_conn.close()
        
        if msg != None:
            print(msg)

        sys.exit(code)


update = Update()
