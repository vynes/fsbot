import re
from urllib.parse import urlparse

from fsbot import log


__all__ = [
    'is_valid_airport_icao_code',
    'is_number',
    'is_uri',
    'is_empty'
]


def is_valid_airport_icao_code(icao):
    """
    Check for valid airport ICAO code.
    """
    # length
    length = len(icao)
    if length > 8 or length < 2: return False 
    
    # characters
    prog_icao = re.compile('[^-a-zA-Z0-9]')
    if prog_icao.search(icao): return False

    return True


def is_number(x):
    """
    Check if x is a real number
    """
    try:
        float(x)
        return True
    except (ValueError, TypeError):
        return False


def is_uri(uri):
    try:
        result = urlparse(uri)
        log.debug('title from url validation: {}'.format(result))
        return all([result.scheme, result.netloc])
    except (AttributeError) as e:
        log.error(e)
        return False


def is_empty(x, zero_is_empty=False):
    """
    Check for null or empty values
    """
    # null and bool
    if (isinstance(x, bool) and x == False) or x is None: return True

    # Zero
    if zero_is_empty == True and isinstance(x, (int, float)) and float(x) == 0: return True
    
    # Empty  Lists, Dicts and Tuples
    if isinstance(x, (list, dict, tuple)) and len(x) == 0: return True

    # Empty iters
    try:
        it = iter(x)
        if sum(1 for _ in it) == 0: return True
    except TypeError:
        pass

    # Strings and things
    if len(str(x)) == 0: return True

    return False
