from fsbot import cfg
from fsbot.validation import is_empty


__all__ = [
    'source_registry'
]


source_registry = {}

def discover_sources():
    if 'TEAMSPEAK_SERVERS' in cfg.sections():
        for idx,value in cfg['TEAMSPEAK_SERVERS'].items():
            values = value[1:].split(value[0],9)
            source_registry[int(idx)] = {
                'name': values[0],
                'server_id': values[1] if not is_empty(values[1]) else 1,
                'base_url': values[2],
                'api_key': values[3],
                'exclude_client_types': values[4],
                'poll_address': values[5],
                'user_pass': values[6],
                'poll_interval': int(values[7]) if not is_empty(values[7]) or int(values[7] > 5) else 10,
                'channels': values[8],
            }


discover_sources()

from .teamspeak import *
