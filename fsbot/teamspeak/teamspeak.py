from typing import Optional, Dict, List
import time
import re
import socket

from fsbot import cfg, log
from fsbot.validation import is_empty
from fsbot.exceptions import throw
from fsbot.connectivity import http, NetError, Telnet
from fsbot.teamspeak import source_registry


__all__ = [
    'TsServerError',
    'session_info',
    'client_list',
    'poll_clients'
]

class TsServerError(Exception):
    pass


def get_source(url: str, api_key: str) -> str:
    try:
        return http.request(
                url,
                json_decode=True,
                headers={'x-api-key': api_key}
            )
    except NetError as e:
        log.error(f'{e}.')
        raise throw(TsServerError(f'TS Server error: {e}.'))

# {source_id: [time, clients]}
ts_poll_registry = {}


def session_info(source_id: int) -> Optional[Dict]:
    """
    Return a list of users.

    PARAMS
    ------
    source_id: int
        Id of source server.
    
    RETURN
    ------
    users: Optional[Dict]
        Users currently online.
    """
    result = None

    try:
        base_url = source_registry[source_id]['base_url']
        if source_registry[source_id].get('server_id'):
            server_id = source_registry[source_id]['server_id']
        else:
            server_id = 1

        if source_registry[source_id].get('exclude_client_types') and not is_empty(source_registry[source_id]['exclude_client_types']):
            exclude_client_types = source_registry[source_id]['exclude_client_types'].split('|')
        else:
            exclude_client_types = []

        api_key = source_registry[source_id]['api_key']
    except KeyError as e:
        log.error(f'{e}.')
        raise throw(TsServerError(f'TS Server config error: {e}.'))

    if api_key is None:
        log.warning('Config: TEAMSPEAK:ApiKey not specified!')
        return None
    
    url = base_url + '/' + str(server_id) + '/clientlist'

    result = get_source(url, api_key)
    
    try:
        if result is None or not isinstance(result, dict) or result['status']['code'] != 0:
            log.error(f'TS Server Query return error. Result was: {result}')
            return None
    except KeyError:
        log.error(f'TS Server Query return error. Result was: {result}')
        return None

    filtered_result = {}

    try:
        filtered_result['body'] = [v for c,v in enumerate(result['body']) if result['body'][c]['client_type'] not in exclude_client_types]
    except (KeyError, IndexError):
        log.error(f'{e}.')
        raise throw(TsServerError(f'Exluding client types error: {e}.'))

    return filtered_result


def telnet_cmd(host: str, port: int, cmds: List[List]) -> str:
    """
    Run a series of telnet commands, capturing the final output.

    PARAMS
    ------
    host: str
        The hostname for establishing a telnet session.
    port: int
        The port.
    cmds: List[List]
        A list of commands and compiled regex "read_untill" values for each command.
        At index 1 of each cmd list is the list compiled regexes. They comprise a
        successful output at index 0 and error output at 1.  

    RETURN
    ------
    str
        The final command result.
    """
    try:
        tn = Telnet(host, port)
    except (ConnectionRefusedError, 
            socket.gaierror, 
            socket.timeout, 
            ConnectionError, 
            ConnectionAbortedError, 
            ConnectionRefusedError, 
            ConnectionResetError) as e:
        raise throw(TsServerError(f'TS Server error: {e}.'))

    for cmd in cmds:
        tn.write(cmd[0])
        result = tn.expect(cmd[1], timeout=20)
        if result[0] == 0:
            continue
        elif result[0] == 1:
            e = result[2].decode("utf-8")
            log.error(f'Telnet command error: {e}')
            raise throw(TsServerError(f'Telnet command error: {e}'))
        else:
            e = result[2].decode("utf-8")
            log.error(f'Telnet Unexpected return: {e}')
            raise throw(TsServerError(f'Telnet Unexpected return: {e}'))

    tn.write(b'quit\n')
    return result[2].decode('utf-8')


def parse_ts_clientlist(data, exclude_client_types: List[Dict] = []):
    """
    Parse result from Teamspeak clientlist, excluding specified client types.
    """
    data = data.strip().split('\n')
    
    clientlist = []

    for c in data[0].split('|'):
        client_data = {}

        cs_strings = c.split()
        
        # check client data size
        if len(cs_strings) != 5:
            log.error('Teamspeak telnet unexpected output from clientlist command:')
            raise throw(TsServerError(f'Unexpected length of teamspeak client data: {len(cs_strings)}. Expected: 5'))
        
        for cs in cs_strings:
            i,v = cs.split('=')
            client_data[i] = v
        
        if int(client_data['client_type']) not in exclude_client_types: 
            clientlist.append(client_data)

    return clientlist


def client_list(source_id: int) -> List[Dict]:
    result = None

    try:
        host, port = source_registry[source_id]['poll_address'].split(':')

        if source_registry[source_id].get('server_id') and not is_empty(source_registry[source_id]['server_id']):
            server_id = source_registry[source_id]['server_id']
        else:
            server_id = 1

        if source_registry[source_id].get('exclude_client_types') and not is_empty(source_registry[source_id]['exclude_client_types']):
            exclude_client_types = source_registry[source_id]['exclude_client_types'].split('|')
        else:
            exclude_client_types = []

        if source_registry[source_id].get('user_pass') and not is_empty(source_registry[source_id]['user_pass']):
            user, password = source_registry[source_id]['user_pass'].split(':', 1)
        else:
            user, password = [None, None]
    except KeyError as e:
        log.error(f'{e}.')
        raise throw(TsServerError(f'TS Server config error: {e}.'))


    return_regexes = [
        re.compile(b'error id=0 msg=ok'),
        re.compile(b'error id=(?!0)[0-9]+ msg=(?!ok).*')
    ]

    cmds = [
        [(f'use {server_id}\n').encode('utf-8'), return_regexes],
        [b'clientlist\n', return_regexes]
    ]

    # If user or password not specified, skip login command.
    if None not in [user,password]:
        cmds.insert(0, [(f'login {user} {password}\n').encode('utf-8'), return_regexes])

    result = telnet_cmd(host, port, cmds=cmds)

    clientlist = parse_ts_clientlist(data=result, exclude_client_types=[1])

    return clientlist


def poll_clients(source_id: int) -> Optional[Dict]:
    """
    Return dict of current, newly connected, reconnected and disconnected clients since last poll.
    Return None if not scheduled.
    """
    time_now = int(time.time())

    result = {
        'online': [],
        'new': [],
        'rejoined': [],
        'left': []
    }

    if source_id in ts_poll_registry:
        clients_previous = ts_poll_registry[source_id][1]
        seconds_passed = time_now - ts_poll_registry[source_id][0]
    else:
        clients_previous = []
        seconds_passed = 9999999
    clients_online_now = client_list(source_id)
    clients_new = []
    clients_reconnected = []
    clients_disconnected = []

    # Not scheduled, return.
    if seconds_passed < source_registry[source_id]['poll_interval']:
        return None

    ts_poll_registry[source_id] = [time_now, clients_online_now]
    result['online'] = clients_online_now

    # Check for new and rejoined clients since last
    for online_client in clients_online_now:
        # Check for newly joined clients since last
        if online_client['client_database_id'] not in [po['client_database_id'] for po in clients_previous]:
            result['new'].append(online_client)

        if len(clients_previous) != 0:
            previous_client_matched = None
            for pc in clients_previous:
                if pc['client_database_id'] == online_client['client_database_id']:
                    previous_client_matched = pc
            
            # Client still present since last poll:
            if previous_client_matched is not None:
                # Check if client reconnected since previous
                if online_client['clid'] != previous_client_matched['clid']:
                    result['rejoined'].append(online_client)


    # Check for clients that went offline since last
    for cp in clients_previous:
        if cp['client_database_id'] not in [oc['client_database_id'] for oc in clients_online_now]:
            result['left'].append(cp)
    
    return result
