from typing import Optional, Any, Dict, List

import os, sys
import pickle
import time
from hashlib import md5

from dataclasses import dataclass, field

from fsbot import cfg, log


__all__ = [
    'SessionCache',
    'FileCache'
]


@dataclass
class SessionCache:
    """
    This is simple caching - a temporary runtime cache.
    No expiry management needed as it isn't persisted across sessions.
    
    Initialised in fsbot/__init__ as session_cache. 
        To use: 
            from fsbot import session_cache
    
    See FileCache for cross-session data feed caching.
    
    # TODO: Manage cache size?
    """

    _data: Dict[str,Any] = field(init=False, repr=True, default_factory=dict)

    def read(self, key: str) -> Optional[Any]:
        try:
            return self._data[key]
        except KeyError:
            return None
    
    def write(self, key: str, value: Any):
        self._data[key] = value
    
    @property
    def size(self):
        return sys.getsizeof(self._data)
    
    @property
    def data(self):
        return self._data


class FileCache:
    """
    Cross-session file caching.
    """
    def __init__(self,
            # A unique key generated from keystr.
            # This should be any string within context of the data cached, like
            # a concatenation of a url and a config string.
            keystr: str,

            # The directory where the cache file and indexes are saved.
            directory: str,

            # Cache is cleared when it has lived longer than this number of seconds.
            # If set to 0, file is persistent cache.
            ttl: int = 600,

            # Force clearing of the cache.
            clear: bool = False
        ):
        
        try:
            self.enabled = cfg.getboolean('CACHE', 'CachingEnabled')
        except ValueError:
            self.enabled = False

        self.timestamp = int(time.time())
        self.key = self.genkey(keystr)
        self.clear = clear
        self.directory = self.get_dir(directory)

        self.ttl = ttl

        # The cache file.
        # This may or may not be the same file used during a cache read - data from
        # an indexed cache file may be used instead. 
        self.w_file = self.get_wfile_path()
        
        self.index_file = os.path.join(self.directory, 'cache.idx')
        self._index = {}

        self._check()

    def genkey(self, keystr: str) -> str:
        """
        Generate a cache key.
        """
        log.debug(f'[File Cache] Keystring: {keystr}')
        return md5(keystr.encode()).hexdigest()

    def get_wfile_path(self) -> str:
        """
        Generate new filename for writing and return the full path to it.
        """
        f = None
        if self.ttl == 0:
            f = self.find()
        
        if f is None:
            f = os.path.join(self.directory, f'{self.timestamp}_{self.key}')
        
        return f 

    def get_dir(self, directory: str) -> str:
        """
        Check cache dir.
        """
        directory = os.path.expanduser(directory)
        try:
            os.makedirs(directory, exist_ok=True)
        except FileExistsError:
            pass

        return directory

    def _check(self):
        """
        Manage cache expiration.
        """
        if not self.enabled:
            log.warning('Caching disabled. Some user data will not persist across sessions.')
            return

        for f in os.listdir(self.directory):
                
            cache_file = os.path.join(self.directory, f)

            # Clear all
            if self.clear == True:
                log.debug(f'[File Cache] Clearing: {cache_file}')
                os.remove(cache_file)
                continue

            if self.ttl == 0:
                # Cache never expires:
                continue

            # Find and clear expired
            fparts = f.split('_', 1)
            if len(fparts) == 2:
                if self.timestamp - int(fparts[0]) >= self.ttl:
                    log.debug(f'[File Cache] Clearing expired: {cache_file}')
                    os.remove(cache_file)
        
        self._index_init()

    def find(self) -> Optional[str]: 
        cache_file: Optional[str] = None

        # Find cached data within expiry time
        for f in os.listdir(self.directory):
            fparts = f.split('_', 1)
            
            if len(fparts) == 2 and fparts[1] == self.key:
                cache_file = os.path.join(self.directory, f)
                break

        return cache_file

    def index_update(self, index):
        """
        Update index.
        """
        with open(self.index_file, 'wb') as f:
            pickle.dump(index, f)
        self._index = index

    def _index_init(self):
        """
        Initialise and validate the index.
        """
        # Write new empty index if not exist else retrieve current index.
        if not os.path.isfile(self.index_file):
            log.debug(f'[File Cache] Indexing: creating file {self.index_file!r}.')
            index = {}
        else:
            with open(self.index_file, 'rb') as f:
                index = pickle.load(f)

        # Clean orphaned keys from index
        if len(index) > 0:
            remove_keys = []
            cache_files = os.listdir(self.directory)
            for key in index:
                matching_cache_keys = list(filter(lambda cf: key in cf, cache_files))
                if len(matching_cache_keys) == 0:
                    remove_keys.append(key)
            for key in remove_keys:
                log.debug(f'[File Cache] Indexing: removing stale key {key!r} from index.')
                del index[key]
        
        self.index_update(index)

    def index_add(self, idxs: List[str]):
        """
        Create new index item.
        """
        log.debug(f'[File Cache] Indexing: adding {idxs} to index at key {self.key!r}.')
        index = self._index
        index[self.key] = idxs
        self.index_update(index)
    
    def index_remove(self, key: str):
        """
        Remove item from index.
        """
        index = self._index
        try:
            del index[key]
        except KeyError as e:
            log.debug(f'[File Cache] Indexing: Error removing key {key!r}: {e!r}')
            return
        self.index_update(index)

    @property
    def index(self):
        return self._index

    def find_indexed(self, idx: str) -> Optional[str]:
        """
        Searches for idx in the index and changes the current cache key to the found index's. 
        """
        for index_key, idxs in self.index.items(): 
            if idx in idxs:
                log.debug(f'[File Cache] Indexing: Found id {idx!r} at key {index_key!r}.')
                log.debug(f'[File Cache] Indexing: Changing cache key to {index_key!r}')
                self.key = index_key
                break

        return self.find()

    def read(self, index_id: Optional[str] = None) -> Any:
        """
        Retrieves data from cache file.

        Params
            index_id: Optional[str]
                An index id can be specified to find a single record within an indexed cache.
        """
        if not self.enabled:
            return

        cached: Any = None
        cache_file: Optional[str] = None
        
        # Find in indexed file
        if index_id is not None:
            cache_file = self.find_indexed(index_id)
        else:
            cache_file = self.find()

        if cache_file is not None:
            log.debug(f'[File Cache] Fetching: {cache_file}')
            with open(cache_file, 'rb') as cf:
                cached = pickle.load(cf)

        return cached

    def write(self, data, index_ids: List[str] = []):
        """
        Writes data to cache and optionally sets index ids.

        Params
            index_ids: List[str]
                Cache data can be indexed for a list of specified ids, so that
                a single record can later be retrieved by id from the same cache
                file, instead of retrieving data and writing a new file only for 
                 that one record. 
        """
        if not self.enabled:
            return
        
        log.debug(f'[File Cache] Saving: {self.w_file}')
        with open(self.w_file, 'wb') as f:
            pickle.dump(data, f)

        if len(index_ids) > 0:
            self.index_add(index_ids)
