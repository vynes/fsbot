from ctypes import c_int, c_wchar_p, c_float, c_void_p

from fsbot.struct import struct_factory


__all__ = [
    'runway_struct',
    'airport_struct'
]


@struct_factory
def runway_struct():
    # type: () -> Dict
    return {
        'fields': (
            ('icao', c_wchar_p),
            ('width', c_float),
            ('width_ft', c_float),
            ('width_m', c_float),
            ('length', c_float),
            ('length_ft', c_float),
            ('length_m', c_float),
            ('surface_type', c_wchar_p),
            ('runway_a_number', c_wchar_p),
            ('runway_a_end_lat', c_float),
            ('runway_a_end_lon', c_float),
            ('runway_b_number', c_wchar_p),
            ('runway_b_end_lat', c_float),
            ('runway_b_end_lon', c_float)
        ),
        'defaults': {
            'icao': 'n/a',
            'width': -9999,
            'width_ft': -9999,
            'width_m': -9999,
            'length': -9999,
            'length_ft': -9999,
            'length_m': -9999,
            'surface_type': 'n/a',
            'runway_a_number': '--',
            'runway_a_end_lat': -9999,
            'runway_a_end_lon': -9999,
            'runway_b_number': '--',
            'runway_b_end_lat': -9999,
            'runway_b_end_lon': -9999
        }
}


@struct_factory
def airport_struct(runway_struct, num_runways=0):
    # type: (RunwayStructure, int) -> Dict
    return {
        'fields': (
            ('icao', c_wchar_p),
            ('type', c_wchar_p),
            ('name', c_wchar_p),
            ('elevation', c_float),
            ('elevation_ft', c_float),
            ('elevation_m', c_float),
            ('iso_country', c_wchar_p),
            ('iso_region', c_wchar_p),
            ('max_runway_length_ft', c_float),
            ('max_runway_length_m', c_float),
            ('runways', runway_struct * num_runways)
        ),
        'defaults': {
            'icao': 'n/a',
            'type': 'n/a',
            'name': 'n/a',
            'elevation': -9999,
            'elevation_ft': -9999,
            'elevation_m': -9999,
            'iso_country': 'n/a',
            'iso_region': 'n/a',
            'max_runway_length_ft': -9999,
            'max_runway_length_m': -9999,
            'runways': (runway_struct * num_runways)()
        }
    }


from .airport import *
