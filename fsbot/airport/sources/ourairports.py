from peewee import fn, JOIN

from fsbot import log
from fsbot.maths import convert_unit
from fsbot.airport import runway_struct, airport_struct
from fsbot.airport.sources import AirportSourceError, BaseSource
from fsbot.airport.sources.models.oa import Airport, Runway


__all__ = [
    'OurAirports'
]


class OurAirports(BaseSource):
    """
    Get Airport data from ourairports database
    """
    
    idx = 2
    name = 'Our Airports'
    info = 'http://ourairports.com'


    airports = None


    # These are important for accessing actual table and column 
    # names (not Model attributes) for use with raw queries, so
    # they do not have to be explicitly defined here.
    # Only currently used columns/tables are listed here. Add 
    # new ones to all source classes as they're used.
    tbl_airport = Airport._meta.table_name
    tbl_runway = Runway._meta.table_name
    col_airport_icao = Airport.icao.column_name
    col_runway_icao = Runway.icao.column_name
    col_runway_rwy_a_end_lat = Runway.rwy_a_end_lat.column_name
    col_runway_rwy_a_end_lon = Runway.rwy_a_end_lon.column_name
    col_runway_rwy_b_end_lat = Runway.rwy_b_end_lat.column_name
    col_runway_rwy_b_end_lon = Runway.rwy_b_end_lon.column_name


    def __init__(self):
        super(OurAirports, self).__init__()


    def get_structure(self):

        airports = []

        for db_airport in self.airports:
        
            # Get a RunwayStruct instance
            RunwayStruct = runway_struct()

            runways = []

            # Assign values to struct fields. 
            # Do not create defaults for unused fields - defaults are set in struct definitions in __init__.py
            #
            # These bunches of ugly ifs are ensuring that fields are set to defaults (defined in the Strcuture's 
            # _defaults_ dict attribute) if values are unexpected.
            for db_runway in db_airport.runways:
                
                runway = RunwayStruct()
                
                if isinstance(db_runway.airport_ident, str): runway.icao = db_runway.airport_ident
                if db_runway.width_ft: 
                    runway.width = runway.width_ft = db_runway.width_ft
                    runway.width_m = convert_unit(db_runway.width_ft, 'ft', 'm')
                if db_runway.length_ft:
                    runway.length = runway.length_ft = db_runway.length_ft
                    runway.length_m = convert_unit(db_runway.length_ft, 'ft', 'm')
                if db_runway.surface: runway.surface_type = db_runway.surface
                if isinstance(db_runway.le_ident, str): runway.db_runway_a_number = db_runway.le_ident
                if db_runway.rwy_a_end_lat: runway.db_runway_a_end_lat = db_runway.rwy_a_end_lat
                if db_runway.rwy_a_end_lon: runway.db_runway_a_end_lon = db_runway.rwy_a_end_lon
                if isinstance(db_runway.he_ident, str): runway.db_runway_b_number = db_runway.he_ident
                if db_runway.rwy_b_end_lat: runway.db_runway_b_end_lat = db_runway.rwy_b_end_lat
                if db_runway.rwy_b_end_lon: runway.db_runway_b_end_lon = db_runway.rwy_b_end_lon
                
                runways.append(runway)

            # initialise AirportStruct with runway array size
            airport = airport_struct(RunwayStruct, len(runways))()

            if isinstance(db_airport.icao, str): airport.icao = db_airport.icao
            if isinstance(db_airport.type, str): airport.type = db_airport.type
            if isinstance(db_airport.name, str): airport.name = db_airport.name
            if db_airport.elevation_ft:
                airport.elevation = airport.elevation_ft = db_airport.elevation_ft
                airport.elevation_m = convert_unit(db_airport.elevation_ft, 'ft', 'm')
            if isinstance(db_airport.iso_country, str): airport.iso_country = db_airport.iso_country
            if isinstance(db_airport.iso_region, str): airport.iso_region = db_airport.iso_region
            if db_airport.max_runway_length: 
                airport.max_runway_length_ft = db_airport.max_runway_length
                airport.max_runway_length_m = convert_unit(db_airport.max_runway_length, 'ft', 'm')
            if len(runways) > 0: airport.runways = (RunwayStruct * len(runways))(*runways)
            
            airports.append(airport)

        return airports


    def base_airport_query(self):
        # type: () -> peewee.ModelSelect
        return Airport.select(
            Airport.icao,
            Airport.type,
            Airport.name,
            Airport.elevation_ft,
            Airport.iso_country,
            Airport.iso_region,
            #Runway.airport_ident,
            #Runway.width_ft,
            fn.MAX(Runway.length_ft).alias('max_runway_length'),
            #Runway.surface,
            #Runway.le_ident,
            #Runway.le_latitude_deg,
            #Runway.le_longitude_deg,
            #Runway.he_ident,
            #Runway.he_latitude_deg,
            #Runway.he_longitude_deg,
        ).join(Runway, join_type=JOIN.LEFT_OUTER, on=(Airport.id == Runway.airport_ref))


    def select(self, icaos):
        # type: (list) -> Union(AirportOA, NoneType)
        log.debug('icaos: {}'.format(icaos))
        
        airports = self.base_airport_query()
        self.airports =  airports.where(Airport.icao << [icao.upper() for icao in icaos]).group_by(Airport.icao)
        
        return self


    def search(self, search_term=None, icao_term=None, country=None, min_elevation=None, max_elevation=None, min_rwy_length=None, max_rwy_length=None, limit=None):
        """
        Do a database search using all the given criteria

        Parameters
        ----------
        search_term : Optional[str]
            Term to search for in airport name.
        icao_term : Optional[str]
            Airport ICAO, supports wild cards '*' or '?'.
        country : Optional[str]
            2-letter country code
        min_elevation : Optional[int]
            Minimum elevation constraint
        max_elevation : Optional[int]
            Maximum elevation constraint
        min_rwy_length : Optional[int]
            Minimum runway length constraint
        max_rwy_length : Optional[int]
            Maximum runway length constraint
        limit : Optional[int]
            Limit results

        Returns
        -------
            OurAirports
        """
        log.debug('FUNC PARAMS: {}'.format(locals()))
        
        airports = self.base_airport_query()

        try:
            int(max_rwy_length)
            airports = airports.where(Runway.length_ft <= max_rwy_length)
        except (TypeError, ValueError):
            pass

        try:
            int(min_rwy_length)
            airports = airports.where(Runway.length_ft >= min_rwy_length).order_by(Runway.length_ft.desc())
        except (TypeError, ValueError):
            pass

        if isinstance(search_term, str):
            airports = airports.where(Airport.name.contains(search_term))

        if isinstance(icao_term, str):
            # first replace wildcard characters to a proper symbol for use with ILIKE
            icao_term = icao_term.replace('?', '%').replace('*', '%')
            airports = airports.where(Airport.icao ** icao_term)

        if country:
            airports = airports.where(Airport.iso_country == country.upper())

        try:
            int(max_elevation)
            airports = airports.where(Airport.elevation_ft <= max_elevation)
        except (TypeError, ValueError):
            pass

        try:
            int(min_elevation)
            airports = airports.where(Airport.elevation_ft >= min_elevation)
            if min_rwy_length == None:
                airports = airports.order_by(Airport.elevation_ft.desc())
        except (TypeError, ValueError):
            pass

        if isinstance(limit, int):
            self.airports = airports.group_by(Airport.icao).limit(limit)
        else:
            self.airports = airports.group_by(Airport.icao)

        return self


    def get_coordinates(self, icao):
        # type: (str) -> tuple
        """
        Get tuple of coordinates from runway table or fallback to airport table.
        """
        result  = Runway.get_or_none(Runway.icao == icao.upper())

        if result == None or result.rwy_a_end_lat == None or result.rwy_a_end_lon == None:
            result = Airport.get_or_none(Airport.icao == icao.upper())
            if result == None or result.latitude_deg == None or result.longitude_deg == None:
                return None
            return (result.latitude_deg, result.longitude_deg)

        return (result.rwy_a_end_lat, result.rwy_a_end_lon)


    def get_country(self, icao):
        # type: (str) -> Union(dict, None)
        """
        Speciality function. Only OurAirports data has country information.
        """
        return Airport.get_or_none(Airport.icao == icao.upper())
