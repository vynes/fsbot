from peewee import FloatField, TextField, IntegerField, ForeignKeyField, SQL

from fsbot.data.model import BaseModel


__all__ = [
    'Airport',
    'Runway',
    'Country'
]


class Airport(BaseModel):
    id = IntegerField(unique=True)
    icao = TextField(unique=True, column_name='ident')
    name = TextField(null=True)
    continent = TextField(null=True)
    elevation_ft = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    gps_code = TextField(null=True)
    iata_code = TextField(null=True)
    iso_country = TextField(null=True)
    iso_region = TextField(null=True)
    latitude_deg = FloatField(null=True)
    local_code = TextField(null=True)
    longitude_deg = FloatField(null=True)
    municipality = TextField(null=True)
    scheduled_service = TextField(null=True)
    type = TextField(null=True)
    home_link = TextField(null=True)
    wikipedia_link = TextField(null=True)
    keywords = TextField(null=True)

    class Meta:
        table_name = 'airports_oa'
        primary_key = False


class Runway(BaseModel):
    id = IntegerField(unique=True)
    airport = ForeignKeyField(Airport, field=Airport.icao, column_name='airport_ident', backref='runways')
    airport_ref = IntegerField(index=True)
    icao = TextField(index=True, null=True, column_name='airport_ident')
    closed = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    he_displaced_threshold_ft = IntegerField(null=True)
    he_elevation_ft = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    he_heading_degt = FloatField(column_name='he_heading_degT', null=True)
    he_ident = TextField(null=True)
    rwy_a_end_lat = FloatField(null=True, column_name='le_latitude_deg')
    rwy_a_end_lon = FloatField(null=True, column_name='le_longitude_deg')
    le_displaced_threshold_ft = FloatField(null=True)
    le_elevation_ft = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    le_heading_degt = FloatField(column_name='le_heading_degT', null=True)
    le_ident = TextField(null=True)
    rwy_b_end_lat = FloatField(null=True, column_name='he_latitude_deg')
    rwy_b_end_lon = FloatField(null=True, column_name='he_longitude_deg')
    length_ft = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    lighted = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    surface = TextField(null=True)
    width_ft = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)

    class Meta:
        table_name = 'runways_oa'
        primary_key = False


class Country(BaseModel):
    id = IntegerField(unique=True)
    code = TextField(index=True)
    continent = TextField(null=True)
    keywords = TextField(null=True)
    name = TextField(null=True)
    wikipedia_link = TextField(null=True)

    class Meta:
        table_name = 'countries_oa'
        primary_key = False
