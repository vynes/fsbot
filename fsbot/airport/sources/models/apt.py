from peewee import FloatField, TextField, IntegerField, ForeignKeyField, SQL

from fsbot.data.model import BaseModel


__all__ = [
    'Surfacetype',
    'Approachlighting',
    'Helipad',
    'Airport',
    'Runway',
    'Runwaymarking',
    'Atccomtype',
    'Com',
    'Glossary',
    'Nav',
    'Parking',
    'Procstat'
]


class Surfacetype(BaseModel):
    code = IntegerField(unique=True)
    desc = TextField()

    class Meta:
        table_name = 'surfacetypes'
        primary_key = False


class Approachlighting(BaseModel):
    code = IntegerField(unique=True)
    desc = TextField()
    name = TextField()

    class Meta:
        table_name = 'approachlighting'
        primary_key = False


class Helipad(BaseModel):
    center_lat = FloatField()
    center_lon = FloatField()
    designator = TextField(constraints=[SQL("DEFAULT 'H1'")])
    edge_lighting = IntegerField()
    heading = FloatField()
    icao = TextField(index=True)
    length = FloatField()
    markings = IntegerField(constraints=[SQL("DEFAULT 0")])  # 
    shoulder_type = IntegerField()
    smoothness = FloatField()
    #surface_code = IntegerField()  # 
    surface_code = ForeignKeyField(Surfacetype, column_name='surface_code', field=Surfacetype.code, backref='surface_type')
    width = FloatField()
    xcode = IntegerField(index=True)

    class Meta:
        table_name = 'helipads'
        primary_key = False


class Airport(BaseModel):
    elevation = IntegerField()
    hastower = IntegerField()
    icao = TextField(unique=True)
    name = TextField()
    xcode = IntegerField(index=True)
    type = TextField(null=True)
    iso_country = TextField(null=True)

    class Meta:
        table_name = 'airports'
        primary_key = False


class Runway(BaseModel):
    airport = ForeignKeyField(Airport, field=Airport.icao, column_name='icao', backref='runways')
    icao = TextField(index=True)
    centerline_lights = IntegerField(constraints=[SQL("DEFAULT 0")])
    edge_lights = IntegerField(constraints=[SQL("DEFAULT 0")])
    length = FloatField()
    perimeter_buoys = IntegerField(constraints=[SQL("DEFAULT 0")])
    rwy_a_approach_lighting_code = ForeignKeyField(Approachlighting, column_name='rwy_a_approach_lighting_code', field=Approachlighting.code, backref='runways_a')
    #rwy_a_approach_lighting_code = IntegerField()
    rwy_a_blastpad_length = FloatField(constraints=[SQL("DEFAULT 0.00")])
    rwy_a_disp_threshold_length = FloatField()
    rwy_a_end_id_lights_code = IntegerField()
    rwy_a_end_lat = FloatField()
    rwy_a_end_lon = FloatField()
    rwy_a_marking_code = IntegerField()
    rwy_a_number = TextField()
    rwy_a_touchdown_zone_lighting_code = IntegerField()
    rwy_b_approach_lighting_code = ForeignKeyField(Approachlighting, column_name='rwy_b_approach_lighting_code', field=Approachlighting.code, backref='runways_b')
    #rwy_b_approach_lighting_code = IntegerField()
    rwy_b_blastpad_length = FloatField(constraints=[SQL("DEFAULT 0.00")])
    rwy_b_disp_threshold_length = FloatField()
    rwy_b_end_id_lights_code = IntegerField()
    rwy_b_end_lat = FloatField()
    rwy_b_end_lon = FloatField()
    rwy_b_marking_code = IntegerField()
    rwy_b_number = TextField()
    rwy_b_touchdown_zone_lighting_code = IntegerField()
    shoulder_type = IntegerField()
    smoothness = FloatField(constraints=[SQL("DEFAULT 0.25")])
    #surface_type = IntegerField()
    surface_type = ForeignKeyField(Surfacetype, column_name='surface_type', field=Surfacetype.code, backref='surface_type')
    width = FloatField()
    xcode = IntegerField(index=True)

    class Meta:
        table_name = 'runways'
        primary_key = False


class Runwaymarking(BaseModel):
    code = IntegerField(unique=True)
    desc = TextField()

    class Meta:
        table_name = 'runwaymarkings'
        primary_key = False


class Atccomtype(BaseModel):
    desc = TextField()
    name = TextField()
    xcode = IntegerField(index=True)

    class Meta:
        table_name = 'atccomtypes'
        primary_key = False


class Com(BaseModel):
    freq = IntegerField()
    icao = ForeignKeyField(Airport, column_name='icao', field=Airport.icao, backref='coms')
    #icao = TextField(index=True)
    name = TextField()
    #xcode = IntegerField(index=True)
    xcode = ForeignKeyField(Atccomtype, column_name='xcode', field=Atccomtype.xcode, backref='coms')

    class Meta:
        table_name = 'coms'
        primary_key = False


class Nav(BaseModel):
    bearing = FloatField()
    elevation = IntegerField()
    frequency = IntegerField()
    icao = TextField(index=True)
    lat = FloatField()
    locid = TextField()
    lon = FloatField()
    name = TextField()
    range = IntegerField()
    runway = TextField()

    class Meta:
        table_name = 'nav'
        primary_key = False


class Parking(BaseModel):
    heading = FloatField()
    icao = ForeignKeyField(Airport, field=Airport.icao, backref='parkings')
    #icao = TextField(index=True)
    lat = FloatField()
    lon = FloatField()
    name = TextField()
    xcode = IntegerField(constraints=[SQL("DEFAULT 15")], index=True)

    class Meta:
        table_name = 'parkings'
        primary_key = False


class Procstat(BaseModel):
    num_airports = IntegerField(null=True)
    num_coms = IntegerField(null=True)
    num_helipads = IntegerField(null=True)
    num_parkings = IntegerField(null=True)
    num_runways = IntegerField(null=True)

    class Meta:
        table_name = 'procstats'
        primary_key = False
