# Do NOT do `from .apt import *` or `from oa import *` here because 
# DB model names are the same. They should be imported in modules 
# using full path: 
#
#`from fsbot.airport.sources.models import apt, oa`
# then used like apt.Airport or oa.Airport etc...  
#
# Or if used in context of one specific model:
#`from fsbot.airport.sources.models.oa import Airport`
