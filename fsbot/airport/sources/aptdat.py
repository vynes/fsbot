from typing import Optional, List, Tuple, Dict
from peewee import fn, JOIN

from fsbot import log
from fsbot.maths import convert_unit
from fsbot.airport import runway_struct, airport_struct
from fsbot.airport.sources import AirportSourceError, BaseSource
from fsbot.airport.sources.models.apt import Airport, Runway


__all__ = [
    'AptAirports'
]


class AptAirports(BaseSource):
    """
    Get Airport data from apt.dat
    """
    
    idx = 1
    name = 'APT.DAT'
    info = 'https://developer.x-plane.com/article/airport-data-apt-dat-file-format-specification/'

    airports = None

    # These are important for accessing actual table and column 
    # names (not Model attributes) for use with raw queries, so
    # they do not have to be explicitly defined here.
    # Only currently used columns/tables are listed here. Add 
    # new ones to all source classes as they're used.
    tbl_airport = Airport._meta.table_name
    tbl_runway = Runway._meta.table_name
    col_airport_icao = Airport.icao.column_name
    col_runway_icao = Runway.icao.column_name
    col_runway_rwy_a_end_lat = Runway.rwy_a_end_lat.column_name
    col_runway_rwy_a_end_lon = Runway.rwy_a_end_lon.column_name
    col_runway_rwy_b_end_lat = Runway.rwy_b_end_lat.column_name
    col_runway_rwy_b_end_lon = Runway.rwy_b_end_lon.column_name


    def __init__(self):
        super(AptAirports, self).__init__()


    def get_structure(self) -> List[Optional[Dict]]:
        airports = []
        
        for db_airport in self.airports:
            
            # Get a RunwayStruct instance
            RunwayStruct = runway_struct()

            runways = []

            # Assign values to struct fields. 
            # Do not create defaults for unused fields - defaults are set in struct definitions in __init__.py
            #
            # These bunches of ugly ifs are ensuring that fields are set to defaults (defined in the Strcuture's 
            # _defaults_ dict attribute) if values are unexpected.
            for db_runway in db_airport.runways:
                
                runway = RunwayStruct()

                if isinstance(db_runway.icao, str): runway.icao = db_runway.icao
                if db_runway.width: 
                    runway.width = runway.width_m = db_runway.width
                    runway.width_ft = convert_unit(db_runway.width, 'm', 'ft')
                if db_runway.length:
                    runway.length = runway.length_m = db_runway.length
                    runway.length_ft = convert_unit(db_runway.length, 'm', 'ft')
                if db_runway.surface_type: runway.surface_type = db_runway.surface_type.desc
                if isinstance(db_runway.rwy_a_number, str): runway.runway_a_number = db_runway.rwy_a_number
                if db_runway.rwy_a_end_lat: runway.runway_a_end_lat = db_runway.rwy_a_end_lat
                if db_runway.rwy_a_end_lon: runway.runway_a_end_lon = db_runway.rwy_a_end_lon
                if isinstance(db_runway.rwy_b_number, str): runway.runway_b_number = db_runway.rwy_b_number
                if db_runway.rwy_b_end_lat: runway.runway_b_end_lat = db_runway.rwy_b_end_lat
                if db_runway.rwy_b_end_lon: runway.runway_b_end_lon = db_runway.rwy_b_end_lon
                
                runways.append(runway)

            # initialise AirportStruct with runway array size
            airport = airport_struct(RunwayStruct, len(runways))()

            if isinstance(db_airport.icao, str): airport.icao = db_airport.icao
            if isinstance(db_airport.type, str): airport.type = db_airport.type
            if isinstance(db_airport.name, str): airport.name = db_airport.name
            if db_airport.elevation:
                airport.elevation = airport.elevation_ft = db_airport.elevation
                airport.elevation_m = convert_unit(db_airport.elevation, 'ft', 'm')
            if isinstance(db_airport.iso_country, str): airport.iso_country = db_airport.iso_country
            if db_airport.max_runway_length: 
                airport.max_runway_length_ft = convert_unit(db_airport.max_runway_length, 'm', 'ft')
                airport.max_runway_length_m = db_airport.max_runway_length
            if len(runways) > 0: airport.runways = (RunwayStruct * len(runways))(*runways)
            
            airports.append(airport)

        return airports


    def base_airport_query(self):
        # type: () -> peewee.ModelSelect
        return Airport.select(
            Airport.icao,
            Airport.type,
            Airport.name,
            Airport.elevation,
            Airport.iso_country,
            #Airport.iso_region,
            #Runway.airport_ident,
            #Runway.width_ft,
            fn.MAX(Runway.length).alias('max_runway_length'),
            #Runway.surface,
            #Runway.le_ident,
            #Runway.le_latitude_deg,
            #Runway.le_longitude_deg,
            #Runway.he_ident,
            #Runway.he_latitude_deg,
            #Runway.he_longitude_deg,
        ).join(Runway, join_type=JOIN.LEFT_OUTER, on=(Airport.icao == Runway.icao))


    def select(self, icaos):
        # type: (list) -> Union(AirportOA, NoneType)
        log.debug('icaos: {}'.format(icaos))
        
        airports = self.base_airport_query()
        self.airports =  airports.where(Airport.icao << [icao.upper() for icao in icaos]).group_by(Airport.icao)
        
        return self


    def search(self, search_term=None, icao_term=None, country=None, min_elevation=None, max_elevation=None, min_rwy_length=None, max_rwy_length=None, limit=None):
        """
        Do a database search using all the given criteria

        Parameters
        ----------
        search_term : Optional[str]
            Term to search for in airport name.
        icao_term : Optional[str]
            Airport ICAO, supports wild cards '*' or '?'.
        country : Optional[str]
            2-letter country code
        min_elevation : Optional[int]
            Minimum elevation constraint
        max_elevation : Optional[int]
            Maximum elevation constraint
        min_rwy_length : Optional[int]
            Minimum runway length constraint
        max_rwy_length : Optional[int]
            Maximum runway length constraint
        limit : Optional[int]
            Limit results

        Returns
        -------
            AptAirports
        """
        log.debug('FUNC PARAMS: {}'.format(locals()))
        
        airports = self.base_airport_query()

        # Convert runways to metres
        # apt.dat has elevation units in ft and runway length units in metres.
        # This method expects all units in ft, so runway values must be converted to metres before executing search query.
        # Besides this conversion, no more worries - mixed unit weirdness already dealt with in creation of AIRPORT Structure.
        min_rwy_length = convert_unit(min_rwy_length, 'ft', 'm') if min_rwy_length != None else min_rwy_length
        max_rwy_length = convert_unit(max_rwy_length, 'ft', 'm') if max_rwy_length != None else max_rwy_length

        try:
            int(max_rwy_length)
            airports = airports.where(Runway.length <= max_rwy_length)
        except (TypeError, ValueError):
            pass

        try:
            int(min_rwy_length)
            airports = airports.where(Runway.length >= min_rwy_length).order_by(Runway.length.desc())
        except (TypeError, ValueError):
            pass

        if isinstance(search_term, str):
            airports = airports.where(Airport.name.contains(search_term))

        if isinstance(icao_term, str):
            # first replace wildcard characters to a proper symbol for use with ILIKE
            icao_term = icao_term.replace('?', '%').replace('*', '%')
            airports = airports.where(Airport.icao ** icao_term)

        if country:
            airports = airports.where(Airport.iso_country == country.upper())

        try:
            int(max_elevation)
            airports = airports.where(Airport.elevation <= max_elevation)
        except (TypeError, ValueError):
            pass

        try:
            int(min_elevation)
            airports = airports.where(Airport.elevation >= min_elevation)
            if min_rwy_length == None:
                airports = airports.order_by(Airport.elevation.desc())
        except (TypeError, ValueError):
            pass

        if isinstance(limit, int):
            self.airports = airports.group_by(Airport.icao).limit(limit)
        else:
            self.airports = airports.group_by(Airport.icao)

        return self


    def get_coordinates(self, icao: str) -> Tuple:
        """
        Get tuple of coordinates from runway table.
        """
        result = Runway.get_or_none(Runway.icao == icao.upper())
        if result == None or result.rwy_a_end_lat == None or result.rwy_a_end_lon == None:
            return None
        return (result.rwy_a_end_lat, result.rwy_a_end_lon)
