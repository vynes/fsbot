from typing import Optional, Union, Tuple, List, Dict
import os, math, sqlite3

from fsbot import log, cfg
from fsbot.airport.sources import source_registry, AirportSourceError
from fsbot.data import DB, OperationalError
from fsbot.maths import get_proximity_bounding_coordinates, convert_unit, geodist, bearing


__all__ = [
    'available_sources',
    'Airport',
    'AirportError',
    'get_airport_coordinates'
]


def available_sources():
    return source_registry


class AirportError(Exception):
    pass


class Airport(object):
    """
    Selects airport data sources, no fallback.

    use_source: specify source name or None to use default (source with smallest index).

    """
    
    def __init__(self, use_source_idx: int = 0):
        # Find all airprt sources
        self.sources = available_sources()
        log.debug('Airport sources: {}'.format(self.sources))

        self.source_cls = None

        # Source(s) to use.
        # There is no failover; either specified source or default will be used.
        # Class with smallest idx will be considered default.
        if use_source_idx in (0, None):
            self.source_cls = self.sources[min(self.sources.keys())]['cls']
        elif use_source_idx in self.sources:
            self.source_cls = self.sources[use_source_idx]['cls']
        else:
            raise AirportError('No valid airport sources discovered')

        log.debug('using Airport source: {}'.format(self.source_cls.name))


    def select(self, icaos: List) -> Dict:
        return self.source_cls().select(icaos).get_structure()


    def search(self, **kwargs):
        return self.source_cls().search(**kwargs).get_structure()


    def coordinates(self, icao: str) ->Tuple:
        return self.source_cls().get_coordinates(icao)


    def nearest(self, 
            icao: str = None, 
            coords: Optional[List[float]] = None, 
            proximity_radius: int = 300, 
            limit: Optional[int] = None, 
            min_distance: int = 0, 
            nmi: bool = False, 
            metar_only: bool = False
        ):
        """
        Get list of nearest ICAOs from given icao or coordinates within proximity_radius.

        proximity_radius:
            radius in km (or NM if nmi==True) used to calculate bounding coordinates.

        min_distance:
            only return airports with minimum distance of min_distance km 
            (or NM if nmi==True) from icao.

        nmi:
            set to True if values in nautical miles

        metar_only == True: 
            returns airports using some specific criteria in order to avoid filling
            number of results with surrounding small airports without metar station.

        ref: http://janmatuschek.de/LatitudeLongitudeBoundingCoordinates 

        """
        result = []
        
        # Set origin coordinates to either ICAO's or provided.
        if coords == None:
            coords = self.coordinates(icao)
        else:
            icao = None

        if nmi == True: 
            proximity_radius = convert_unit(proximity_radius, 'NM', 'km')
            min_distance = convert_unit(min_distance, 'NM', 'km')
        
        try:
            ref_lat_deg, ref_lon_deg = (float(coords[0]), float(coords[1]))
        except (ValueError, TypeError, IndexError):
            # Return empty list
            return result
        
        # Get DB instance
        dbfile = os.path.expanduser(cfg.get('DATABASE', 'AirportSqliteFile'))
        aptdbconn = DB(dbfile, mapping='Row')

        # Catch user function exceptions
        user_func_error_list = {}
        log_level = log.getEffectiveLevel()
        # Set True to enable user function tracebacks:
        if log_level == 10: sqlite3.enable_callback_tracebacks(True)

        def do_user_func_log(func, e):
            if log_level == 10:
                func = func + '_' + e.__class__.__name__
                user_func_error_list[func] = [0, e] if func not in user_func_error_list else [user_func_error_list[func][0] + 1, e]
        def user_acos(x):
            try:
                return math.acos(x)
            except (ValueError, TypeError) as e:
                do_user_func_log('acos', e)
        def user_sin(x):
            try:
                return math.sin(x)
            except (ValueError, TypeError) as e:
                do_user_func_log('sin_', e)
        def user_cos(x):
            try:
                return math.cos(x)
            except (ValueError, TypeError) as e:
                do_user_func_log('cos', e)
        def user_radians(x):
            try:
                return math.radians(x)
            except (ValueError, TypeError) as e:
                do_user_func_log('radians', e)
        def user_len(x):
            try:
                return len(x)
            except (ValueError, TypeError) as e:
                do_user_func_log('len', e)

        # Create functions to use in query
        aptdbconn.conn.create_function("acos", 1, user_acos)
        aptdbconn.conn.create_function("sin", 1, user_sin)
        aptdbconn.conn.create_function("cos", 1, user_cos)
        aptdbconn.conn.create_function("radians", 1, user_radians)
        aptdbconn.conn.create_function("len", 1, user_len)

        # Convert to rads
        ref_lat_rad = math.radians(ref_lat_deg)
        ref_lon_rad = math.radians(ref_lon_deg)

        # {"angular_radius": angular_radius, "min":(lat_min, lon_min), "max":(lat_max, lon_max)}
        bounding_coords = get_proximity_bounding_coordinates(ref_lat_deg, ref_lon_deg, proximity_radius, True)
        log.debug(bounding_coords);

        # TODO: might want to lookup table name in DB catalogue instead of
        # param substitution here, (coz sql injection). Or cleaning function 
        # stripping table names of special chars.
        
        proximity_query = 'SELECT * FROM ( ' \
                'SELECT {icao},{rwy_a_end_lat}, {rwy_a_end_lon} FROM {runways} WHERE ' \
                    '{metar_icao_len}' \
                    '{rwy_a_end_lat} is not NULL AND {rwy_a_end_lon} is not NULL AND ' \
                    '(radians({rwy_a_end_lat}) >= (?) AND radians({rwy_a_end_lat}) <= (?)) ' \
                    'AND ' \
                    '(radians({rwy_a_end_lon}) >= (?) AND radians({rwy_a_end_lon}) <= (?)) ' \
                    ') WHERE ' \
                        '(acos(sin(?) * sin(radians({rwy_a_end_lat})) + cos(?) * cos(radians({rwy_a_end_lat})) * cos(radians({rwy_a_end_lon}) - ?)) <= (?))'.format (
                            icao=self.source_cls.col_runway_icao,
                            rwy_a_end_lat=self.source_cls.col_runway_rwy_a_end_lat,
                            rwy_a_end_lon=self.source_cls.col_runway_rwy_a_end_lon,
                            runways=self.source_cls.tbl_runway,
                            metar_icao_len='len({}) = 4 AND '.format(self.source_cls.col_runway_icao) if metar_only == True else ''
                        )
        
        log.debug('proximity_query: {}'.format(proximity_query))
        
        aptdbconn.c.execute(
            proximity_query, 
            (
                bounding_coords["min"][0],
                bounding_coords["max"][0],
                bounding_coords["min"][1],
                bounding_coords["max"][1],
                ref_lat_rad,
                ref_lat_rad,
                ref_lon_rad,
                bounding_coords["angular_radius"]
            )
        )
        
        try:
            nearest_airports = aptdbconn.c.fetchall()
        except OperationalError as e:
            log.error("{}".format(e.message))
            aptdbconn.close()
            raise
            
        aptdbconn.close()
        
        log.debug('user func errors: {}'.format(user_func_error_list))

        previous_icao = "None"


        # Jux the pose db result with distances
        for nearest_airport in nearest_airports:
            
            # Skip duplicates
            # TODO: Assumes runways are grouped by icao in DB, use order by.
            # TODO: Probably use some unique sequence here 
            if nearest_airport[self.source_cls.col_runway_icao] == previous_icao:
                continue
            previous_icao = nearest_airport[self.source_cls.col_runway_icao]


            # Distance to nearest
            distance = geodist(
                ref_lat_deg, ref_lon_deg, 
                nearest_airport[self.source_cls.col_runway_rwy_a_end_lat], 
                nearest_airport[self.source_cls.col_runway_rwy_a_end_lon]
            )

            distance_km = convert_unit(distance, 'm', 'km')

            # Bearing from ICAO to nearest
            bearing_to_nearest = bearing(coords[0], coords[1], nearest_airport[self.source_cls.col_runway_rwy_a_end_lat], nearest_airport[self.source_cls.col_runway_rwy_a_end_lon])


            if distance_km > min_distance and icao != nearest_airport[self.source_cls.col_runway_icao]:
                result.append(
                    (
                        nearest_airport[self.source_cls.col_runway_icao], 
                        convert_unit(distance, 'm', 'NM'), 
                        convert_unit(distance, 'm', 'km'),
                        bearing_to_nearest
                    )
                )

        log.debug("Closest ICAOs: {}".format(result))

        # return sorted and limited
        # limit of None will take all in result list
        return sorted(result, key=lambda closest: closest[1])[:limit]


def get_airport_coordinates(icaos: Union[str, List]) -> Dict:
    """
    Expects a string or a list.

    Returns an ICAO indexed dict containing tuples of coordinates.
    """
    result = {}

    icaos = [icaos] if not isinstance(icaos, list) else icaos

    for icao in icaos:
        result[icao] = Airport().coordinates(icao)

    return result
