from dataclasses import dataclass, field
from typing import List, Dict

from fsbot.exceptions import throw
from fsbot import log
from fsbot.rfactor import source_registry
from fsbot.connectivity import http, NetError


__all__ = [
    'live_info',
    'session_info',
    'RfServerError'
]


class RfServerError(Exception):
    pass


# Types of sessions
# From InternalsPlugin.h:
#    // current session
#    // 0=testday
#    // 1-4=practice
#    // 5-8=qual
#    // 9=warmup
#    // 10-13=race
session_types = (
    'TD',
    'P1', 'P2', 'P3', 'P4', 
    'Q1', 'Q2', 'Q3', 'Q4',
    'WU',
    'R1', 'R2', 'R3', 'R4'
)

# Vehicle control types
# From InternalsPlugin.h:
#    // who's in control
#    // -1=nobody (shouldn't get this)
#    // 0=local player
#    // 1=local AI
#    // 2=remote
#    // 3=replay (shouldn't get this)
vehicle_control_types = (
    'LOCALPLAYER', 'LOCALAI', 'REMOTE', 'REPLAY'
)

# Race/game phases
# From IntermalsPlugin.h:
#    // Game phases:
#    // 0 Before session has begun
#    // 1 Reconnaissance laps (race only)
#    // 2 Grid walk-through (race only)
#    // 3 Formation lap (race only)
#    // 4 Starting-light countdown has begun (race only)
#    // 5 Green flag
#    // 6 Full course yellow / safety car
#    // 7 Session stopped
#    // 8 Session over
race_phases = (
    'PRESESSION', 'RECONLAPS', 'GRIDWALKTHRU', 
    'FORMATIONLAP', 'STARTCOUNTDOWN', 
    'GREENFLAG', 'SAFETYCAR', 
    'SESSIONSTOP', 'SESSIONOVER'
)

# Full-course yellow / safetycar state
# From IntermalsPlugin.h:
#    // Yellow flag states (applies to full-course only)
#    // -1 Invalid
#    //  0 None
#    //  1 Pending
#    //  2 Pits closed
#    //  3 Pit lead lap
#    //  4 Pits open
#    //  5 Last lap
#    //  6 Resume
#    //  7 Race halt (not currently used)
yellow_states = [
    'NONE', 'PENDING', 
    'PITSCLOSED', 'PITLEADLAP', 'PITSOPEN', 
    'LASTLAP', 'RESUME', 'RACEHALT'
]


def rain_type(value):
    rain = 'NORAIN'
    if value > 0:
        if value < 0.4:
            rain = 'LIGHT RAIN'
        elif value < 0.7:
            rain = 'MODERATE RAIN'
        else:
            rain = 'HEAVY RAIN'
    return rain


@dataclass
class LiveVehicleInfoData:
    Driver: str
    ID: int
    Vehicle: str
    Laps: int
    Sector: int
    FinishStatus: int
    LapDist: float
    PathLat: float
    RelevantTrackEdge: float
    Best: List[float] #[23.075, 49.809, 89.607],
    Last: List[float] #[30.700, 57.458, 97.241],
    CurrentSector1: float
    CurrentSector2: float
    Pitstops: int
    Penalties: int

    # Whether this is the player's vehicle
    IsPlayer: int

    Control: int
    InPits: int
    LapStartET: float
    Place: int
    VehicleClass: str
    TimeBehindNext: float
    LapsBehindNext: int
    TimeBehindLeader: float
    LapsBehindLeader: int
    Pos: List[float] #[222.102,8.157,-88.668]

    ## NON-INITVARS ##

    ControlType: str = field(init=False)
    ControlIsAI: bool = field(init=False)

    def __post_init__(self):
        self.ControlType = vehicle_control_types[self.Control]
        self.ControlIsAI = self.ControlType == 'LOCALAI'


@dataclass
class LiveInfoData:
    LiveInfoVersionMajor: str
    LiveInfoVersionMinor: str
    TrackName: str
    Session: int
    NumVehicles: int
    CurET: float
    EndET: float
    MaxLaps:int
    LapDist: float
    AiMuted: int
    GamePhase: int
    YellowFlagState: int
    SectorFlags: List[int]
    InRealtime: int
    StartLight: int
    NumRedLights: int
    PlayerName: str
    PlrFileName: str
    DarkCloud: float
    Raining: float
    AmbientTemp: float
    TrackTemp: float
    Wind: List[float]
    MinPathWetness: float
    MaxPathWetness: float
    VehicleInfo: List[LiveVehicleInfoData] = field(default_factory=list)

    ## NON-INITVARS ##

    SessionType: str = field(init=False)
    SessionIsRace: bool = field(init=False)

    RacePhase: str = field(init=False)
    SessionIsOver: bool = field(init=False)
    PhaseIsSafetyCar: bool = field(init=False)

    YellowFlagStateName: str = field(init=False)

    WxIsRaining: bool = field(init=False)
    WxRainType: str = field(init=False)

    def __post_init__(self):
        self.SessionType = session_types[self.Session]
        self.SessionIsRace = self.SessionType in ['R1', 'R2', 'R3', 'R4']
        self.RacePhase = race_phases[self.GamePhase]
        self.SessionIsOver = self.RacePhase == 'SESSIONOVER'
        self.PhaseIsSafetyCar = self.RacePhase == 'SAFETYCAR'
        self.YellowFlagStateName = yellow_states[self.YellowFlagState]
        self.WxIsRaining = self.Raining > 0
        self.WxRainType = rain_type(self.Raining)


@dataclass
class SessionAIVehicleData:
    pass


@dataclass
class SessionDetails:
    ai_vehicles: List[SessionAIVehicleData]
    aids: int # ex: 6160
    allowCCTransfers: int # ex: 2
    damage_mult: int # ex: 70
    finish_type: int # ex: 2
    fixed_setups: bool
    fixed_upgrades: bool
    game_name: str # "rFactor2"
    in_development: bool
    is_anonymous: bool
    is_closed: bool
    laps_left: int # ex: 2147483647,
    mod_id: str # ex: "AAA991",
    mod_version: str # ex: "1.0",
    package_name: str # ex: "R3RR Honda BTCC ZandvoortGP.rfmod"
    packed_flags: int # ex: 55
    ping: int
    rules: int # ex: 2
    session: int # ex: 1
    shared_skins: bool
    shared_vehs: bool
    special_pack: int # ex: 0
    stream: int # ex: 0
    time_left: int # ex: 6673

    ## NON-INITVARS
    session_type: str = field(init=False)

    def __post_init__(self):
        self.session_type = session_types[self.session]


@dataclass
class SessionModInfo:
    layout: str # ex: "Zandvoort GP"
    location: str # ex: "Zandvoort"
    name: str # ex: "R3RR Honda BTCC ZandvoortGP"
    sig: str # ex: "777fd2abd0e6ce48070629856116f9834014e699b09bb6af8b481e037ced51f1"


@dataclass
class SessionPlayer:
    pass


@dataclass
class SessionInfoData:
    build: int # ex: 1117
    details: SessionDetails
    httpPort: int # ex: 64297
    isiOfficial: bool
    lanIp: str # ex: "0.0.0.0"
    maxPlayers: int # ex: 45
    mod: SessionModInfo
    name: str # ex: "R3RR.COM"
    passwordProtected: bool
    playerCount: int # ex: 1
    players: List[SessionPlayer]
    simulationPort: int # ex: 54297
    wanIp: str # ex: "0.0.0.0"


def live_info(source_id: int) -> LiveInfoData:
    try:
        result = http.request(source_registry[source_id]['live_info_url'], json_decode=True, timeout=5)
    except NetError as e:
        log.error(f'{e}.')
        raise throw(RfServerError(f'Server error: {e}.'))

    if not all(x in result for x in ['LiveInfoVersionMajor', 'LiveInfoVersionMinor', 'VehicleInfo']):
        log.error(f'Server error: Unexpected return from server.')
        raise throw(RfServerError(f'Server error: Unexpected return from server.'))

    vinfo_result = result.pop('VehicleInfo')
    vinfo_list = []

    for v in vinfo_result:
        vinfo_list.append(LiveVehicleInfoData(**v))

    return LiveInfoData(**result, VehicleInfo=vinfo_list)


def session_info(source_id: int) -> SessionInfoData:
    try:
        result = http.request(source_registry[source_id]['session_info_url'], json_decode=True, timeout=5)
    except NetError as e:
        log.error(f'{e}.')
        raise throw(RfServerError(f'Server error: {e}.'))

    if 'Module Error' in result:
        log.error(f'Server error: {result["Module Error"]}.')
        raise throw(RfServerError(f'Server error: {result["Module Error"]}.'))

    if not all(x in result for x in ['build', 'details', 'players']):
        log.error(f'Server error: Unexpected return from server.')
        raise throw(RfServerError(f'Server error: Unexpected return from server.'))

    ai_vehicles_result = result['details'].pop('ai_vehicles')
    ai_vehicles_list = []
    for av in ai_vehicles_result:
        ai_vehicles_list.append(SessionAIVehicleData(**av))

    session_details = SessionDetails(**result.pop('details'), ai_vehicles=ai_vehicles_list)

    mod_info = SessionModInfo(**result.pop('mod'))

    players_result = result.pop('players')
    players_list = []
    for p in players_result:
        players_list.append(SessionPlayer(**p))

    return SessionInfoData(**result, details=session_details, mod=mod_info, players=players_list)
