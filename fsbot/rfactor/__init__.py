from fsbot import cfg


__all__ = [
    'source_registry'
]


source_registry = {}

def discover_sources():
    if 'RFACTOR_SERVERS' in cfg.sections():
        for idx,value in cfg['RFACTOR_SERVERS'].items():
            values = value[1:].split(value[0],3)
            source_registry[int(idx)] = {
                'name': values[0],
                'web': values[1],
                'session_info_url': values[2],
                'live_info_url': values[3]
            }


discover_sources()

from .rfactor2 import *
