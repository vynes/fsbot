# coding=utf-8
from __future__ import absolute_import, division, print_function, \
    unicode_literals

from ctypes import c_int, c_wchar_p, c_float

from fsbot.struct import BaseStructure



__all__ = [
    'PilotStruct'
]



class PilotStruct(BaseStructure):
    _fields_ = [
        ('server', c_wchar_p),
        ('callsign', c_wchar_p),
        ('uid', c_wchar_p),
        ('name', c_wchar_p),
        ('client_type', c_wchar_p),
        ('latitude', c_float),
        ('longitude', c_float),
        ('altitude', c_float),
        ('groundspeed', c_float),
        ('heading', c_float),
        ('aircraft_info', c_wchar_p),
        ('start_time', c_wchar_p),
        ('planned_aircraft', c_wchar_p),
        ('planned_destination_airport', c_wchar_p),
        ('planned_departure_airport', c_wchar_p),
        ('planned_route', c_wchar_p),

        # When relative value (ex: distance) of a reference point 
        # is queried, or any other arbitrary data is required, 
        # use this to store the name and value.
        ('reference_point_name', c_wchar_p),
        ('reference_point_value', c_float)
    ]

    # Set defaults
    _defaults_ = {
        'server': 'n/a',
        'callsign': 'n/a',
        'uid': 'n/a',
        'name': 'n/a',
        'client_type': 'n/a',
        'latitude': -9999,
        'longitude': -9999,
        'altitude': -9999,
        'groundspeed': -9999,
        'heading': -9999,
        'aircraft_info': 'n/a',
        'start_time': 'n/a',
        'planned_aircraft': 'n/a',
        'planned_destination_airport': 'n/a',
        'planned_departure_airport': 'n/a',
        'planned_route': 'n/a',

        # When relative value (ex: distance) of a reference point 
        # is queried, or any other arbitrary float data is required, 
        # use this to store the name and value.
        'reference_point_name': 'n/a',
        'reference_point_value': -9999
    }



from .mp import *
