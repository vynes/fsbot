from fsbot.mp.sources import source_registry, MPSourceError
from fsbot.exceptions import throw


__all__ = [
    'available_sources',
    'MP',
    'MPError'
]


def available_sources():
    return source_registry


class MPError(Exception):
    pass


class MP(object):
    """
    Selects mp server data sources, no fallback.

    use_source: specify source name or None to use default.

    """
    
    def __init__(self, use_source_idx=None):
        # type: (Union[int, NoneType, str]) -> None

        # Find all news sources
        self.sources = source_registry


        # Source(s) to use.
        # Only specified source will be used with no fallback, 
        # else first if none specified.
        if use_source_idx in (0, None, 'any', 'all'):
            self.use_source_idx = min(self.sources.keys())
        else:
            if use_source_idx in self.sources:
                self.use_source_idx = use_source_idx
            else:
                self.use_source_idx = 0

        self.source_cls = self.sources[self.use_source_idx]['cls']


    def search(self, more=None, **kwargs):
        # type: (Optional[Str], Optional[Str]) -> Union(List[PilotStruct], None]
        return self.source_cls(more=more).search_pilots(**kwargs)
