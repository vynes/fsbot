from peewee import fn, JOIN

from fsbot import log, cast_from_ctype
from fsbot.exceptions import throw
from fsbot.mp import PilotStruct
from fsbot.mp.sources import MPSourceError, BaseSource


__all__ = [
    'VatSim',
    'VatSimError'
]


class VatSimError(Exception):
    pass


class VatSim(BaseSource):
    """VatSim"""
    
    #name = 'vatsim'

    def __init__(self):
        
        super(VatSim, self).__init__()

    def search_pilots(self):
        pass
    