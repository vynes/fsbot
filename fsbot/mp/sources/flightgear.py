import re, json
from peewee import fn, JOIN

from fsbot import log, cast_from_ctype
from fsbot.exceptions import throw
from fsbot.mp import PilotStruct
from fsbot.mp.sources import MPSourceError, BaseSource
from fsbot.connectivity import http, telnet
from fsbot.airport import get_airport_coordinates
from fsbot.maths import geodist


__all__ = [
    'FlightGear',
    'FlightGearError'
]


class FlightGearError(Exception):
    pass


class FlightGear(BaseSource):
    """FlightGear"""
    
    idx = 1
    name = 'FlightGear'
    info = 'https://fgtracker.us.to'

    # API
    api_url = 'https://fgtracker.us.to/modules/fgtracker/interface.php'
    
    api_url_params = {
        'action': 'livepilots',
        'wpt': 'n'
    }


    # Telnet
    telnet_address = 'mpserver01.flightgear.org'
    telnet_port = 5001


    # ATC models
    atc_models = [
        'openradar',
        'atc',
        'atc2',
        'atc-fs',
        'atc-pie',
        'atc-ml',
        'atc-tower'
    ]


    def __init__(self, more=False):
        
        super(FlightGear, self).__init__()

        self.more = more

        # Get more data - makes more queries and takes longer
        if more == True:
            # Get waypoint data
            self.api_url_params['wpt'] = 'y'

        # holds raw live data
        self.live_data = self.fetch_data()     # type: dict


    def get_model_from_path(self, mstring):
        # type: (Text) -> Text
        """
        Get model name from sim model path string.
        """
        mdldir = ''
        mdlparts = mstring.split('/')
        mdl = mdlparts[len(mdlparts)-1]

        if mdlparts[0] == 'Aircraft' and (len(mdlparts) == 4 or len(mdlparts) == 3):
            mdldir = mdlparts[1]
        elif mdlparts[0] == 'Models' and len(mdlparts) == 2:
            mdldir = mdlparts[1][:-4]

        if mdl.lower().endswith('.xml'):
            return "{} ({})".format(mdldir, mdl[:-4])
        
        return mdl

    
    def parse_tracker_data(self, data):
        # type: (Str) -> List[Dict]
        """
        Parse tracker telnet result and return all information absent in API result.

        callsign is left in so api & telnet results can be matched.
        """
        result = []

        log.debug(data)

        data = data.split('\n')


        for pilot_line in data:
            if pilot_line[0:1] != "#" and len(pilot_line) > 0:

                # First split by @ because pilots are allowed spaces in their names
                pilot_callsign, rest_of_line = pilot_line.split('@', 1)

                # Get rest of line
                pilotinfo = rest_of_line.split(" ")

                # Aircraft model
                pilot_model = self.get_model_from_path(pilotinfo[len(pilotinfo)-1])

                log.debug('TELNET PILOT: {}, {}, {}'.format(pilot_callsign, pilotinfo, pilot_model))

                result.append(
                    {
                        "callsign": pilot_callsign,
                        "model": pilot_model,
                        "server": pilotinfo[0][:-1],
                        "lat": float(pilotinfo[4]),
                        "lon": float(pilotinfo[5]),
                        "alt": float(pilotinfo[6])
                    }
                )
        
        return result


    def fetch_from_api(self):
        # type: () -> List[Dict]
        """
        Get result from api and return list of dict (json decoded).
        """
        try:
            result =  http.request('{}?{}'.format(self.api_url, http.url_encode(self.api_url_params)))
        except http.NetError as e:
            log.error('Error getting data from {}. Error: {}'.format(self.name, e))
            raise ValueError(e)

        log.debug('API RESULT: {}'.format(result))

        try:
            return json.loads(result)['data']['pilot']
        except (IndexError, json.decoder.JSONDecodeError) as e:
            raise ValueError(e)


    def fetch_from_telnet(self):
        # type: () -> List[Dict]
        """
        Get result from tracker telnet, get it parsed and return dict.
        """
        try:
            result = telnet.telnet_get(host=self.telnet_address, port=self.telnet_port)
        except http.TelNetError as e:
            log.error('Error getting data from {}. Error: {}'.format(self.name, e))
            raise ValueError(e)
        
        log.debug('TELNET RESULT: {}'.format(result))

        return self.parse_tracker_data(result)
        

    def fetch_data(self):
        # type: () -> Dict
        """
        Get and return API results.
        """
        result = {'telnet': [], 'api': []}
        
        try:
            result['api'] = self.fetch_from_api()
        except (ValueError, IndexError) as e:
            log.error(e)

        # Telnet 
        try:
            result['telnet'] = self.fetch_from_telnet()
        except (ValueError, IndexError) as e:
            log.error(e)

        return result
        

    def combine_results(self):
        # type: () -> List[Dict]
        """
        Combine results from api and telnet.
        """
        result = []

        try:
            for telnet_pilot in self.live_data['telnet']:
                pilot = {
                    'server': telnet_pilot['server'],
                    'callsign': telnet_pilot['callsign'],
                    'client_type': 'ATC' if telnet_pilot['model'].lower() in self.atc_models else 'PILOT',
                    'latitude': telnet_pilot['lat'],
                    'longitude': telnet_pilot['lon'],
                    'altitude': telnet_pilot['alt'],
                    'aircraft_info': telnet_pilot['model']
                }

                for api_pilot in self.live_data['api']:
                    if api_pilot['callsign'] == telnet_pilot['callsign']:
                        # these need ?wpt=y in query string, which takes a long time
                        if self.more == True:
                            pilot['heading'] = api_pilot['wpt'][0]['hdg']
                        pilot['start_time'] = api_pilot['start_time']
                        pilot['planned_departure_airport'] = api_pilot['start_location']['icao']
                        pilot['aircraft_info'] = '{} ({})'.format(api_pilot['model_raw'], api_pilot['model'])
                        
                
                result.append(pilot)
        except (IndexError, KeyError, TypeError) as e:
            log.error(e)

        return result


    def search_pilots(self, callsign=None, find_callsign=None, near_icao=None, client_type=None, aircraft=None):
        """
        Search for pilots.

        Parmeters
        ---------
        callsign : Optional[str]
            get results with this exact callsign
        find_callsign : Optional[str]
            include callsigns containing `find_callsign`
        near_icao : Optional[str]
            filter results with pilots near this airport
        client_type : Optional[str]
            filter by client type ('atc', 'pilot', ...)
        aircraft : Optional[str]
            included results containig `aircraft` in aircraft model

        Returns
        -------
        Optional[List[PilotStruct]]
        """
        combined_data = self.combine_results()

        try:
            if not isinstance(combined_data, list) or len(combined_data) == 0:
                raise ValueError('No pilot data in {}.'.format(self.name))
        
        except (ValueError) as e:
            log.error(e)
            return None

        result = []

        # Get coordinates here since near_icao coordiantes need not be calculalted for each pilot.
        if isinstance(near_icao, str):
            try:
                icao_coordinates = get_airport_coordinates(near_icao.upper())[near_icao.upper()]
            except IndexError as e:
                log.error(e)
                return None

        for pilot in combined_data:
            test = True

            if isinstance(find_callsign, str):
                if find_callsign.lower() not in str(pilot['callsign']).lower():
                    test = False

            if isinstance(callsign, str) and not isinstance(find_callsign, str):
                if callsign.lower() != str(pilot['callsign']).lower():
                    test = False

            if isinstance(near_icao, str):
                try:
                    distance_from_icao = geodist(icao_coordinates[0], icao_coordinates[1], pilot['latitude'], pilot['longitude'])
                except (IndexError, KeyError) as e:
                    log.error(e)
                    return None
                if distance_from_icao / 1000 > 300: test = False

            if isinstance(client_type, str):
                if client_type.lower() not in str(pilot['client_type']).lower():
                    test = False

            if isinstance(aircraft, str):
                if aircraft.lower() not in str(pilot['aircraft_info']).lower():
                    test = False


            # populate struct
            if test == True:
                try:
                    pilot_entry = PilotStruct()

                    if pilot.get('server'): pilot_entry.server = pilot['server']
                    if pilot.get('callsign'): pilot_entry.callsign = pilot['callsign']
                    if pilot.get('client_type'): pilot_entry.client_type = pilot['client_type']
                    if pilot.get('latitude'): pilot_entry.latitude = pilot['latitude']
                    if pilot.get('longitude'): pilot_entry.longitude = pilot['longitude']
                    if pilot.get('altitude'): pilot_entry.altitude = pilot['altitude']
                    if pilot.get('heading'): pilot_entry.heading = pilot['heading']
                    if pilot.get('aircraft_info'): pilot_entry.aircraft_info = pilot['aircraft_info']
                    if pilot.get('start_time'): pilot_entry.start_time = pilot['start_time']
                    if pilot.get('planned_departure_airport'): pilot_entry.planned_departure_airport = pilot['planned_departure_airport']
                    
                    if near_icao and 'distance_from_icao' in locals():
                        pilot_entry.reference_point_name = near_icao.upper()
                        pilot_entry.reference_point_value = distance_from_icao

                except (IndexError, TypeError) as e:
                    log.error(e)
                    continue
                
                result.append(pilot_entry)

        return result
