# coding=utf-8
from __future__ import absolute_import, division, print_function, \
    unicode_literals

from peewee import FloatField, TextField, IntegerField, ForeignKeyField, SQL

from fsbot.data.model import BaseModel



__all__ = [
    'Glossary'
]



class Glossary(BaseModel):
    description = TextField()
    term = TextField(index=True)

    class Meta:
        table_name = 'glossary'
        primary_key = False
