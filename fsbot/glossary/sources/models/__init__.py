# coding=utf-8
from __future__ import absolute_import


# Do NOT do `from .local import *` here because 
# DB model names are the same, or will be if additional sources are added. 
# They should be imported in modules using full path: 
#
#`from fsbot.glossary.sources.models import local`
# then used like local.Glossary...  
#
# Or if used in context of one specific model:
#`from fsbot.glossary.sources.models.local import Glossary`
