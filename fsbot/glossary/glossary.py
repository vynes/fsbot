from peewee import fn

from fsbot import log
from fsbot.glossary.sources.models.local import Glossary 


__all__ = [
    'search'
]


def search(gloss_term=None, description_term=None, exact_term=False, limit=None):
    """
    Do a database search using all the given criteria

    Parameters
    ----------
    gloss_term : Optional[str]
        Term to search for in glossary name.
    description_term : Optional[str]
        Term to search for in glossary definition/description
    exact_term : Optional[boolean]
        Exact term to search for in glossary description
    limit : Optional[int]
        Limit results

    Returns
    -------
        ModelSelect
    """
    log.debug('GLOSS FUNC PARAMS: {}'.format(locals()))
    
    glossaries = Glossary.select(Glossary.term, Glossary.description)

    if isinstance(gloss_term, str):
        if exact_term == True:
            glossaries = glossaries.where(fn.Lower(Glossary.term) == gloss_term.lower())
        else:
            glossaries = glossaries.where(Glossary.term.contains(gloss_term))

    if isinstance(description_term, str):
        glossaries = glossaries.where(Glossary.description.contains(description_term))

    if isinstance(limit, int):
        glossaries = glossaries.order_by(Glossary.term).limit(limit)
    else:
        glossaries = glossaries.order_by(Glossary.term)

    return glossaries
