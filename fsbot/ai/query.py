from fsbot import cfg, log
from fsbot.ai.sources import source_registry, AiSourceError
from fsbot.exceptions import throw


__all__ = [
    'Query'
]


class AiError(Exception):
    pass

class AiInvalidResponseError(Exception):
    pass


class Query():
    """
    use_source_idx: specify source id or use 'any'/None/0 for default.

    """
        
    def __init__(self, use_source_idx=0):
        # type: (Union[int, NoneType, str]) -> None

        # Find all ai sources
        self.sources = source_registry


        # Source(s) to use.
        # Only specified source will be used with no fallback, 
        # else first if none specified.
        if use_source_idx in (0, None, 'any', 'all'):
            self.use_source_idx = min(self.sources.keys())
        else:
            if use_source_idx in self.sources:
                self.use_source_idx = use_source_idx
            else:
                self.use_source_idx = 0


    def query(self, q, username, channel):
        source_cls = self.sources[self.use_source_idx]['cls']
        return source_cls().query(q, username, channel)


    # def list(self):
    #     source_cls = self.sources[self.use_source_idx]['cls']
    #     return source_cls().list()
