from abc import ABCMeta, abstractmethod, abstractproperty
from importlib import import_module
from pkgutil import walk_packages
from inspect import isabstract as is_abstract, isclass as is_class, \
    getmembers as get_members

from fsbot import log
from fsbot.exceptions import throw



__all__ = [
    'AiSourceError',
    'AiInvalidRequestError',
    'BaseSource',
    'source_registry'
]



source_registry = {}



class AiSourceError(Exception):
    pass


class AiInvalidRequestError(Exception):
    pass


def discover_sources(package, recursively=True):
    # type: (Union[ModuleType, Text], bool) -> Dict[Text, 'SourceMeta']
    """
    Automatically discover sources in the specified package.

    :param package:
        Package path or reference.

    :param recursively:
        If True, will descend recursively into sub-packages.

    :return:
        All sources discovered in the specified package, indexed by
        source name (note: not class name).
    """
    # http://stackoverflow.com/a/25562415/
    if isinstance(package, str):
        package = import_module(package) # type: ModuleType

    sources = {}

    for _, name, is_package in walk_packages(package.__path__):
        # Loading the module is good enough; the SourceMeta metaclass will
        # ensure that any sources in the module get registered.
        sub_package = import_module(package.__name__ + '.' + name)
        log.debug('sub_package: {}'.format(sub_package))

        # Index any source classes that we find.
        for (_, obj) in get_members(sub_package):
            if is_class(obj) and isinstance(obj, SourceMeta):
                source_idx = getattr(obj, 'idx')
                source_name = getattr(obj, 'name')
                source_info = getattr(obj, 'info')
                log.debug('source: [{}] {} - {}'.format(source_idx, source_name, source_info))
                if source_idx and source_name:
                    sources[source_idx] = {"cls": obj, "idx": source_idx, "name": source_name, "info": source_info}

        if recursively and is_package:
            sources.update(discover_sources(sub_package))
        
    log.debug('sources: {}'.format(sources))

    return sources



class SourceMeta(ABCMeta):
    """
    Automatically register sources.
    """
    # noinspection PyShadowingBuiltins
    def __init__(cls, what, bases=None, dict=None):
        super(SourceMeta, cls).__init__(what, bases, dict)

        if not is_abstract(cls):
            source_idx = getattr(cls, 'idx')
            source_name = getattr(cls, 'name')
            source_info = getattr(cls, 'info')
            if source_idx and source_name:
                source_registry[source_idx] = {"cls": cls, "idx": source_idx, "name": source_name, "info": source_info}



class BaseSource(metaclass=SourceMeta):
    """
    Base Source Class
    """

    idx = None # type: int
    name = None # type: str
    info = '' # type: str


    def __init__(self):
        return None


    @abstractmethod
    def query(self):
        """
        Return ai response to query
        """
        raise NotImplementedError(
            'Not implemented in {cls}.'.format(cls=type(self).__name__),
        )


    # @abstractmethod
    # def list(self):
    #     """
    #     Must return list of ?
    #     """
    #     raise NotImplementedError(
    #         'Not implemented in {cls}.'.format(cls=type(self).__name__),
    #     )



# Autodiscover feeds in this package.
discover_sources(__name__)
