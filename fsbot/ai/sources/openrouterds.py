import json
from fsbot.exceptions import throw
# from fsbot import log, cast_from_ctype
from fsbot.connectivity import http
from fsbot.ai.sources import AiSourceError, AiInvalidRequestError, BaseSource
from fsbot import log


__all__ = [
    'OpenRouterDsRo'
]


class OpenRouterDsRo(BaseSource):
    """
    Get ai responses from OpenRouter
    """
    
    idx = 2
    name = 'OpenRouter::deepseek/deepseek-r1:free'
    info = 'https://openrouter.ai/deepseek/deepseek-r1:free'

    base_url = 'https://openrouter.ai/api/v1/chat/completions'

    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer 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"
    }

    post_data = {
        "model": "deepseek/deepseek-r1:free",
        "include_reasoning": False,
        "messages": [
            {
                "role": "user",
                "content": ""
            },
            {
                "role": "system",
                "content": "You are a bot in an IRC channel. Keep responses short and relevant."
            }
        ]
    }

    def __init__(self):
        #type: () -> None

        super(OpenRouterDsRo, self).__init__()
 
        
    def query(self, query, username=None, channel=None):
        if username != None and channel != None:
            query = f'Here is a message to you from irc user \'{username}\' (respond to the user) in channel \'{channel}\': {query}'

        self.base_url = self.base_url.rstrip("/")
        self.post_data['messages'][0]["content"] = query
        
        try:
            openrouter_result = http.request(
                method='POST',
                url=self.base_url,
                data=json.dumps(self.post_data),
                headers=self.headers,
                json_decode=True,
                ignore_error_codes=[400]
            )
        except http.NetError as e:
            log.error('Error getting data from {}. Error: {}'.format(self.name, e))
            raise AiSourceError(e)

        if 'error' in openrouter_result or 'choices' not in openrouter_result:
            err = 'AiSourceError'
            if 'error' in openrouter_result and 'message' in openrouter_result['error']:
                try:
                    err = json.loads(openrouter_result["error"]["metadata"]["raw"])['detail']
                except:
                    err = openrouter_result["error"]["message"]
            raise throw(
                exc=AiSourceError(err),
                context={
                    'data': query
                }
            )

        log.debug(f'got openrouter respone: {openrouter_result}')

        try:
            return openrouter_result['choices'][0]['message']['content']
        except:
            return 'Err[retarded_ai]or'


    # def list(self):
        
    #     self.base_url = self.base_url.rstrip('/') + '/currencies.json'

    #     try:
    #         oxr_result = http.request(url=self.base_url, params=self.url_params, json_decode=True)
    #     except http.NetError as e:
    #         log.error('Error getting data from {}. Error: {}'.format(self.name, e))
    #         raise AiSourceError(e)

    #     return oxr_result
