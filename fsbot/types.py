from ctypes import c_int, c_wchar_p, c_float, c_double
from typing import Any


__all__ = [
    'cast_from_ctype'
]


def cast_from_ctype(value: str, c_type: Any) -> Any:
    """
    Convert ctypes to python equivalent
    """
    conversion = {
        c_int: int,
        c_wchar_p: str,
        c_float: float,
        c_double: float
    }

    return conversion[c_type](value)
