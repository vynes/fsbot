from typing import Optional
import re

from fsbot import log


__all__ = [
    'MorseConverter',
    'MorseInputError'
]


class MorseInputError(Exception):
    pass

class MorseError(Exception):
    pass


class MorseConverter():
    """
    Simple Morse Code converter
    """

    default_output_separator = ' '

    morse_table_a = {
        'a': '._', 'b': '_...', 'c': '_._.', 'd': '_..', 'e': '.', 'f': '.._.', 
        'g': '__.', 'h': '....', 'i': '..', 'j': '.___', 'k': '_._', 'l': '._..', 
        'm': '__', 'n': '_.', 'o': '___', 'p': '.__.', 'q': '__._', 'r': '._.', 
        's': '...', 't': '_', 'u': '.._', 'v': '..._', 'w': '.__', 'x': '_.._', 
        'y': '_.__', 'z': '__..', 
        '1': '.____', '2': '..___', '3': '...__', '4': '...._', '5': '.....', 
        '6': '_....', '7': '__...', '8': '___..', '9': '____.', '0': '_____' 
    }

    morse_table_u = {
        'a': '•−', 'b': '−•••', 'c': '−•−•', 'd': '−••', 'e': '•', 'f': '••−•', 
        'g': '−−•', 'h': '••••', 'i': '••', 'j': '•−−−', 'k': '−•−', 'l': '•−••', 
        'm': '−−', 'n': '−•', 'o': '−−−', 'p': '•−−•', 'q': '−−•−', 'r': '•−•', 
        's': '•••', 't': '−', 'u': '••−', 'v': '•••−', 'w': '•−−', 'x': '−••−', 
        'y': '−•−−', 'z': '−−••', 
        '1': '•−−−−', '2': '••−−−', '3': '•••−−', '4': '••••−', '5': '•••••', 
        '6': '−••••', '7': '−−•••', '8': '−−−••', '9': '−−−−•', '0': '−−−−−' 
    }


    def __init__(self, input_text: str, output_separator: Optional[str] = ' ', output_unicode: bool = True, capitalize: bool = False):
        self.output_separator = output_separator if output_separator else self.default_output_separator
        self.input_text = input_text.lower()
        self.output_unicode = output_unicode
        self.capitalize = capitalize

        self.answer = None
        self.ascii_text, self.ascii_morse = self.convert()


    def __str__(self):
        try:
            return self.answer
        except TypeError as e:
            log.error(e)
            raise MorseError(e)


    def ascii_to_morse(self):

        morse_table = self.morse_table_u if self.output_unicode == True else self.morse_table_a 

        new_text = []

        ascii_sentences = re.sub(r'[^0-9A-Za-z \.]', '', self.input_text).strip().split('. ')

        for ascii_sentence in ascii_sentences:
            new_sentence = []
            ascii_words = ascii_sentence.split()

            for ascii_word in ascii_words:
                new_word = []
                ascii_letters = ascii_word

                for ascii_letter in ascii_letters:
                    if ascii_letter == '.': continue
                    new_word.append(morse_table[ascii_letter])

                new_sentence.append(self.output_separator.join(new_word))

            new_text.append((self.output_separator * 2).join(new_sentence))

        return (self.output_separator * 4).join(new_text)


    def morse_to_ascii(self):
        use_morse_table = self.morse_table_a if '_' in self.input_text else self.morse_table_u
        # invert table
        morse_table = {v: k for k, v in use_morse_table.items()}

        if re.search(r'[^ _\.\"\'•−]', self.input_text.strip()):
            raise MorseInputError('Input is garbage.')

        new_text = []
        morse_sentences = re.sub(r'[^ _\.•−]', '', self.input_text).strip().strip(' ').split('    ')
        
        for morse_sentence in morse_sentences:
            new_sentence = []
            morse_words = morse_sentence.split('  ')

            for morse_word in morse_words:
                new_word = ''
                morse_letters = morse_word.split(' ')

                for morse_letter in morse_letters:
                    try:
                        new_word += morse_table[morse_letter]
                    except KeyError:
                        raise MorseInputError('Could not ignore ambiguous garbage.')

                new_sentence.append(new_word)

            new_text.append(' '.join(new_sentence))

            if self.capitalize:
                new_text[-1] = new_text[-1].capitalize()

        # Add a period if more than one sentence
        period = '.' if len(new_text) > 1 else ''

        return '. '.join(new_text) + period


    def convert(self):
        if set(self.input_text).intersection(self.morse_table_a):
            self.answer = self.ascii_to_morse()
            return (self.input_text, self.answer)
        self.answer = self.morse_to_ascii()
        return (self.answer, self.input_text)
