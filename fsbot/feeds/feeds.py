import feedparser
import hashlib, time

from typing import List, Dict, Optional

from fsbot.exceptions import throw
from fsbot import cfg, log, app_state
from fsbot.feeds import source_registry


__all__ = [
    'rss_get',
    'scheduled_feeds',
    'FeedSourceError'
]


# {reg_id: [time, result_hash]}
polling_registry = {}


class FeedSourceError(Exception):
    pass


def rss_get(source_id: int, limit: Optional[int] = None, category_filters: Optional[List] = None, title_filters: Optional[List] = None) -> List[feedparser.FeedParserDict]:
    log.debug(f'Making feed rquest to: {source_registry[source_id]["url"]}')
    
    try:
        feed = feedparser.parse(source_registry[source_id]['url'])
    except Exception as e:
        raise throw(FeedSourceError(e), {'url': source_registry[source_id]['url']})

    if feed.get('bozo') == 1:
        if feed.get('bozo_exception') is not None:
            log.warning(f'Feed error ({source_registry[source_id]["url"]}): {feed.bozo_exception}.')

    matched_entries = []

    if category_filters is not None or title_filters is not None:
        for item in feed.entries:
            if item.get('title') is None or item.get('title') == '': continue

            # Category filters
            if category_filters is None: category_filters = []
            if title_filters is None: title_filters = []

            cfilters_neg = [tn[1:].lower() for tn in category_filters if tn[0] == '^']
            cfilters_pos = [tp.lower() for tp in category_filters if tp[0] != '^']

            tfilters_neg = [tn[1:].lower() for tn in title_filters if tn[0] == '^']
            tfilters_pos = [tp.lower() for tp in title_filters if tp[0] != '^']

            tags = item.tags if item.get('tags') is not None else []

            tag_hits = 0

            if len(tfilters_neg) > 0 and not any([f in item.title.lower() for f in tfilters_neg]):
                tag_hits += len(tfilters_neg)

            if len(cfilters_neg) > 0 and not any([tag.term.lower() in cfilters_neg for tag in tags]):
                tag_hits += len(cfilters_neg)

            if any([tag.term.lower() in cfilters_pos for tag in tags]):
                tag_hits += len(cfilters_pos)

            if any([f in item.title.lower() for f in tfilters_pos]):
                tag_hits += len(tfilters_pos)

            if tag_hits == len(category_filters) + len(title_filters):
                matched_entries.append(item)
        return matched_entries[:limit]
    else:
        return feed.entries[:limit]


def scheduled_feeds(connected_channels: List) -> List:
        """
        Return due scheduled feeds.
        """
        feed_results = []

        period_seconds = {
            'm': 60,
            'h': 3600,
            'd': 86400,
            'w': 604800,
            'M': 2419200
        }

        # Used to cache feed results to prevent making multiple duplicate requests.
        feed_cache = {}

        # First run task start time
        first_run_offset = cfg.getfloat('POLLING', 'FirstRunTimeOffset', fallback=300)

        # Number of seconds to space the first-run posts apart. This also spaces 
        # apart same interval tasks during regular schedule.
        first_run_increment = cfg.getfloat('POLLING', 'FirstRunTimeIncrement', fallback=1200)

        for idx,feed_src in source_registry.items():

            if feed_src['polling_channels'] is None or feed_src['polling_limit'] < 1: continue

            for sched_channel in feed_src['polling_channels'].split():

                changed_only = False
                sched,channel = sched_channel.split('#')
                channel = '#' + channel
                if channel not in connected_channels: continue

                if sched[-1] == '*':
                    changed_only = True
                    sched = sched[:-1]

                if sched[-1] not in period_seconds:
                    log.error(f'Bogus schedule: \'{sched}\'. Skipping...')
                    continue

                psec = period_seconds[sched[-1]]
                num = float(sched[:-1])
                intv = psec / num

                # Identifier
                reg_id = hashlib.md5(bytes(feed_src['name'] + channel, 'utf-8')).hexdigest()

                # Has not run yet, don't run first time, but add to registry with last 
                # run time set to now.
                if reg_id not in polling_registry:
                    polling_registry[reg_id] = [0 ,'']
                    continue

                if intv < cfg.get('POLLING', 'MinPollingScheduledInterval', fallback=30):
                    log.error(f'Schedule interval too short: {intv} seconds. Skipping...')
                    continue

                if polling_registry[reg_id][0] == 0:
                    # First run
                    scheduled_time_check = app_state['start_time'] + first_run_offset
                else:
                    scheduled_time_check = polling_registry[reg_id][0] + intv

                log.debug(f'TEST: {time.time()} > {scheduled_time_check}: {time.time() > scheduled_time_check}')

                if (time.time() > scheduled_time_check):
                    if feed_cache.get(feed_src['url']) is None:
                        try:
                            feed_cache[feed_src['url']] = rss_get(idx, feed_src['polling_limit'], feed_src['polling_category_filters'].split(), feed_src['polling_title_filters'].split())
                        except FeedSourceError as e:
                            # Log error and skip
                            log.error(f'Error occurred trying to get {e.context["url"]}')
                            continue

                    feed_result = feed_cache[feed_src['url']]

                    if len(feed_result) > 0:
                        if hasattr(feed_result[0], 'published'):
                            timestamp = feed_result[0].published
                        elif hasattr(feed_result[0], 'updated'):
                            timestamp = feed_result[0].updated
                        else:
                            timestamp = ""
                        
                        result_hash = hashlib.md5(bytes(timestamp + feed_result[0].title, 'utf-8')).hexdigest()

                        if changed_only == True and polling_registry[reg_id][1] == result_hash:
                            log.debug(f"Result hasn't changed, skipping {result_hash}.")
                        else:
                            feed_results.append({'name':feed_src['name'], 'channel': channel, 'items': feed_result})

                        polling_registry[reg_id] = [time.time(), result_hash]
                    else:
                        log.warning(f'Empty result set for feed: {feed_src["url"]}.')
                        polling_registry[reg_id] = [time.time(), '']

                        # No results for this feed therefore no use iterating over rest of channels for this
                        # feed, so break out of channel loop for this feed.
                        break

            first_run_offset += first_run_increment

        log.debug(f'Polling registry: {polling_registry}')

        return feed_results
