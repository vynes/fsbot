from fsbot import cfg, ConfigValueError


__all__ = [
    'source_registry',
    'feed_shedule_queue'
]


source_registry = {}


def discover_sources():
    # 1 = \Linux Kernel\https://www.kernel.org/feeds/kdist.xml\2d*#channel_b\1\\mainline
    if 'RSS_FEEDS' in cfg.sections():
        for idx,value in cfg['RSS_FEEDS'].items():
            
            values = value[1:].split(value[0],6)
            
            try:
                source_registry[int(idx)] = {
                    'name': values[0],
                    'url': values[1]
                }
                
                # Polling values
                if len(values) > 2:
                    # Optional[str]
                    source_registry[int(idx)]['polling_channels'] = values[2] if len(values[2]) > 0 else None
                    # int
                    source_registry[int(idx)]['polling_limit'] = int(values[3]) if len(values[3]) > 0 else 0
                    # Optional[str]
                    source_registry[int(idx)]['polling_category_filters'] = values[4] if len(values[2]) > 0 else None
                    # Optional[str]
                    source_registry[int(idx)]['polling_title_filters'] = values[5] if len(values[2]) > 0 else None
            except (IndexError, KeyError, ValueError):
                raise ConfigValueError(f'Unable to parse feed: {idx} = {value}')

discover_sources()

from .feeds import *
