from typing import Optional, List, Dict

from fsbot import log, cfg
from fsbot.airport import get_airport_coordinates
from fsbot.connectivity import http


__all__ = [
    'timezone_information',
    'icao_timezone_information',
    'sym_degrees'
]


sym_degrees = '\u00b0T'


def timezone_information(lat: float, lon: float) -> Optional[Dict]:
    """
    Returns timezone information for set of coordinates.
    """
    result = http.request(
        'http://api.geonames.org/timezoneJSON?lat={lat}&lng={lon}&username={username}'.format(
            lat=lat,
            lon=lon,
            username='fsbot'
        ), json_decode=True)
    
    return result


def icao_timezone_information(icao: str) -> Optional[Dict]:
    """
    Returns timezone information for airport ICAO.
    """
    coords = get_airport_coordinates(icao)
    if coords == None or None in coords:
        return None
    
    result = timezone_information(coords[icao][0], coords[icao][1])
    
    return result