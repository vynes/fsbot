import datetime, os

from fsbot import log, is_number


__all__ = [
    'print_line_sep',
    'utc_date_from_timestamp',
    'utf8',
    'full_path',
    'path_is_file',
    'gt',
    'lt',
    'eq'
]


def print_line_sep(size=70, char='-', nlb=False, nla=False):
    # type: (int, str) -> str
    """print a line separator"""
    if not isinstance(size, int) or not isinstance(char, str):
        line = '-' * 70
    else:
        line = char * size

    print('{}{}{}'.format('\n' if nlb else '', line, '\n' if nla else ''))


def utc_date_from_timestamp(timestamp):
    # type: Union(str, float, int) -> datestring
    try:
        return datetime.datetime.utcfromtimestamp(float(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
    except ValueError as e:
        log.error(e)
        return '--/--/--'


def utf8(unicode_string, errors='replace'):
    # type: (str, str) -> bytes
    return unicode_string.encode('utf-8', errors)


def full_path(file_path):
    # type: (str) -> str
    """
    Return user expanded absolute path
    """
    return os.path.abspath(os.path.expanduser(file_path)) if isinstance(file_path, str) else None


def path_is_file(file_path):
    # type: (str) -> bool
    """
    Check whether file_path points to an existing FILE

    file_path must be full absolute path.
    """
    return os.path.isfile(file_path) if isinstance(file_path, str) else False


def gt(x, y):
    # type: (Union(float, it, str), Union(float, it, str)) -> bool 
    """
    Test if x is greater than y
    """
    try:
        return float(x) > float(y)
    except ValueError:
        return False


def lt(x, y):
    # type: (Union(float, it, str), Union(float, it, str)) -> bool 
    """
    Test if x is less than y
    """
    try:
        return float(x) < float(y)
    except ValueError:
        return False


def eq(x, y):
    # type: (Union(float, it, str), Union(float, it, str)) -> bool 
    """
    Test if x is equal to y
    """
    try:
        return float(x) == float(y)
    except ValueError:
        return False


def parse_compare(comp_string, y):
    # type: (str, Union(float, int, str)) -> bool
    """
    Parse comparison string and return associated comparison 
    result (bool) between its value and y value.
    """
    comp = {
        '>': gt,
        '<': lt,
        '=': eq
    } 

    if comp_string[:1] not in comp and is_number(comp_string):
        comp_string = '{}{}'.format('=', comp_string)

    if not is_number(comp_string[1:]):
        return False

    return comp[comp_string[:1]](comp_string[1:], y)
